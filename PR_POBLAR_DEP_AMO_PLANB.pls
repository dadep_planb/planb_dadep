create or replace PROCEDURE PR_POBLAR_DEP_AMO_PLANB (V_ANO VARCHAR2, V_MES VARCHAR2)AS 

ANO_PROCESO number := 0;
MES_PROCESO number := 0;

BEGIN



  INSERT  into conbi_depreciacion_amortizacio
                ( NIVEL_SECUNDARIO,
                  PRED_NRO_PREDIO,
                  CONTROL_ANO_PROCESO,
                  CONTROL_MES_PROCESO,
                  CONTROL_TIPO_PROCESO,
                  DEPRECIACION,
                  DEPRECIACION_AJUSTADA,
                  DEPRECIACION_ACUMULADA,
                  AXI_DEPRECIACION,
                  CUENTA_CONBI_DB,
                  CUENTA_CONBI_CR,
                  PREURBANIZACI_NRO_URBANIZACION,
                  CON_ID,
                  TIEMPO_DEP_AMO_CALCULADA,
                  VALOR_NETO,
                  TIPO_MOVIMIENTO,
                  TRANSACCION,
                  VERSION,
                  USUARIO,
                  FECULTACT,
                  VAL_TOTAL--,
                  --ESTADO--,
                  --ID 
                  )
                          select 
                              NIVEL_SECUNDARIO,
                              PRED_NRO_PREDIO,
                              CONTROL_ANO_PROCESO,
                              CONTROL_MES_PROCESO,
                              CONTROL_TIPO_PROCESO,
                              DEPRECIACION,
                              DEPRECIACION_AJUSTADA,
                              DEPRECIACION_ACUMULADA,
                              AXI_DEPRECIACION,
                              CUENTA_CONBI_DB,
                              CUENTA_CONBI_CR,
                              PREURBANIZACI_NRO_URBANIZACION,
                              0,
                              TIEMPO_DEP_AMO_CALCULADA,
                              VALOR_NETO,
                              TIPO_MOVIMIENTO,
                              TRANSACCION,
                              0,
                              USUARIO,
                              sysdate,
                              VAL_TOTAL--,
                              --ESTADO--,
                              --ID 
                              from conbi_dep_amo_pruebas
                              where CONTROL_ANO_PROCESO= V_ANO and CONTROL_MES_PROCESO= V_MES;  


delete from conbi_dep_amo_pruebas;

END;