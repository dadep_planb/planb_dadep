create or replace procedure PR_CONSULTA_DEPRECIAPLANB_CIE (V_ANO  VARCHAR2, V_MES  VARCHAR2) 
AS

meses                   number:= 0;
fec_ing_cont            varchar2(20);
costo_adquisicion       number:= 0;
cuenta_conbi_db         number:= 0;
cuenta_conbi_cr         number:= 0;
fecha                   number:= 0;
nueva_alicuota          number:= 0;
depreciacion            number:= 0;
tvidautilsindepreciar   number:= 0;
alicuota_acumulada_ant  number:= 0;
nueva_alicuota_acumulada  number:= 0;
alicuota_anterior       number:= 0;    
v_valor_neto              number:= 0;
alicuota_acumulada      number:= 0;
vida_util_total         number:= 600;
bandera                 number:=0;
v_valor_alicuota_anterior_ac          number:=0;
v_valor_alicuota_anterior number:=0;
v_alicuota_traslado     number := 0;
v_valor_predio_traslado number := 0;
v_suma_egresosC         number :=0;
v_suma_ingresosC        number :=0;
v_costo_historico       number  :=  0;
v_costo_historico_ant       number  :=  0;
v_fecha_baja            varchar2(30);
v_egr varchar2(1) := 0;
v_cta_mov varchar2(30);
  v_ano1       varchar2(10) := 0;
  v_mes1       varchar2(10) := 0;
   v_mesa number := 0;
begin
   --execute immediate 'truncate table conbi_dep_amo_pruebas1';
   delete from CONBI_DEP_AMO_PRUEBAS5;
commit; 
  
   /*Periodo inmediatamente anterior, para predios que tienen solo egresos en el mes*/
if  v_mes = '01' then
  v_mes1 := '12';
  v_ano1 := to_char(to_number(v_ano,9999)-1);
else
  v_mesa := to_number(v_mes,99)-1;
  if v_mesa < 10 then
    v_mes1 := '0' || to_char(v_mesa);
  else
    v_mes1 := to_char(v_mesa);
  end if;
  v_ano1 := v_ano;
end if;

  /*Predios depreciables en  la fecha de consulta*/             
  for c_c in (SELECT distinct 
                                cm.preurbanizaci_nro_urbanizacion,
                                cm.pred_nro_predio,
                                cm.nivel_desagregacion, 
                                cm.cuenta_conbi_db,
                                cm.cuenta_conbi_cr,
                                cm.valor_predio,
                                cm.depreciable,
                                cm.tipo_movimiento,
                                cm.tipo_acto_juridico,
                                cm.uso_predio,
                                cm.uso_nivel1,
                                cm.uso_nivel2,
                                cm.tipo_procedencia,
                                cm.tipo_transaccion,
                                cm.clase_cuenta,
                                cm.fecha_movimiento
                      from      conbi_movimientos cm
                      where     cm.fecha_movimiento = (select   max(mm.fecha_movimiento)
                                                         from   conbi_movimientos mm 
                                                        where   cm.preurbanizaci_nro_urbanizacion = mm.preurbanizaci_nro_urbanizacion
                                                          and   cm.pred_nro_predio = mm.pred_nro_predio
                                                          and   cm.nivel_desagregacion = mm.nivel_desagregacion
                                                          and   (to_char(mm.fecha_movimiento, 'yyyy') < v_ano  or 
                                                                (to_char(mm.fecha_movimiento, 'yyyy') = v_ano 
                                                                 and to_char(mm.fecha_movimiento, 'mm')<= v_mes))
                                                          and   (mm.cuenta_conbi_db like '1640%' 
                                                                 or mm.cuenta_conbi_db like '19200601%' 
                                                                 or mm.cuenta_conbi_db like '19200602%' 
                                                                 or mm.cuenta_conbi_db like '19200609%')
                                                          )
                      and     to_char((select min (mmm.fecha_movimiento)
                                        from conbi_movimientos mmm
                                        where (mmm.cuenta_conbi_db like '1640%'
                                                or mmm.cuenta_conbi_db like '19200601%' 
                                                or mmm.cuenta_conbi_db like '19200602%' 
                                                or mmm.cuenta_conbi_db like '19200609%')
                      and     cm.preurbanizaci_nro_urbanizacion = mmm.preurbanizaci_nro_urbanizacion
                      and     cm.pred_nro_predio = mmm.pred_nro_predio
                      and     cm.nivel_desagregacion = mmm.nivel_desagregacion ), 'yyyymm') >= '199201'
                      and     cm.depreciable='S'
                      and     cm.nivel_desagregacion            <>     1  
                      and     (cm.cuenta_conbi_db not like '83%' and cm.cuenta_conbi_db not like '1605%'  and    
                               cm.cuenta_conbi_db not like '16409024%' and cm.cuenta_conbi_db not like '19200633%' and 
                               cm.cuenta_conbi_db not like '17%')
                      order by cm.preurbanizaci_nro_urbanizacion asc, cm.pred_nro_predio asc, cm.nivel_desagregacion asc
              )
   loop          

      /*Consultar la fecha de ingreso a contabilidad del predio*/

      
      select to_char(min (mmm.fecha_movimiento),'DD/MM/YYYY')
               into fec_ing_cont
               from conbi_movimientos mmm
              where mmm.preurbanizaci_nro_urbanizacion = c_c.preurbanizaci_nro_urbanizacion
                and mmm.pred_nro_predio = c_c.pred_nro_predio
                and mmm.nivel_desagregacion = c_c.nivel_desagregacion
                and (mmm.cuenta_conbi_db like '1640%' or mmm.cuenta_conbi_cr like '1920%')
               and  mmm.cuenta_conbi_cr not like '19200633%';
      
      begin
      /*Consultar cuenta de movimiento del predio*/
      select  cm.CUENTA_CONBI_DB as cta
      into    v_cta_mov
      from      conbi_movimientos cm
      where   CM.PREURBANIZACI_NRO_URBANIZACION = c_c.PREURBANIZACI_NRO_URBANIZACION
        and   CM.PRED_NRO_PREDIO = c_c.PRED_NRO_PREDIO
        and   CM.NIVEL_DESAGREGACION = c_c.nivel_desagregacion
        and     cm.FECHA_MOVIMIENTO = (select max(mm.fecha_movimiento)
                                           from   CONBI_MOVIMIENTOS mm 
                                          where   CM.PREURBANIZACI_NRO_URBANIZACION = MM.PREURBANIZACI_NRO_URBANIZACION
                                            and   CM.PRED_NRO_PREDIO = MM.PRED_NRO_PREDIO
                                            and   CM.NIVEL_DESAGREGACION = MM.NIVEL_DESAGREGACION
                                            and   ( to_char(MM.FECHA_MOVIMIENTO, 'yyyy') < v_ano  or 
                                                   (to_char(MM.FECHA_MOVIMIENTO, 'yyyy') = v_ano 
                                                   and    to_char(MM.FECHA_MOVIMIENTO, 'mm')<= v_mes))
                                            and    (mm.cuenta_conbi_db like '1640%' 
                                                                 or mm.cuenta_conbi_db like '19200601%' 
                                                                 or mm.cuenta_conbi_db like '19200602%' 
                                                                 or mm.cuenta_conbi_db like '19200609%')
                                            )
        and     to_char((select min (mmm.fecha_movimiento)
                                        from conbi_movimientos mmm
                                        where (mmm.cuenta_conbi_db like '1640%'
                                                or mmm.cuenta_conbi_db like '19200601%' 
                                                or mmm.cuenta_conbi_db like '19200602%' 
                                                or mmm.cuenta_conbi_db like '19200609%')
                      and     cm.preurbanizaci_nro_urbanizacion = mmm.preurbanizaci_nro_urbanizacion
                      and     cm.pred_nro_predio = mmm.pred_nro_predio
                      and     cm.nivel_desagregacion = mmm.nivel_desagregacion ), 'yyyymm') >= '199201'
                      and     cm.depreciable='S'
                      and     cm.nivel_desagregacion            <>     1  
                      and     (cm.cuenta_conbi_db not like '83%' and cm.cuenta_conbi_db not like '1605%'  and    
                               cm.cuenta_conbi_db not like '16409024%' and cm.cuenta_conbi_db not like '19200633%' and 
                               cm.cuenta_conbi_db not like '17%');
      
      --Consultar las cuentas del movimiento
      select ca.cuenta_conbi_db, ca.cuenta_conbi_cr     
        into cuenta_conbi_db, cuenta_conbi_cr
        from conbi_pr_plan_contable_alterno ca 
       where 
            ca.uso_predio = c_c.uso_predio 
        and ca.uso_nivel1 = c_c.uso_nivel1 
        and ca.uso_nivel2 = c_c.uso_nivel2 
        and ca.tipo_acto_juridico = c_c.tipo_acto_juridico 
        and ca.tipo_procedencia = c_c.tipo_procedencia
        and ca.clase_cuenta = c_c.clase_cuenta  
        and ca.tipo_movimiento = 'DEP';
                
      /*Consultar el costo historico hasta la fecha de consulta*/
      select nvl(sum(valor_predio),0)
        into v_suma_ingresosC
        from conbi_movimientos cm 
       where cm.preurbanizaci_nro_urbanizacion = c_c.preurbanizaci_nro_urbanizacion
         and cm.pred_nro_predio = c_c.pred_nro_predio
         and cm.nivel_desagregacion = c_c.nivel_desagregacion
         and cm.tipo_movimiento = 'ING'
         and (cm.cuenta_conbi_db like '1640%' or cm.cuenta_conbi_db like '1920%')
         and  cm.cuenta_conbi_db not like '19200633%'
         and (to_char(cm.fecha_movimiento, 'yyyy') < v_ano  or 
             (to_char(cm.fecha_movimiento, 'yyyy') = v_ano 
              and to_char(cm.fecha_movimiento, 'mm')<= v_mes));
             
      select nvl(sum(valor_predio) ,0)
        into v_suma_egresosC
        from conbi_movimientos cm 
       where cm.preurbanizaci_nro_urbanizacion = c_c.preurbanizaci_nro_urbanizacion
         and cm.pred_nro_predio = c_c.pred_nro_predio
         and cm.nivel_desagregacion = c_c.nivel_desagregacion
      --and   cm.tipo_movimiento = 'EGR'
         and (cm.cuenta_conbi_cr like '1640%' or cm.cuenta_conbi_cr like '1920%')
         and  cm.cuenta_conbi_cr not like '19200633%'
         and (to_char(cm.fecha_movimiento, 'yyyy') < v_ano  or 
              (to_char(cm.fecha_movimiento, 'yyyy') = v_ano 
               and to_char(cm.fecha_movimiento, 'mm')<= v_mes));
       
       v_costo_historico := v_suma_ingresosC-v_suma_egresosC;
      if v_costo_historico > 0 then
        /*Consultar datos del mes anterior*/
        begin    
          v_valor_alicuota_anterior := 0;
          v_valor_alicuota_anterior_ac := 0;
          v_costo_historico_ant := 0;
          meses := 0;
          /*Consultar los datos del mes anterior*/
          select depreciacion, depreciacion_acumulada,val_total,TIEMPO_DEP_AMO_CALCULADA--,egr
          into v_valor_alicuota_anterior, v_valor_alicuota_anterior_ac,v_costo_historico_ant,meses--,v_egr
          from (select depreciacion, depreciacion_acumulada,val_total,TIEMPO_DEP_AMO_CALCULADA--,egr
          from conbi_depreciacion_amortizacio  
          where 
          PREURBANIZACI_NRO_URBANIZACION  = c_c.PREURBANIZACI_NRO_URBANIZACION
          and  PRED_NRO_PREDIO            = c_c.PRED_NRO_PREDIO
          and  NIVEL_SECUNDARIO           = c_c.nivel_desagregacion
          and  control_ano_proceso        = '2012'
          and  control_mes_proceso        = '12'
          and  control_tipo_proceso       = 'DEP'
          --order by control_ano_proceso desc, control_mes_proceso desc, con_id desc
          ) where rownum = 1;
        
          costo_adquisicion         := v_costo_historico;
          nueva_alicuota            := meses;--c_c.TIEMPO_DEP_AMO_CALCULADA;                                                         
          tvidautilsindepreciar     := vida_util_total - nueva_alicuota;
          depreciacion              := v_valor_alicuota_anterior;
          alicuota_anterior         := v_valor_alicuota_anterior;
          alicuota_acumulada_ant    := v_valor_alicuota_anterior_ac;
          alicuota_acumulada        := v_valor_alicuota_anterior_ac; 
          v_valor_neto              := costo_adquisicion - alicuota_acumulada;
          NUEVA_ALICUOTA_ACUMULADA  := alicuota_acumulada + depreciacion;  
          
        exception when no_data_found then
          /*El predio nunca se ha amotizado, es la primera vez*/
            costo_adquisicion         :=  v_costo_historico;
            nueva_alicuota            := 1;
            depreciacion              := 0;--round((costo_adquisicion/600),1) * nueva_alicuota; 
            tvidautilsindepreciar     := 600;-- - nueva_alicuota;
            alicuota_acumulada_ant    := 0;                                                   
            nueva_alicuota_acumulada  := 0;--round((costo_adquisicion/600),1) * nueva_alicuota ;
            alicuota_anterior         := 0;    
            v_valor_neto              := 0; --costo_adquisicion - depreciacion * nueva_alicuota;
            alicuota_acumulada        := 0; --depreciacion * nueva_alicuota;
        end;            
                    
        INSERT INTO CONBI_DEP_AMO_PRUEBAS5(
                                            preurbanizaci_nro_urbanizacion, 
                                            pred_nro_predio,
                                            NIVEL_SECUNDARIO,
                                            DEPRECIACION,   
                                            DEPRECIACION_ACUMULADA,
                                            DEPRECIACION_AJUSTADA,        
                                            axi_depreciacion,  
                                            vida_util,                    
                                            tvidautilsindepreciar,        
                                            tiempo_dep_amo_calculada,     
                                            control_ano_proceso,        
                                            control_mes_proceso,
                                            control_tipo_proceso,         
                                            valor_neto,                   
                                            TRANSACCION,
                                            TIPO_MOVIMIENTO,
                                            ID,
                                            FECHA_INCORPORA,
                                            COSTO_HISTORICO,
                                            NUEVA_ALICUOTA_ACUMULADA,       
                                            VALOR_ALICUOTA_ANT,             
                                            ALICUOTA_ANTERIOR,
                                            cuenta_conbi_db,
                                            cuenta_conbi_cr,
                                            CUENTA_MOVIMIENTO,
                                            USO_PREDIO,
                                            USO_NIVEL1,
                                            USO_NIVEL2,
                                            TIPO_ACTO_JURIDICO,
                                            TIPO_PROCEDENCIA,
                                            CLASE_CUENTA
                                            )
                  VALUES    (
                              c_c.PREURBANIZACI_NRO_URBANIZACION,     
                              c_c.PRED_NRO_PREDIO,                    
                              c_c.nivel_desagregacion,                 
                              depreciacion,                           
                              alicuota_acumulada_ant,                 
                              0,                                      
                              0,                                      
                              vida_util_total,                        
                              tvidautilsindepreciar,                  
                              nueva_alicuota,                         
                              v_ano,                                  
                              v_mes,                                  
                              'DEP',                                  
                              v_valor_neto,                              
                              1,                                      
                              'CAL',                                  
                              SEQ_CONBI_AMO_PLANB.NEXTVAL,             
                              fec_ing_cont,                 
                              v_costo_historico,            
                              alicuota_acumulada,                     
                              alicuota_anterior,                      
                              nueva_alicuota,                         
                              cuenta_conbi_db,                        
                              cuenta_conbi_cr,
                              v_cta_mov,--cta de movimiento
                              0,
                              0,
                              0,
                              '',
                              0,
                              ''
                              ); 
      end if;
      exception when no_data_found then
      bandera := 0;
      end;
     end loop;
   commit;
end;