create or replace PROCEDURE        "PR_APROBAR_AMORTIZACION1" (V_ANO VARCHAR2, V_MES VARCHAR2)AS 
  v_cuenta_conbi_db number := 0;
  v_cuenta_conbi_cr number := 0;
  v_movimiento  varchar2(10) := '';
  v_ano1       varchar2(10) := 0;
  v_mes1       varchar2(10) := 0;
  v_valor_alicuota_anterior_ac          number:=0;
  v_valor_alicuota_anterior number:=0;
  v_tran  number  := 0;
  v_nroM number := 0;
  v_anterior number := 0;
  v_mesa number := 0;
  v_ing number := 0;  
BEGIN
for c_cP in (select distinct
                              NIVEL_SECUNDARIO,
                              PRED_NRO_PREDIO,
                              CONTROL_ANO_PROCESO,
                              CONTROL_MES_PROCESO,
                              CONTROL_TIPO_PROCESO,
                              DEPRECIACION,
                              DEPRECIACION_AJUSTADA,
                              NUEVA_ALICUOTA_ACUMULADA,
                              AXI_DEPRECIACION,
                              CUENTA_CONBI_DB,
                              CUENTA_CONBI_CR,
                              PREURBANIZACI_NRO_URBANIZACION,
                              CON_ID,
                              TIEMPO_DEP_AMO_CALCULADA,
                              VALOR_NETO,
                              TIPO_MOVIMIENTO,
                              TRANSACCION,
                              FECHA_INCORPORA
                         from conbi_dep_amo_pruebas3
                              /*where 
                              CONTROL_ANO_PROCESO = V_ANO and CONTROL_MES_PROCESO = V_MES 
                              and CONTROL_TIPO_PROCESO = 'AMO'
                         -- order by preurbanizaci_nro_urbanizacion asc, pred_nro_predio asc*/
                              )
                              
  loop
    v_tran := 1;
    /*Nro de movimientos del periodo consultado que afecten las cuentas de depreciacion*/
    select count(*)
        into v_nroM 
        from      conbi_movimientos cm
        where     cm.PREURBANIZACI_NRO_URBANIZACION = c_cP.PREURBANIZACI_NRO_URBANIZACION
              and cm.PRED_NRO_PREDIO = c_cP.PRED_NRO_PREDIO
              and cm.NIVEL_DESAGREGACION = c_cP.NIVEL_SECUNDARIO
              and to_char(cm.FECHA_MOVIMIENTO, 'yyyy') = v_ano   
              and to_char(cm.FECHA_MOVIMIENTO, 'mm') = v_mes
              and (cm.cuenta_conbi_db like '1710%' or cm.cuenta_conbi_db like '1720%' or cm.cuenta_conbi_cr like '1710%' or cm.cuenta_conbi_cr like '1720%')
              and cm.FECHA_MOVIMIENTO>=c_cP.FECHA_INCORPORA
              ;
        
      if v_nroM>1 then 
          /*Buscar todos los movimientos del predio para el periodo consultado*/
          for c_cM in (
            select  distinct
                                        cm.PREURBANIZACI_NRO_URBANIZACION,
                                        cm.PRED_NRO_PREDIO,
                                        cm.NIVEL_DESAGREGACION, 
                                        cm.CUENTA_CONBI_DB,
                                        cm.CUENTA_CONBI_CR,
                                        cm.VALOR_PREDIO,
                                        cm.depreciable,
                                        cm.TIPO_MOVIMIENTO,
                                        cm.TIPO_ACTO_JURIDICO,
                                        cm.USO_PREDIO,
                                        cm.USO_NIVEL1,
                                        cm.USO_NIVEL2,
                                        cm.TIPO_PROCEDENCIA,
                                        cm.TIPO_TRANSACCION,
                                        cm.CLASE_CUENTA,
                                        cm.FECHA_MOVIMIENTO
                        from      conbi_movimientos cm
                        where     cm.PREURBANIZACI_NRO_URBANIZACION = c_cP.PREURBANIZACI_NRO_URBANIZACION
                              and cm.PRED_NRO_PREDIO = c_cP.PRED_NRO_PREDIO
                              and cm.NIVEL_DESAGREGACION = c_cP.NIVEL_SECUNDARIO
                              and to_char(cm.FECHA_MOVIMIENTO, 'yyyy') = v_ano   
                              and to_char(cm.FECHA_MOVIMIENTO, 'mm') = v_mes
                              and cm.FECHA_MOVIMIENTO>=c_cP.FECHA_INCORPORA
                              order by cm.FECHA_MOVIMIENTO asc
                )
                loop 
                
                    begin  
                      if c_cP.TIPO_MOVIMIENTO = 'ING' then
                        v_movimiento := 'AMO';
                      end if;
                      if c_cM.TIPO_MOVIMIENTO = 'EGR' then
                        v_movimiento := 'EAMO';
                      end if;
                      
                      select CA.CUENTA_CONBI_DB, CA.CUENTA_CONBI_CR     
                      into v_cuenta_conbi_db, v_CUENTA_CONBI_CR
                      from CONBI_PR_PLAN_CONTABLE_ALTERNO CA 
                      where 
                      CA.USO_PREDIO=c_cM.USO_PREDIO 
                      and CA.USO_NIVEL1=c_cM.USO_NIVEL1 
                      and CA.USO_NIVEL2=c_cM.USO_NIVEL2 
                      and CA.TIPO_ACTO_JURIDICO=c_cM.TIPO_ACTO_JURIDICO 
                      and CA.TIPO_PROCEDENCIA=c_cM.TIPO_PROCEDENCIA
                      AND CA.CLASE_CUENTA= c_cM.CLASE_CUENTA 
                      and CA.TIPO_MOVIMIENTO=v_movimiento
                      ;
                    
                      select depreciacion, depreciacion_acumulada
                      into v_valor_alicuota_anterior, v_valor_alicuota_anterior_ac
                      from (select depreciacion, depreciacion_acumulada
                      from conbi_depreciacion_amortizaci1  
                      where 
                      PREURBANIZACI_NRO_URBANIZACION  = c_cP.PREURBANIZACI_NRO_URBANIZACION
                      and  PRED_NRO_PREDIO            = c_cP.PRED_NRO_PREDIO
                      and  NIVEL_SECUNDARIO           = c_cP.NIVEL_SECUNDARIO
                      and  control_tipo_proceso       = 'AMO'
                      order by control_ano_proceso desc, control_mes_proceso desc,con_id desc
                      ) where rownum = 1;
                      
                      
                      
                      if c_cM.TIPO_MOVIMIENTO = 'EGR' then
                        /*Insertar en la tabla el valor en cero*/
                              INSERT  into /*+ append */ conbi_depreciacion_amortizaci1
                                ( NIVEL_SECUNDARIO,
                                  PRED_NRO_PREDIO,
                                  CONTROL_ANO_PROCESO,
                                  CONTROL_MES_PROCESO,
                                  CONTROL_TIPO_PROCESO,
                                  DEPRECIACION,
                                  DEPRECIACION_AJUSTADA,
                                  DEPRECIACION_ACUMULADA,
                                  AXI_DEPRECIACION,
                                  CUENTA_CONBI_DB,
                                  CUENTA_CONBI_CR,
                                  PREURBANIZACI_NRO_URBANIZACION,
                                  CON_ID,
                                  TIEMPO_DEP_AMO_CALCULADA,
                                  VALOR_NETO,
                                  TIPO_MOVIMIENTO,
                                  TRANSACCION,
                                  VERSION,
                                  FECULTACT,
                                  VAL_TOTAL--,
                                  )
                                  values
                                  (
                                    c_cM.NIVEL_DESAGREGACION,
                                    c_cM.pred_nro_predio,
                                    c_cP.control_ano_proceso,        
                                    c_cP.control_mes_proceso,
                                    c_cP.control_tipo_proceso,
                                    v_valor_alicuota_anterior_ac, 
                                    0,        
                                    0,
                                    c_cP.axi_depreciacion,  
                                    v_cuenta_conbi_db,
                                    v_cuenta_conbi_cr,
                                    c_cP.preurbanizaci_nro_urbanizacion, 
                                    seq_conbi_amo_planb.nextval, 
                                    c_cP.tiempo_dep_amo_calculada,
                                    c_cP.valor_neto,
                                    c_cM.tipo_movimiento,
                                    v_tran,
                                    0,
                                    sysdate,
                                    0
                                  );
                                  v_tran := v_tran +1;
                      else
                        if v_ing = 0 then
                          INSERT  into /*+ append */ conbi_depreciacion_amortizaci1
                            ( NIVEL_SECUNDARIO,
                              PRED_NRO_PREDIO,
                              CONTROL_ANO_PROCESO,
                              CONTROL_MES_PROCESO,
                              CONTROL_TIPO_PROCESO,
                              DEPRECIACION,
                              DEPRECIACION_AJUSTADA,
                              DEPRECIACION_ACUMULADA,
                              AXI_DEPRECIACION,
                              CUENTA_CONBI_DB,
                              CUENTA_CONBI_CR,
                              PREURBANIZACI_NRO_URBANIZACION,
                              CON_ID,
                              TIEMPO_DEP_AMO_CALCULADA,
                              VALOR_NETO,
                              TIPO_MOVIMIENTO,
                              TRANSACCION,
                              VERSION,
                              FECULTACT,
                              VAL_TOTAL--,
                              )
                              values
                              (
                                c_cM.NIVEL_DESAGREGACION,
                                c_cP.pred_nro_predio,
                                c_cP.control_ano_proceso,        
                                c_cP.control_mes_proceso,
                                c_cP.control_tipo_proceso,
                                v_valor_alicuota_anterior, 
                                0,        
                                v_valor_alicuota_anterior,
                                c_cP.axi_depreciacion,  
                                v_cuenta_conbi_db,
                                v_cuenta_conbi_cr,
                                c_cP.preurbanizaci_nro_urbanizacion, 
                                seq_conbi_amo_planb.nextval,
                                c_cP.tiempo_dep_amo_calculada,
                                c_cP.valor_neto,
                                c_cM.TIPO_MOVIMIENTO,
                                v_tran,
                                0,
                                sysdate,
                                0
                              );
                              v_tran := v_tran +1;
                          v_ing := 1;
                        end if;
                      end if;
                      --commit;
                      exception
                       when no_data_found then
                          /*Primera vez que se calcula para un ingreso*/
                          INSERT  into /*+ append */ conbi_depreciacion_amortizaci1
                            ( NIVEL_SECUNDARIO,
                              PRED_NRO_PREDIO,
                              CONTROL_ANO_PROCESO,
                              CONTROL_MES_PROCESO,
                              CONTROL_TIPO_PROCESO,
                              DEPRECIACION,
                              DEPRECIACION_AJUSTADA,
                              DEPRECIACION_ACUMULADA,
                              AXI_DEPRECIACION,
                              CUENTA_CONBI_DB,
                              CUENTA_CONBI_CR,
                              PREURBANIZACI_NRO_URBANIZACION,
                              CON_ID,
                              TIEMPO_DEP_AMO_CALCULADA,
                              VALOR_NETO,
                              TIPO_MOVIMIENTO,
                              TRANSACCION,
                              VERSION,
                              FECULTACT,
                              VAL_TOTAL--,
                              )
                              values
                              (
                                c_cM.NIVEL_DESAGREGACION,
                                c_cP.pred_nro_predio,
                                c_cP.control_ano_proceso,        
                                c_cP.control_mes_proceso,
                                c_cP.control_tipo_proceso,
                                round((c_cM.VALOR_PREDIO/600),1),
                                0,        
                                round((c_cM.VALOR_PREDIO/600),1),
                                c_cP.axi_depreciacion,  
                                v_cuenta_conbi_db,
                                v_cuenta_conbi_cr,
                                c_cP.preurbanizaci_nro_urbanizacion, 
                                seq_conbi_amo_planb.nextval,
                                1,
                                c_cM.VALOR_PREDIO,
                                c_cM.TIPO_MOVIMIENTO,
                                v_tran,
                                0,
                                sysdate,
                                0
                              );
                          --commit;
                          v_tran := v_tran +1;
                     end; 
                end loop;
    end if;  
    INSERT  into /*+ append */ conbi_depreciacion_amortizaci1
                ( NIVEL_SECUNDARIO,
                  PRED_NRO_PREDIO,
                  CONTROL_ANO_PROCESO,
                  CONTROL_MES_PROCESO,
                  CONTROL_TIPO_PROCESO,
                  DEPRECIACION,
                  DEPRECIACION_AJUSTADA,
                  DEPRECIACION_ACUMULADA,
                  AXI_DEPRECIACION,
                  CUENTA_CONBI_DB,
                  CUENTA_CONBI_CR,
                  PREURBANIZACI_NRO_URBANIZACION,
                  CON_ID,
                  TIEMPO_DEP_AMO_CALCULADA,
                  VALOR_NETO,
                  TIPO_MOVIMIENTO,
                  TRANSACCION,
                  VERSION,
                  FECULTACT,
                  VAL_TOTAL--,
                  )
                  values
                  (
                  c_cP.NIVEL_SECUNDARIO,
                  c_cP.PRED_NRO_PREDIO,
                  c_cP.CONTROL_ANO_PROCESO,
                  c_cP.CONTROL_MES_PROCESO,
                  c_cP.CONTROL_TIPO_PROCESO,
                  c_cP.DEPRECIACION,
                  c_cP.DEPRECIACION_AJUSTADA,
                  c_cP.NUEVA_ALICUOTA_ACUMULADA,
                  c_cP.AXI_DEPRECIACION,
                  c_cP.CUENTA_CONBI_DB,
                  c_cP.CUENTA_CONBI_CR,
                  c_cP.PREURBANIZACI_NRO_URBANIZACION,
                  seq_conbi_amo_planb.nextval,
                  c_cP.TIEMPO_DEP_AMO_CALCULADA,
                  c_cP.VALOR_NETO,
                  c_cP.TIPO_MOVIMIENTO,
                  c_cP.TRANSACCION,
                  0,
                  sysdate,
                  0
                  );
  end loop;
  
  /*Periodo inmediatamente anterior, para predios que tienen solo egresos en el mes*/
if  v_mes = '01' then
  v_mes1 := '12';
  v_ano1 := to_char(to_number(v_ano,9999)-1);
else
  v_mesa := to_number(v_mes,99)-1;
  if v_mesa < 10 then
    v_mes1 := '0' || to_char(v_mesa);
  else
    v_mes1 := to_char(v_mesa);
  end if;
  v_ano1 := v_ano;
end if;
    
    for c_cA in (
                select distinct
                      cp.NIVEL_SECUNDARIO,
                      cp.PRED_NRO_PREDIO,
                      cp.CONTROL_ANO_PROCESO,
                      cp.CONTROL_MES_PROCESO,
                      cp.CONTROL_TIPO_PROCESO,
                      cp.DEPRECIACION,
                      cp.DEPRECIACION_AJUSTADA,
                      cp.DEPRECIACION_ACUMULADA,
                      cp.AXI_DEPRECIACION,
                      cp.CUENTA_CONBI_DB,
                      cp.CUENTA_CONBI_CR,
                      cp.PREURBANIZACI_NRO_URBANIZACION,
                      cp.CON_ID,
                      cp.TIEMPO_DEP_AMO_CALCULADA,
                      cp.VALOR_NETO,
                      --cp.TIPO_MOVIMIENTO,
                      cp.TRANSACCION,
                      --cm1.TIPO_MOVIMIENTO,
                      cm1.TIPO_ACTO_JURIDICO,
                      cm1.USO_PREDIO,
                      cm1.USO_NIVEL1,
                      cm1.USO_NIVEL2,
                      cm1.TIPO_PROCEDENCIA,
                      cm1.TIPO_TRANSACCION,
                      cm1.CLASE_CUENTA
                from 
                      conbi_depreciacion_amortizaci1 cp
                      left  join conbi_movimientos cm1 on cm1.preurbanizaci_nro_urbanizacion = cp.preurbanizaci_nro_urbanizacion and cp.PRED_NRO_PREDIO = cm1.PRED_NRO_PREDIO
                      and   cm1.nivel_desagregacion = cp.nivel_secundario
                where CONTROL_ANO_PROCESO = v_ano1   
                and   CONTROL_MES_PROCESO = v_mes1
                and   TO_CHAR(cm1.fec_mov,'YYYYMM') = v_ano || v_mes
                and   cm1.tipo_movimiento = 'EGR'
                and cp.control_tipo_proceso='AMO'
                and   (cm1.cuenta_conbi_db like '1710%' or cm1.cuenta_conbi_db like '1720%' )
    )
    loop 
        select count(*)
        into    v_anterior
        from    conbi_dep_amo_pruebas3 cp
        where 
              cp.PREURBANIZACI_NRO_URBANIZACION = c_cA.PREURBANIZACI_NRO_URBANIZACION
        and   cp.PRED_NRO_PREDIO = c_cA.PRED_NRO_PREDIO
        and   cp.nivel_secundario = c_cA.NIVEL_SECUNDARIO;

        if v_anterior = 0 then 
                
          select CA.CUENTA_CONBI_DB, CA.CUENTA_CONBI_CR     
          into v_CUENTA_CONBI_DB, v_CUENTA_CONBI_CR
          from CONBI_PR_PLAN_CONTABLE_ALTERNO CA
          where 
          CA.USO_PREDIO=c_cA.USO_PREDIO 
          and CA.USO_NIVEL1=c_cA.USO_NIVEL1 
          and CA.USO_NIVEL2=c_cA.USO_NIVEL2 
          and CA.TIPO_ACTO_JURIDICO=c_cA.TIPO_ACTO_JURIDICO 
          and CA.TIPO_PROCEDENCIA=c_cA.TIPO_PROCEDENCIA
          AND CA.CLASE_CUENTA= c_cA.CLASE_CUENTA 
          and CA.TIPO_MOVIMIENTO='EAMO';
                    
          INSERT  into /*+ append */ conbi_depreciacion_amortizaci1
                              ( NIVEL_SECUNDARIO,
                                PRED_NRO_PREDIO,
                                CONTROL_ANO_PROCESO,
                                CONTROL_MES_PROCESO,
                                CONTROL_TIPO_PROCESO,
                                DEPRECIACION,
                                DEPRECIACION_AJUSTADA,
                                DEPRECIACION_ACUMULADA,
                                AXI_DEPRECIACION,
                                CUENTA_CONBI_DB,
                                CUENTA_CONBI_CR,
                                PREURBANIZACI_NRO_URBANIZACION,
                                CON_ID,
                                TIEMPO_DEP_AMO_CALCULADA,
                                VALOR_NETO,
                                TIPO_MOVIMIENTO,
                                TRANSACCION,
                                VERSION,
                                FECULTACT,
                                VAL_TOTAL
                                )
                                values
                                (
                                  c_cA.nivel_secundario,
                                  c_cA.pred_nro_predio,
                                  v_ano,
                                  v_mes,
                                  c_cA.control_tipo_proceso,
                                  c_cA.DEPRECIACION_ACUMULADA, 
                                  0,        
                                  0,
                                  c_cA.axi_depreciacion,  
                                  v_CUENTA_CONBI_DB,
                                  v_cuenta_conbi_cr,
                                  c_cA.preurbanizaci_nro_urbanizacion, 
                                  seq_conbi_amo_planb.nextval, 
                                  c_cA.tiempo_dep_amo_calculada,
                                  c_cA.valor_neto,
                                  'EGR',
                                  v_tran,
                                  0,
                                  sysdate,
                                  0
                                );     
          v_tran := v_tran +1;
        end if;    
    end loop;

INSERT INTO CONBI_CONTROL_PROCESOS values ('AMO',V_ANO, V_MES, 'F', sysdate, 0);
execute immediate 'truncate table conbi_dep_amo_pruebas1';

commit;
--commit;
END;