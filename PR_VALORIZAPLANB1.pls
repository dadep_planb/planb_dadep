create or replace procedure pr_valorizaplanb1 (V_ANO  VARCHAR2, V_MES  VARCHAR2 ) 
AS

avaluo_mes              number:= 0;
v_valorizacion_mes      number:= 0;
v_acumulada_mes         number:= 0;
cuenta_conbi_db         number:= 0;
cuenta_conbi_cr         number:= 0;
fecha                   number:= 0;
v_suma_ingresos         number:= 0;
v_suma_egresos          number:= 0;
v_cuenta_conbi_db       number:= 0;
v_cuenta_conbi_cr       number:= 0;
v_depreciacion          number:= 0;

begin
delete from conbi_dep_amo_pruebas2; 
commit;

    -------------------------------------------------------------------------------
    /*   CONSULTA PARA LOS PREDIOS INCORPORADOS HASTA LA FECHA DEL ULTIMO CORTE  */
    /*   ALMACENA EN UNA TABLA TEMORAL POR SESION                                */
    -------------------------------------------------------------------------------
    for c_cv in (
                select  cm.PREURBANIZACI_NRO_URBANIZACION,
                        cm.PRED_NRO_PREDIO,  
                        cm.tipo_procedencia,
                        cm.tipo_acto_juridico, 
                        cm.uso_nivel1, 
                        cm.uso_nivel2, 
                        cm.uso_predio,
                        ae.fecha_avaluo,
                        ae.valor_avaluo,
                        cm.valor_predio,
                        cm.nivel_desagregacion,
                        (select max(mm.fecha_movimiento)
                           from   CONBI_MOVIMIENTOS mm 
                          where   CM.PREURBANIZACI_NRO_URBANIZACION = MM.PREURBANIZACI_NRO_URBANIZACION
                            and   CM.PRED_NRO_PREDIO = MM.PRED_NRO_PREDIO
                            and   CM.NIVEL_DESAGREGACION = MM.NIVEL_DESAGREGACION
                            and   ( to_char(MM.FECHA_MOVIMIENTO, 'yyyy') < v_ano or --'1994'  or 
                                  (to_char(MM.FECHA_MOVIMIENTO, 'yyyy') = v_ano )--'1994' )
                                  and    to_char(MM.FECHA_MOVIMIENTO, 'mm')< v_mes )--'01')
                                  ) as fecha,
                        (select cd.costo_historico 
                           from conbi_datos_contables cd 
                          where 
                                cd.PREURBANIZACI_NRO_URBANIZACION = cm.PREURBANIZACI_NRO_URBANIZACION 
                            and cd.PRED_NRO_PREDIO = cm.PRED_NRO_PREDIO 
                            and cd.nivel_desagregacion = cm.nivel_desagregacion) as costo_historico,
                        (select da.depreciacion_acumulada
                             from conbi_depreciacion_amortizacio da  
                          where 
                                da.PREURBANIZACI_NRO_URBANIZACION = cm.PREURBANIZACI_NRO_URBANIZACION 
                            and da.PRED_NRO_PREDIO      = cm.PRED_NRO_PREDIO 
                            and da.nivel_secundario  = cm.nivel_desagregacion
                            and da.control_ano_proceso = v_ano
                            and da.control_mes_proceso = v_mes
                            )  as depreciacion
                        from conbi_movimientos cm
                        inner join  RUPI_AVALUO_EDIFICACIONES ae on cm.PREURBANIZACI_NRO_URBANIZACION = ae.PREURBANIZACI_NRO_URBANIZACION and ae.PRED_NRO_PREDIO = cm.PRED_NRO_PREDIO 
                        where to_char(ae.fecha_avaluo,'yyyy')= v_ano--'2005' -- '2010'
                        and   to_char(ae.fecha_avaluo,'mm') = v_mes--'01' 
                        and   cm.FECHA_MOVIMIENTO = (   select max(mm.fecha_movimiento)
                                                          from   CONBI_MOVIMIENTOS mm 
                                                         where   CM.PREURBANIZACI_NRO_URBANIZACION = MM.PREURBANIZACI_NRO_URBANIZACION
                                                           and   CM.PRED_NRO_PREDIO = MM.PRED_NRO_PREDIO
                                                           and   CM.NIVEL_DESAGREGACION = MM.NIVEL_DESAGREGACION
                                                           and   ( to_char(MM.FECHA_MOVIMIENTO, 'yyyy') < v_ano or --'1994'  or 
                                                                 (to_char(MM.FECHA_MOVIMIENTO, 'yyyy') = v_ano )--'1994' )
                                                                 and    to_char(MM.FECHA_MOVIMIENTO, 'mm') < v_mes )--'01')
                                                                 )
                        and cm.cuenta_conbi_db not like '17%'
                        and cm.clase_cuenta = 'B' and cm.uso_predio = 1
                        
                        order by 1,2
                        ) 
    
    
    loop 
        begin
                    select nvl(sum(cm.VALOR_PREDIO),0)
                    into v_suma_ingresos
                    from    CONBI_MOVIMIENTOS CM 
                    where CM.PREURBANIZACI_NRO_URBANIZACION = c_cv.PREURBANIZACI_NRO_URBANIZACION
                    and   CM.PRED_NRO_PREDIO = c_cv.PRED_NRO_PREDIO
                    and   CM.NIVEL_DESAGREGACION = c_cv.NIVEL_DESAGREGACION
                    and   cm.tipo_movimiento = 'ING'
                    and   (cm.CUENTA_CONBI_DB like '1640%' or cm.CUENTA_CONBI_CR like '1640%')
                    and   CM.FECHA_MOVIMIENTO <('01/jul/2013')  ;
                    
                   select   nvl(sum(cm.VALOR_PREDIO) ,0)
                     into   v_suma_egresos
                     from   CONBI_MOVIMIENTOS CM
                    where   CM.PREURBANIZACI_NRO_URBANIZACION = c_cv.PREURBANIZACI_NRO_URBANIZACION
                      and   CM.PRED_NRO_PREDIO = c_cv.PRED_NRO_PREDIO
                      and   CM.NIVEL_DESAGREGACION = c_cv.NIVEL_DESAGREGACION
                      and   cm.tipo_movimiento = 'EGR' and cm.tipo_transaccion = 2
                      and   (cm.CUENTA_CONBI_DB like '1640%' or cm.CUENTA_CONBI_CR like '1640%')
                      and   CM.FECHA_MOVIMIENTO <('01/jul/2013')
                    ;
                    
                    
                    if  (v_suma_ingresos-v_suma_egresos) > 0 then
                    
                        begin 
                            v_valorizacion_mes := 0;
                            v_acumulada_mes    := 0;
                            
                            if c_cv.depreciacion is NULL then
                              v_depreciacion := 0;
                            else
                              v_depreciacion := c_cv.depreciacion;
                            end if;
                                    
                           select   da.depreciacion_acumulada
                             into   v_acumulada_mes
                             from   conbi_depreciacion_amortizacio da
                            where   da.PREURBANIZACI_NRO_URBANIZACION = c_cv.PREURBANIZACI_NRO_URBANIZACION
                              and   da.PRED_NRO_PREDIO = c_cv.PRED_NRO_PREDIO
                              and   da.NIVEL_secundario = c_cv.NIVEL_DESAGREGACION
                              and   da.control_ano_proceso = v_ano
                              and   da.control_mes_proceso = v_mes
                              ;
                              
                              v_valorizacion_mes := c_cv.valor_avaluo + (c_cv.costo_historico - c_cv.depreciacion + v_valorizacion_mes);
                              v_acumulada_mes := v_valorizacion_mes ;
    
                          exception
                             when   no_data_found then
                                    v_valorizacion_mes := c_cv.valor_avaluo - (c_cv.costo_historico + v_depreciacion);
                                    v_acumulada_mes := v_valorizacion_mes;
                        end;  
    
                        if  v_valorizacion_mes < 0 THEN 
                        
                              select  CA.CUENTA_CONBI_DB, CA.CUENTA_CONBI_CR     
                              into    v_CUENTA_CONBI_DB, v_CUENTA_CONBI_CR
                              from    CONBI_PR_PLAN_CONTABLE_ALTERNO CA 
                              where 
                                  CA.USO_PREDIO=c_cv.USO_PREDIO 
                              and CA.USO_NIVEL1=c_cv.USO_NIVEL1 
                              and CA.USO_NIVEL2=c_cv.USO_NIVEL2 
                              and CA.TIPO_ACTO_JURIDICO=c_cv.TIPO_ACTO_JURIDICO 
                              and CA.TIPO_PROCEDENCIA=c_cv.TIPO_PROCEDENCIA
                              AND CA.CLASE_CUENTA= 'B' 
                              and CA.TIPO_MOVIMIENTO='EDESV';
                       else         
                              select  CA.CUENTA_CONBI_DB, CA.CUENTA_CONBI_CR     
                              into    v_CUENTA_CONBI_DB, v_CUENTA_CONBI_CR
                              from    CONBI_PR_PLAN_CONTABLE_ALTERNO CA 
                              where 
                                  CA.USO_PREDIO=c_cv.USO_PREDIO 
                              and CA.USO_NIVEL1=c_cv.USO_NIVEL1 
                              and CA.USO_NIVEL2=c_cv.USO_NIVEL2 
                              and CA.TIPO_ACTO_JURIDICO=c_cv.TIPO_ACTO_JURIDICO 
                              and CA.TIPO_PROCEDENCIA=c_cv.TIPO_PROCEDENCIA
                              AND CA.CLASE_CUENTA= 'B' 
                              and CA.TIPO_MOVIMIENTO='VAL';
                       end if;
                  
                        INSERT INTO CONBI_DEP_AMO_PRUEBAS2(
                                                  preurbanizaci_nro_urbanizacion, 
                                                  pred_nro_predio,
                                                  NIVEL_SECUNDARIO,
                                                  CONTROL_ANO_VALORIZACION,        
                                                  CONTROL_MES_VALORIZACION,
                                                  control_tipo_proceso,               --  AMO
                                                  DEPRECIACION, 
                                                  CUENTA_CONBI_DB,                    --  VALOR_PREDIO  
                                                  CUENTA_CONBI_CR,
                                                  VALOR_AVALUO,
                                                  COSTO_HISTORICO,
                                                  valorizacion_mes,                   /* Al�. Acumulada*/
                                                  acumulada_mes
                                                  )
                        VALUES    (
                                  c_cv.PREURBANIZACI_NRO_URBANIZACION,     
                                  c_cv.PRED_NRO_PREDIO,                    
                                  c_cv.nivel_desagregacion,                
                                  v_ano,           
                                  v_mes,
                                  'VAL',
                                  0,
                                  v_CUENTA_CONBI_DB,                       
                                  v_CUENTA_CONBI_CR,
                                  c_cv.valor_avaluo,
                                  c_cv.costo_historico,
                                  v_valorizacion_mes,
                                  v_acumulada_mes
                                  );
                  end if;
               end; 
                  
              
          
               
               
               
    end loop;
    DELETE tmp_valorizacion;
    commit;
end;