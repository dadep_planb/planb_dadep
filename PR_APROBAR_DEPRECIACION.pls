create or replace PROCEDURE "PR_APROBAR_DEPRECIACION" (V_ANO VARCHAR2, V_MES VARCHAR2)AS 
 v_cuenta_conbi_db  number := 0;
 v_cuenta_conbi_cr  number := 0;
 v_movimiento       varchar2(10) := '';
 v_ano1             varchar2(10) := 0;
 v_mes1             varchar2(10) := 0;
 v_valor_alicuota_anterior_ac number := 0;
 v_valor_alicuota_anterior    number := 0;
 v_tran             number := 0;
 v_nroM             number := 0;
 v_anterior         number := 0;
 v_mesa             number := 0;
 v_alicuota_traslado     number := 0;
 v_valor_predio_traslado number := 0;
 v_ing              number := 0;          
 v_un_egreso        number :=0;
BEGIN

for c_c in (select  preurbanizaci_nro_urbanizacion, 
                    pred_nro_predio,
                    NIVEL_SECUNDARIO,
                    DEPRECIACION,   
                    DEPRECIACION_ACUMULADA,
                    DEPRECIACION_AJUSTADA,        
                    axi_depreciacion,  
                    vida_util,                    
                    tvidautilsindepreciar,        
                    tiempo_dep_amo_calculada,     
                    control_ano_proceso,        
                    control_mes_proceso,
                    control_tipo_proceso,         
                    valor_neto,                   
                    TRANSACCION,
                    TIPO_MOVIMIENTO,
                    CON_ID,
                    FECHA_INCORPORA,
                    COSTO_HISTORICO,
                    NUEVA_ALICUOTA_ACUMULADA,       
                    VALOR_ALICUOTA_ANT,             
                    ALICUOTA_ANTERIOR,
                    cuenta_conbi_db,
                    cuenta_conbi_cr,
                    CUENTA_MOVIMIENTO,
                    USO_PREDIO,
                    USO_NIVEL1,
                    USO_NIVEL2,
                    TIPO_ACTO_JURIDICO,
                    TIPO_PROCEDENCIA,
                    CLASE_CUENTA
             from   conbi_dep_amo_pruebas
             where 
                    CONTROL_ANO_PROCESO = V_ANO and CONTROL_MES_PROCESO = V_MES 
             and    CONTROL_TIPO_PROCESO = 'DEP'
                    /*PROBAR PREDIO ERROR 2013 - 10 */
                    /*and PREURBANIZACI_NRO_URBANIZACION = 2
                    and PRED_NRO_PREDIO = 564
                    and NIVEL_SECUNDARIO = 2*/
                  
             order by preurbanizaci_nro_urbanizacion,pred_nro_predio
                  )
                              
  loop
    v_tran := 1;
    v_ing  := 0;
    /*Nro de movimientos del periodo consultado*/
      select  count(*)
      into    v_nroM 
      from    conbi_movimientos cm
      where   cm.PREURBANIZACI_NRO_URBANIZACION = c_c.PREURBANIZACI_NRO_URBANIZACION
      and     cm.PRED_NRO_PREDIO      = c_c.PRED_NRO_PREDIO
      and     cm.NIVEL_DESAGREGACION  = c_c.NIVEL_SECUNDARIO
      and     to_char(cm.FECHA_MOVIMIENTO, 'yyyy') = v_ano   
      and     to_char(cm.FECHA_MOVIMIENTO, 'mm')   = v_mes
      and     cm.FECHA_MOVIMIENTO >= c_c.FECHA_INCORPORA;
        
    if v_nroM>1 then 
        /*Buscar todos los movimientos del predio para el periodo consultado*/
        for c_cM in (
                    select  
                                cm.PREURBANIZACI_NRO_URBANIZACION,
                                cm.PRED_NRO_PREDIO,
                                cm.NIVEL_DESAGREGACION, 
                                cm.CUENTA_CONBI_DB,
                                cm.CUENTA_CONBI_CR,
                                cm.VALOR_PREDIO,
                                cm.depreciable,
                                cm.TIPO_MOVIMIENTO,
                                cm.TIPO_ACTO_JURIDICO,
                                cm.USO_PREDIO,
                                cm.USO_NIVEL1,
                                cm.USO_NIVEL2,
                                cm.TIPO_PROCEDENCIA,
                                cm.TIPO_TRANSACCION,
                                cm.CLASE_CUENTA,
                                cm.FECHA_MOVIMIENTO
                      from      conbi_movimientos cm
                      where     cm.PREURBANIZACI_NRO_URBANIZACION = c_c.PREURBANIZACI_NRO_URBANIZACION
                      and       cm.PRED_NRO_PREDIO = c_c.PRED_NRO_PREDIO
                      and       cm.NIVEL_DESAGREGACION = c_c.NIVEL_SECUNDARIO
                      and       to_char(cm.FECHA_MOVIMIENTO, 'yyyy') = v_ano   
                      and       to_char(cm.FECHA_MOVIMIENTO, 'mm') = v_mes
                      and       cm.FECHA_MOVIMIENTO>=c_c.FECHA_INCORPORA
                      and       (cm.cuenta_conbi_db like '1640%' or cm.cuenta_conbi_db like '1920%' or cuenta_conbi_db like '31050303%' or cm.cuenta_conbi_cr like '1640%' or cm.cuenta_conbi_cr like '1920%' or cuenta_conbi_cr like '31050303%')
                      and       cm.cuenta_conbi_db not like '19200633%'
                      and       cm.cuenta_conbi_cr not like '19200633%'
                      /*and       rownum < (select max(rownum) from conbi_movimientos cm1
                                          where cm1.PREURBANIZACI_NRO_URBANIZACION = c_c.PREURBANIZACI_NRO_URBANIZACION
                                          and   cm1.PRED_NRO_PREDIO = c_c.PRED_NRO_PREDIO
                                          and   cm1.NIVEL_DESAGREGACION = c_c.NIVEL_SECUNDARIO
                                          and   to_char(cm1.FECHA_MOVIMIENTO, 'yyyy') = v_ano   
                                          and   to_char(cm1.FECHA_MOVIMIENTO, 'mm') = v_mes
                                          and   cm1.FECHA_MOVIMIENTO>=c_c.FECHA_INCORPORA
                                          )*/
                      order by cm.FECHA_MOVIMIENTO asc
              )
          loop 
            begin  
              if  c_cM.TIPO_MOVIMIENTO = 'ING' then
                  v_movimiento := 'DEP';
              end if;
              if  c_cM.TIPO_MOVIMIENTO = 'EGR' then
                  v_movimiento := 'EDEP';
              end if;
                    
              select DISTINCT CA.CUENTA_CONBI_DB, CA.CUENTA_CONBI_CR     
              into    v_cuenta_conbi_db, v_cuenta_conbi_cr
              from    CONBI_PR_PLAN_CONTABLE_ALTERNO CA 
              where   CA.USO_PREDIO = c_cM.USO_PREDIO 
                and   CA.USO_NIVEL1 = c_cM.USO_NIVEL1 
                and   CA.USO_NIVEL2 = c_cM.USO_NIVEL2 
                and   CA.TIPO_ACTO_JURIDICO = c_cM.TIPO_ACTO_JURIDICO 
                and   CA.TIPO_PROCEDENCIA = c_cM.TIPO_PROCEDENCIA
                and   CA.CLASE_CUENTA = c_cM.CLASE_CUENTA 
                and   CA.TIPO_MOVIMIENTO = v_movimiento
              ;
              
              select  depreciacion, depreciacion_acumulada
              into    v_valor_alicuota_anterior, v_valor_alicuota_anterior_ac
              from (
                    select  depreciacion, depreciacion_acumulada
                    from    conbi_depreciacion_amortizacio  
                    where   PREURBANIZACI_NRO_URBANIZACION  = c_c.PREURBANIZACI_NRO_URBANIZACION
                      and   PRED_NRO_PREDIO            = c_c.PRED_NRO_PREDIO
                      and   NIVEL_SECUNDARIO           = c_c.NIVEL_SECUNDARIO
                      and   control_tipo_proceso       = 'DEP'
                      order by control_ano_proceso desc, control_mes_proceso desc, con_id desc
                    ) 
              where rownum = 1;
              
              if c_cM.TIPO_MOVIMIENTO = 'EGR' then
                /*Insertar en la tabla el valor en cero y aculumado todo lo que lleva*/
                INSERT  into conbi_depreciacion_amortizacio
                        ( NIVEL_SECUNDARIO,
                          PRED_NRO_PREDIO,
                          CONTROL_ANO_PROCESO,
                          CONTROL_MES_PROCESO,
                          CONTROL_TIPO_PROCESO,
                          DEPRECIACION,
                          DEPRECIACION_AJUSTADA,
                          DEPRECIACION_ACUMULADA,
                          AXI_DEPRECIACION,
                          CUENTA_CONBI_DB,
                          CUENTA_CONBI_CR,
                          PREURBANIZACI_NRO_URBANIZACION,
                          CON_ID,
                          TIEMPO_DEP_AMO_CALCULADA,
                          VALOR_NETO,
                          TIPO_MOVIMIENTO,
                          TRANSACCION,
                          VERSION,
                          FECULTACT,
                          VAL_TOTAL--,
                          )
                          values
                          (
                            c_cM.NIVEL_DESAGREGACION,
                            c_cM.pred_nro_predio,
                            c_c.control_ano_proceso,        
                            c_c.control_mes_proceso,
                            c_c.control_tipo_proceso,
                            v_valor_alicuota_anterior_ac, 
                            0,        
                            0,
                            c_c.axi_depreciacion,  
                            v_cuenta_conbi_db,
                            v_cuenta_conbi_cr,
                            c_c.preurbanizaci_nro_urbanizacion, 
                            seq_conbi_amo_planb.nextval, 
                            c_c.tiempo_dep_amo_calculada,
                            c_c.valor_neto,
                            c_cM.tipo_movimiento,
                            v_tran,
                            0,
                            sysdate,
                            0
                          );
                v_tran := v_tran +1;
              /*else--Si es un ingreso
                --Preguntar si el predio viene de traslado y guardar el movimiento
                if v_ing = 0 then
                  INSERT into conbi_depreciacion_amortizacio
                      ( NIVEL_SECUNDARIO,
                        PRED_NRO_PREDIO,
                        CONTROL_ANO_PROCESO,
                        CONTROL_MES_PROCESO,
                        CONTROL_TIPO_PROCESO,
                        DEPRECIACION,
                        DEPRECIACION_AJUSTADA,
                        DEPRECIACION_ACUMULADA,
                        AXI_DEPRECIACION,
                        CUENTA_CONBI_DB,
                        CUENTA_CONBI_CR,
                        PREURBANIZACI_NRO_URBANIZACION,
                        CON_ID,
                        TIEMPO_DEP_AMO_CALCULADA,
                        VALOR_NETO,
                        TIPO_MOVIMIENTO,
                        TRANSACCION,
                        VERSION,
                        FECULTACT,
                        VAL_TOTAL--,
                        )
                        values
                        (
                          c_cM.NIVEL_DESAGREGACION,
                          c_c.pred_nro_predio,
                          c_c.control_ano_proceso,        
                          c_c.control_mes_proceso,
                          c_c.control_tipo_proceso,
                          v_valor_alicuota_anterior, 
                          0,        
                          v_valor_alicuota_anterior,
                          c_c.axi_depreciacion,  
                          v_cuenta_conbi_db,
                          v_cuenta_conbi_cr,
                          c_c.preurbanizaci_nro_urbanizacion, 
                          seq_conbi_amo_planb.nextval,
                          c_c.tiempo_dep_amo_calculada,
                          c_c.valor_neto,
                          c_cM.TIPO_MOVIMIENTO,
                          v_tran,
                          0,
                          sysdate,
                          0
                        );
                  v_tran := v_tran +1;
                  v_ing := 1;
                end if;*/
              end if;
            exception
            when no_data_found then
              begin
                /*Predios Trasladados*/
                select  sum(cm.alicuota_traslado), sum(cm.valor_predio), MIN(cm.fecha_movimiento)
                into    v_alicuota_traslado, v_valor_predio_traslado, v_movimiento 
                from    conbi_movimientos cm 
                where   cuenta_conbi_db like '31050303%'
                and     cm.tipo_movimiento= 'ING' 
                and     cm.preurbanizaci_nro_urbanizacion = c_c.preurbanizaci_nro_urbanizacion
                and     cm.pred_nro_predio = c_c.pred_nro_predio
                and     to_char(cm.fecha_movimiento,'yyyymm') = to_char(c_c.FECHA_INCORPORA,'yyyymm') 
                group by 'empty set of columns';
               
                /*Insertar la depreciacion acumulada - Traslado*/
                INSERT  into conbi_depreciacion_amortizacio
                            ( NIVEL_SECUNDARIO,
                              PRED_NRO_PREDIO,
                              CONTROL_ANO_PROCESO,
                              CONTROL_MES_PROCESO,
                              CONTROL_TIPO_PROCESO,
                              DEPRECIACION,
                              DEPRECIACION_AJUSTADA,
                              DEPRECIACION_ACUMULADA,
                              AXI_DEPRECIACION,
                              CUENTA_CONBI_DB,
                              CUENTA_CONBI_CR,
                              PREURBANIZACI_NRO_URBANIZACION,
                              CON_ID,
                              TIEMPO_DEP_AMO_CALCULADA,
                              VALOR_NETO,
                              TIPO_MOVIMIENTO,
                              TRANSACCION,
                              VERSION,
                              FECULTACT,
                              VAL_TOTAL--,
                              )
                              values
                              (
                                c_c.NIVEL_SECUNDARIO,
                                c_c.pred_nro_predio,
                                c_c.control_ano_proceso,        
                                c_c.control_mes_proceso,
                                c_c.control_tipo_proceso,
                                0, 
                                c_c.DEPRECIACION_AJUSTADA,        
                                v_valor_predio_traslado, --Depreciacion acumulada traslado
                                c_c.axi_depreciacion,  
                                c_c.cuenta_conbi_db,
                                c_c.cuenta_conbi_cr,
                                c_c.preurbanizaci_nro_urbanizacion, 
                                seq_conbi_amo_planb.nextval,
                                v_alicuota_traslado, --Nro alicuotas traslado
                                c_c.valor_neto,
                                'ING',
                                v_tran,
                                0,
                                sysdate,
                                0
                              );
                v_tran := v_tran +1;
              exception when no_data_found then
                /*Primera vez que se calcula para un ingreso*/
                v_ing := 0;
              end;
            end; 
          end loop;
    /*else 
      if v_nroM=1 then
            for c_cM in (
                    select  
                              cm.PREURBANIZACI_NRO_URBANIZACION,
                              cm.PRED_NRO_PREDIO,
                              cm.NIVEL_DESAGREGACION, 
                              cm.CUENTA_CONBI_DB,
                              cm.CUENTA_CONBI_CR,
                              cm.VALOR_PREDIO,
                              cm.depreciable,
                              cm.TIPO_MOVIMIENTO,
                              cm.TIPO_ACTO_JURIDICO,
                              cm.USO_PREDIO,
                              cm.USO_NIVEL1,
                              cm.USO_NIVEL2,
                              cm.TIPO_PROCEDENCIA,
                              cm.TIPO_TRANSACCION,
                              cm.CLASE_CUENTA,
                              cm.FECHA_MOVIMIENTO
                      from    conbi_movimientos cm
                      where   cm.PREURBANIZACI_NRO_URBANIZACION = c_c.PREURBANIZACI_NRO_URBANIZACION
                      and     cm.PRED_NRO_PREDIO      = c_c.PRED_NRO_PREDIO
                      and     cm.NIVEL_DESAGREGACION  = c_c.NIVEL_SECUNDARIO
                      and     to_char(cm.FECHA_MOVIMIENTO, 'yyyy')  = v_ano   
                      and     to_char(cm.FECHA_MOVIMIENTO, 'mm')    = v_mes
                      and       (cm.cuenta_conbi_db like '1640%' or cm.cuenta_conbi_db like '1920%' or cm.cuenta_conbi_cr like '1640%' or cm.cuenta_conbi_cr like '1920%')
                      and       cm.cuenta_conbi_db not like '19200633%'
                      and       cm.cuenta_conbi_cr not like '19200633%'
                      order by cm.FECHA_MOVIMIENTO asc
              )
          loop
            begin
              if  c_cM.TIPO_MOVIMIENTO = 'EGR' then
                  select  DISTINCT CA.CUENTA_CONBI_DB, CA.CUENTA_CONBI_CR     
                  into    v_cuenta_conbi_db,  v_cuenta_conbi_cr
                  from    CONBI_PR_PLAN_CONTABLE_ALTERNO CA
                  where 
                      CA.USO_PREDIO = c_cM.USO_PREDIO 
                  and CA.USO_NIVEL1 = c_cM.USO_NIVEL1 
                  and CA.USO_NIVEL2 = c_cM.USO_NIVEL2 
                  and CA.TIPO_ACTO_JURIDICO = c_cM.TIPO_ACTO_JURIDICO 
                  and CA.TIPO_PROCEDENCIA   = c_cM.TIPO_PROCEDENCIA
                  AND CA.CLASE_CUENTA       = c_cM.CLASE_CUENTA 
                  and CA.TIPO_MOVIMIENTO    = 'EDEP';
                          
                INSERT  into conbi_depreciacion_amortizacio
                ( NIVEL_SECUNDARIO,
                  PRED_NRO_PREDIO,
                  CONTROL_ANO_PROCESO,
                  CONTROL_MES_PROCESO,
                  CONTROL_TIPO_PROCESO,
                  DEPRECIACION,
                  DEPRECIACION_AJUSTADA,
                  DEPRECIACION_ACUMULADA,
                  AXI_DEPRECIACION,
                  CUENTA_CONBI_DB,
                  CUENTA_CONBI_CR,
                  PREURBANIZACI_NRO_URBANIZACION,
                  CON_ID,
                  TIEMPO_DEP_AMO_CALCULADA,
                  VALOR_NETO,
                  TIPO_MOVIMIENTO,
                  TRANSACCION,
                  VERSION,
                  FECULTACT,
                  VAL_TOTAL--,
                  )
                  values
                  (
                    c_c.NIVEL_SECUNDARIO,
                    c_c.pred_nro_predio,
                    c_c.control_ano_proceso,        
                    c_c.control_mes_proceso,
                    c_c.control_tipo_proceso,
                    c_cM.VALOR_PREDIO, 
                    0,        
                    0,
                    c_c.axi_depreciacion,  
                    c_c.cuenta_conbi_db,
                    c_c.cuenta_conbi_cr,
                    c_c.preurbanizaci_nro_urbanizacion, 
                    seq_conbi_amo_planb.nextval,
                    c_c.tiempo_dep_amo_calculada,
                    c_c.valor_neto,
                    c_c.TIPO_MOVIMIENTO,
                    v_tran,
                    0,
                    sysdate,
                    c_c.COSTO_HISTORICO --Costo historico sobre el que se ha calculado la alicuota
                  );     
                v_tran := v_tran +1;
              end if;
            end;
          end loop;
        end if;*/
    end if;            
    
    /*Insertar la depreciacion*/
    INSERT  into conbi_depreciacion_amortizacio
                ( NIVEL_SECUNDARIO,
                  PRED_NRO_PREDIO,
                  CONTROL_ANO_PROCESO,
                  CONTROL_MES_PROCESO,
                  CONTROL_TIPO_PROCESO,
                  DEPRECIACION,
                  DEPRECIACION_AJUSTADA,
                  DEPRECIACION_ACUMULADA,
                  AXI_DEPRECIACION,
                  CUENTA_CONBI_DB,
                  CUENTA_CONBI_CR,
                  PREURBANIZACI_NRO_URBANIZACION,
                  CON_ID,
                  TIEMPO_DEP_AMO_CALCULADA,
                  VALOR_NETO,
                  TIPO_MOVIMIENTO,
                  TRANSACCION,
                  VERSION,
                  FECULTACT,
                  VAL_TOTAL--,
                  )
                  values
                  (
                    c_c.NIVEL_SECUNDARIO,
                    c_c.pred_nro_predio,
                    c_c.control_ano_proceso,        
                    c_c.control_mes_proceso,
                    c_c.control_tipo_proceso,
                    c_c.DEPRECIACION, 
                    c_c.DEPRECIACION_AJUSTADA,        
                    c_c.DEPRECIACION_ACUMULADA+c_c.DEPRECIACION,
                    c_c.axi_depreciacion,  
                    c_c.cuenta_conbi_db,
                    c_c.cuenta_conbi_cr,
                    c_c.preurbanizaci_nro_urbanizacion, 
                    seq_conbi_amo_planb.nextval,
                    c_c.tiempo_dep_amo_calculada,
                    c_c.valor_neto,
                    c_c.TIPO_MOVIMIENTO,
                    v_tran,
                    0,
                    sysdate,
                    c_c.COSTO_HISTORICO --Costo historico sobre el que se ha calculado la alicuota
                  );
    v_tran := v_tran +1;
end loop;

/*Periodo inmediatamente anterior, para predios que tienen solo egresos en el mes*/
if  v_mes  = '01' then
    v_mes1 := '12';
    v_ano1 := to_char(to_number(v_ano,9999)-1);
else
    v_mesa := to_number(v_mes,99)-1;
if  v_mesa < 10 then
    v_mes1 := '0' || to_char(v_mesa);
else
    v_mes1 := to_char(v_mesa);
end if;
v_ano1 := v_ano;
end if;
    
    for c_cA in (
                select distinct 
                      cp.NIVEL_SECUNDARIO,
                      cp.PRED_NRO_PREDIO,
                      cp.CONTROL_ANO_PROCESO,
                      cp.CONTROL_MES_PROCESO,
                      cp.CONTROL_TIPO_PROCESO,
                      cp.DEPRECIACION,
                      cp.DEPRECIACION_AJUSTADA,
                      cp.DEPRECIACION_ACUMULADA,
                      cp.AXI_DEPRECIACION,
                      cp.CUENTA_CONBI_DB,
                      cp.CUENTA_CONBI_CR,
                      cp.PREURBANIZACI_NRO_URBANIZACION,
                      cp.CON_ID,
                      cp.TIEMPO_DEP_AMO_CALCULADA,
                      cp.VALOR_NETO,
                      --cp.TIPO_MOVIMIENTO,
                      cp.TRANSACCION,
                      --cm1.TIPO_MOVIMIENTO,
                      cm1.TIPO_ACTO_JURIDICO,
                      cm1.USO_PREDIO,
                      cm1.USO_NIVEL1,
                      cm1.USO_NIVEL2,
                      cm1.TIPO_PROCEDENCIA,
                      cm1.TIPO_TRANSACCION,
                      cm1.CLASE_CUENTA,
                      cm1.nivel_desagregacion
                from 
                      conbi_depreciacion_amortizacio cp
                left  join conbi_movimientos cm1 on cm1.preurbanizaci_nro_urbanizacion = cp.preurbanizaci_nro_urbanizacion 
                and cp.PRED_NRO_PREDIO = cm1.PRED_NRO_PREDIO           
                and   cm1.nivel_desagregacion = cp.nivel_secundario
                where 
                not exists (select PRED_NRO_PREDIO from conbi_dep_amo_pruebas pa where cp.PREURBANIZACI_NRO_URBANIZACION = pa.PREURBANIZACI_NRO_URBANIZACION
                                                      and   cp.PRED_NRO_PREDIO = pa.PRED_NRO_PREDIO
                                                      and   cp.nivel_secundario = pa.nivel_secundario)
                  
                
                and   CONTROL_ANO_PROCESO = v_ano1   
                and   CONTROL_MES_PROCESO = v_mes1
                and   TO_CHAR(cm1.fecha_movimiento,'YYYYMM') = v_ano || v_mes
                and   cm1.tipo_movimiento     = 'EGR'
                and   cm1.nivel_desagregacion <> 1
                and   cp.control_tipo_proceso = 'DEP'
                and   cm1.cuenta_conbi_cr like '1640%'
                
    )
    loop
      /*select    count(*)
        into    v_anterior
        from    conbi_dep_amo_pruebas cp
        where 
              cp.PREURBANIZACI_NRO_URBANIZACION = c_cA.PREURBANIZACI_NRO_URBANIZACION
        and   cp.PRED_NRO_PREDIO  = c_cA.PRED_NRO_PREDIO
        and   cp.nivel_secundario = c_cA.NIVEL_SECUNDARIO;
        
        select count(*) -- cuenta si tiene mas de un egreso en el mismo mes 
        into  v_un_egreso
        from  conbi_movimientos cmm
        where   
              cmm.PREURBANIZACI_NRO_URBANIZACION = c_cA.PREURBANIZACI_NRO_URBANIZACION
        and   cmm.PRED_NRO_PREDIO     = c_cA.PRED_NRO_PREDIO
        and   cmm.nivel_desagregacion = c_cA.nivel_desagregacion
        and   cmm.tipo_movimiento     = 'EGR'; 
        
     
     if v_un_egreso = 1 then   *//*si solo tiene un movimiento en el mes*/
       -- if v_anterior = 0 then 
                
          select DISTINCT CA.CUENTA_CONBI_DB, CA.CUENTA_CONBI_CR     
          into  v_cuenta_conbi_db, v_cuenta_conbi_cr
          from  CONBI_PR_PLAN_CONTABLE_ALTERNO CA
          where 
          CA.USO_PREDIO = c_cA.USO_PREDIO 
          and CA.USO_NIVEL1 = c_cA.USO_NIVEL1 
          and CA.USO_NIVEL2 = c_cA.USO_NIVEL2 
          and CA.TIPO_ACTO_JURIDICO = c_cA.TIPO_ACTO_JURIDICO 
          and CA.TIPO_PROCEDENCIA   = c_cA.TIPO_PROCEDENCIA
          AND CA.CLASE_CUENTA       = c_cA.CLASE_CUENTA 
          and CA.TIPO_MOVIMIENTO    = 'EDEP';
                    
          INSERT  into conbi_depreciacion_amortizacio
                              ( NIVEL_SECUNDARIO,
                                PRED_NRO_PREDIO,
                                CONTROL_ANO_PROCESO,
                                CONTROL_MES_PROCESO,
                                CONTROL_TIPO_PROCESO,
                                DEPRECIACION,
                                DEPRECIACION_AJUSTADA,
                                DEPRECIACION_ACUMULADA,
                                AXI_DEPRECIACION,
                                CUENTA_CONBI_DB,
                                CUENTA_CONBI_CR,
                                PREURBANIZACI_NRO_URBANIZACION,
                                CON_ID,
                                TIEMPO_DEP_AMO_CALCULADA,
                                VALOR_NETO,
                                TIPO_MOVIMIENTO,
                                TRANSACCION,
                                VERSION,
                                FECULTACT,
                                VAL_TOTAL
                                )
                                values
                                (
                                  c_cA.nivel_secundario,
                                  c_cA.pred_nro_predio,
                                  v_ano,
                                  v_mes,
                                  c_cA.control_tipo_proceso,
                                  c_cA.DEPRECIACION_ACUMULADA, 
                                  0,        
                                  0,
                                  c_cA.axi_depreciacion,  
                                  v_cuenta_conbi_db,
                                  v_cuenta_conbi_cr,
                                  c_cA.preurbanizaci_nro_urbanizacion, 
                                  seq_conbi_amo_planb.nextval, 
                                  c_cA.tiempo_dep_amo_calculada,
                                  c_cA.valor_neto,
                                  'EGR',
                                  v_tran,
                                  0,
                                  sysdate,
                                  0
                                );     
          v_tran := v_tran +1;
        --end if; 
      --end if;
    end loop;

INSERT INTO CONBI_CONTROL_PROCESOS values ('DEP',V_ANO, V_MES, 'F', sysdate, 0);
--delete from conbi_dep_amo_pruebas;
commit;
--commit;
END;