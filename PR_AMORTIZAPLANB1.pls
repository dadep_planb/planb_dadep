create or replace procedure pr_amortizaplanb1 (V_ANO  VARCHAR2, V_MES  VARCHAR2) 
AS

urbanizacion            number:= 0;
predio                  number:= 0;
costo_adquisicion       number:= 0;
cuenta_conbi_db         number:= 0;
cuenta_conbi_cr         number:= 0;
fecha                   number:= 0;
nueva_alicuota          number:= 0;
amortizacion            number:= 0;
tvidautilsindepreciar   number:= 0;
alicuota_acumulada_ant  number:= 0;
nueva_alicuota_acumulada  number:= 0;
alicuota_anterior       number:= 0;    
valor_neto              number:= 0;
alicuota_acumulada      number:= 0;
v_CUENTA_CONBI_DB       varchar2(30):= 0;
v_CUENTA_CONBI_CR       varchar2(30):= 0;
vida_util_total         number:= 600;
v_count                 number:= 600;
conteo_rupi             NUMBER:= 0;
n                       number:=1;
bandera                 number:=0;
predio_activo           number:=0;
v_valor_alicuota_anterior_ac          number:=0;
v_valor_alicuota_anterior number:=0;
v_tipo_movimiento       number := 0;
v_tipo_transaccion      number := 0;
type movs is varray(3) of varchar2(5);
conteo_movs movs;
begin
delete from conbi_dep_amo_pruebas1;

commit;

   
                      
      /*Predios depreciables en  la fecha de consulta*/
     insert into  tmp_amortizacion1(PREURBANIZACI_NRO_URBANIZACION,
                                  PRED_NRO_PREDIO,
                                  NIVEL_DESAGREGACION,
                                  fecha_ingreso_contabilidad,
                                  CUENTA_CONBI_DB,
                                  CUENTA_CONBI_CR,
                                  VALOR_PREDIO,
                                  amortizable,
                                  TIPO_MOVIMIENTO,
                                  TIPO_ACTO_JURIDICO,
                                  USO_PREDIO,
                                  USO_NIVEL1,
                                  USO_NIVEL2,
                                  TIPO_PROCEDENCIA,
                                  TIPO_TRANSACCION,
                                  CLASE_CUENTA,
                                  FECHA_MOVIMIENTO) 
                          select distinct 
                                      cm.PREURBANIZACI_NRO_URBANIZACION,
                                      cm.PRED_NRO_PREDIO,
                                      cm.NIVEL_DESAGREGACION, 
                                      (select min (mmm.fecha_movimiento)
                                        from conbi_movimientos mmm
                                        where CM.PREURBANIZACI_NRO_URBANIZACION = mmm.PREURBANIZACI_NRO_URBANIZACION
                                        and   CM.PRED_NRO_PREDIO = mmm.PRED_NRO_PREDIO
                                        and   CM.NIVEL_DESAGREGACION = mmm.NIVEL_DESAGREGACION ) as fecha_ingreso_contabilidad , -- cdc.fecha_ingreso_contabilidad,
                                      cm.CUENTA_CONBI_DB,
                                      cm.CUENTA_CONBI_CR,
                                      cm.VALOR_PREDIO,
                                      cm.amortizable,
                                      cm.TIPO_MOVIMIENTO,
                                      cm.TIPO_ACTO_JURIDICO,
                                      cm.USO_PREDIO,
                                      cm.USO_NIVEL1,
                                      cm.USO_NIVEL2,
                                      cm.TIPO_PROCEDENCIA,
                                      cm.TIPO_TRANSACCION,
                                      cm.CLASE_CUENTA,
                                      cm.FECHA_MOVIMIENTO
                      from      conbi_movimientos cm
                      left join conbi_datos_contables cdc on cm.PREURBANIZACI_NRO_URBANIZACION = cdc.preurbanizaci_nro_urbanizacion and
                                cm.pred_nro_predio = cdc.pred_nro_predio and cdc.nivel_desagregacion = cm.nivel_desagregacion           
                      where     cm.FECHA_MOVIMIENTO = (select max(mm.fecha_movimiento)
                                                         from   CONBI_MOVIMIENTOS mm 
                                                        where   CM.PREURBANIZACI_NRO_URBANIZACION = MM.PREURBANIZACI_NRO_URBANIZACION
                                                          and   CM.PRED_NRO_PREDIO = MM.PRED_NRO_PREDIO
                                                          and   CM.NIVEL_DESAGREGACION = MM.NIVEL_DESAGREGACION
                                                          and   ( to_char(MM.FECHA_MOVIMIENTO, 'yyyy') < v_ano  or 
                                                                 (to_char(MM.FECHA_MOVIMIENTO, 'yyyy') = v_ano 
                                                                 and    to_char(MM.FECHA_MOVIMIENTO, 'mm')<= v_mes))
                                                          and   MM.cuenta_conbi_db not like '3%'
                                                          )
                                                          --and   MM.FECHA_MOVIMIENTO <('01/jul/2013'))
                      and     cm.tipo_movimiento<>'EGR'
                      and     to_char(fecha_ingreso_contabilidad, 'yyyymm') >= '199201' 
                      and     cm.amortizable='S'
                      and     cm.cuenta_conbi_db not like '8%'
                      and     cm.cuenta_conbi_db not like '1640%';
                      
   commit;                   
      
                    for c_c in (SELECT distinct ta.PREURBANIZACI_NRO_URBANIZACION, ta.PRED_NRO_PREDIO, ta.NIVEL_DESAGREGACION, 
                        cd.valor_costo_adquisicion,
                        ta.tipo_movimiento,
                        ta.fecha_ingreso_contabilidad,
                        ta.USO_PREDIO,
                        ta.USO_NIVEL1,
                        ta.USO_NIVEL2,
                        ta.TIPO_ACTO_JURIDICO,
                        ta.TIPO_PROCEDENCIA,
                        ta.CLASE_CUENTA,  
                        ta.tipo_transaccion,
                        ta.fecha_movimiento,
                        ta.cuenta_conbi_db,
                        abs(round(MONTHS_BETWEEN( TO_DATE(TO_CHAR(ta.fecha_ingreso_contabilidad,'YYYYMM'),'YYYYMM'),
                        TO_DATE(TO_CHAR(v_ano||v_mes),'YYYYMM')),0)) as meses,
                        abs(round(MONTHS_BETWEEN( TO_DATE(TO_CHAR(ta.fecha_movimiento,'YYYYMM'),'YYYYMM'),
                        TO_DATE(TO_CHAR(v_ano||v_mes),'YYYYMM')),0)) as mesesuno,
                        ta.valor_predio
                 from   tmp_amortizacion1 ta
                 left join conbi_datos_contables  cd on ta.PREURBANIZACI_NRO_URBANIZACION = cd.PREURBANIZACI_NRO_URBANIZACION
                 and      ta.PRED_NRO_PREDIO                =     cd.PRED_NRO_PREDIO
                 and      ta.NIVEL_DESAGREGACION            =     cd.NIVEL_DESAGREGACION
                 where    ta.amortizable='S' 
                 and      (to_char(ta.fecha_movimiento,'yyyy')  < v_ano  or 
                          (to_char(ta.fecha_movimiento,'yyyy')  = v_ano 
                 and      to_char(ta.fecha_movimiento, 'mm')   <= v_mes)) 
                 --order by ta.NIVEL_DESAGREGACION desc
                 order by ta.PRED_NRO_PREDIO
                 )      
     loop          
     
     /*Consultar el ultimo movimiento del predio en contabilidad*/
     for c_cU in (select * from (
                    select distinct cm.PREURBANIZACI_NRO_URBANIZACION,
                                      cm.PRED_NRO_PREDIO,
                                      cm.NIVEL_DESAGREGACION, 
                                      cdc.fecha_ingreso_contabilidad,
                                      cm.CUENTA_CONBI_DB,
                                      cm.CUENTA_CONBI_CR,
                                      cm.VALOR_PREDIO,
                                      cm.amortizable,
                                      cm.TIPO_MOVIMIENTO,
                                      cm.TIPO_ACTO_JURIDICO,
                                      cm.USO_PREDIO,
                                      cm.USO_NIVEL1,
                                      cm.USO_NIVEL2,
                                      cm.TIPO_PROCEDENCIA,
                                      cm.TIPO_TRANSACCION,
                                      cm.CLASE_CUENTA,
                                      cm.FECHA_MOVIMIENTO
                      from      conbi_movimientos cm
                      left join conbi_datos_contables cdc on cm.PREURBANIZACI_NRO_URBANIZACION = cdc.preurbanizaci_nro_urbanizacion and
                                cm.pred_nro_predio = cdc.pred_nro_predio and cdc.nivel_desagregacion = cm.nivel_desagregacion           
                      where     cm.FECHA_MOVIMIENTO = (select max(mm.fecha_movimiento)
                                                         from   CONBI_MOVIMIENTOS mm 
                                                        where   CM.PREURBANIZACI_NRO_URBANIZACION = MM.PREURBANIZACI_NRO_URBANIZACION
                                                          and   CM.PRED_NRO_PREDIO = MM.PRED_NRO_PREDIO
                                                          and   CM.NIVEL_DESAGREGACION = MM.NIVEL_DESAGREGACION
                                                          and   MM.FECHA_MOVIMIENTO <('01/jul/2013'))
                      and       cm.tipo_movimiento<>'EGR'
                      and       to_char(cdc.fecha_ingreso_contabilidad, 'yyyymm') >= '199201' 
                      and       cm.amortizable='S'
                      and       cm.cuenta_conbi_db not like '8%'
                      and       cm.cuenta_conbi_db not like '1640%'
                      and     cm.PREURBANIZACI_NRO_URBANIZACION = c_c.PREURBANIZACI_NRO_URBANIZACION
                      and     cm.PRED_NRO_PREDIO = c_c.PRED_NRO_PREDIO
                      and     cm.NIVEL_DESAGREGACION = c_c.NIVEL_DESAGREGACION
                  union all
                  select distinct cm.PREURBANIZACI_NRO_URBANIZACION,
                                      cm.PRED_NRO_PREDIO,
                                      cm.NIVEL_DESAGREGACION, 
                                      cdc.fecha_ingreso_contabilidad,
                                      cm.CUENTA_CONBI_DB,
                                      cm.CUENTA_CONBI_CR,
                                      cm.VALOR_PREDIO,
                                      cm.amortizable,
                                      cm.TIPO_MOVIMIENTO,
                                      cm.TIPO_ACTO_JURIDICO,
                                      cm.USO_PREDIO,
                                      cm.USO_NIVEL1,
                                      cm.USO_NIVEL2,
                                      cm.TIPO_PROCEDENCIA,
                                      cm.TIPO_TRANSACCION,
                                      cm.CLASE_CUENTA,
                                      cm.FECHA_MOVIMIENTO
                      from      conbi_movimientos cm
                      left join conbi_datos_contables cdc on cm.PREURBANIZACI_NRO_URBANIZACION = cdc.preurbanizaci_nro_urbanizacion and
                                cm.pred_nro_predio = cdc.pred_nro_predio and cdc.nivel_desagregacion = cm.nivel_desagregacion           
                      where     cm.FECHA_MOVIMIENTO = (select max(mm.fecha_movimiento)
                                                         from   CONBI_MOVIMIENTOS mm 
                                                        where   CM.PREURBANIZACI_NRO_URBANIZACION = MM.PREURBANIZACI_NRO_URBANIZACION
                                                          and   CM.PRED_NRO_PREDIO = MM.PRED_NRO_PREDIO
                                                          and   CM.NIVEL_DESAGREGACION = MM.NIVEL_DESAGREGACION
                                                          and   MM.FECHA_MOVIMIENTO <('01/jul/2013'))
                      and       cm.tipo_movimiento = 'EGR'
                      and       cm.tipo_transaccion in (4,5)
                      and       to_char(cdc.fecha_ingreso_contabilidad, 'yyyymm') >= '199201' 
                      and       cm.amortizable='S'
                      and       cm.cuenta_conbi_db not like '8%'
                      and       cm.cuenta_conbi_db not like '1640%'
                      and       cm.PREURBANIZACI_NRO_URBANIZACION = c_c.PREURBANIZACI_NRO_URBANIZACION
                      and       cm.PRED_NRO_PREDIO = c_c.PRED_NRO_PREDIO
                      and       cm.NIVEL_DESAGREGACION = c_c.NIVEL_DESAGREGACION
                      )
                  where rownum  = 1)
        loop
          
            bandera:=0;
                       
            
            select CA.CUENTA_CONBI_DB, CA.CUENTA_CONBI_CR     
                into v_CUENTA_CONBI_DB, v_CUENTA_CONBI_CR
                from CONBI_PR_PLAN_CONTABLE_ALTERNO CA 
                where 
                CA.USO_PREDIO=c_c.USO_PREDIO 
                and CA.USO_NIVEL1=c_c.USO_NIVEL1 
                and CA.USO_NIVEL2=c_c.USO_NIVEL2 
                and CA.TIPO_ACTO_JURIDICO=c_c.TIPO_ACTO_JURIDICO 
                and CA.TIPO_PROCEDENCIA=c_c.TIPO_PROCEDENCIA
                AND CA.CLASE_CUENTA= c_c.CLASE_CUENTA 
                and CA.TIPO_MOVIMIENTO='AMO'
                ;
            
            v_valor_alicuota_anterior := 0;
            v_valor_alicuota_anterior_ac := 0;
         
                if c_cU.TIPO_MOVIMIENTO = 'ING' then             
                  bandera:=1;
                end if;
                               
                conteo_movs:=movs('','','');
                if c_cU.TIPO_MOVIMIENTO = 'EGR' and (c_cU.tipo_transaccion = 4 or c_cU.tipo_transaccion = 5) then             
                 for c_c3 in (select * from (select  cm.tipo_movimiento
                        from      conbi_movimientos cm
                        where     cm.PREURBANIZACI_NRO_URBANIZACION = c_c.PREURBANIZACI_NRO_URBANIZACION
                       and  cm.PRED_NRO_PREDIO = c_c.PRED_NRO_PREDIO
                       and  cm.NIVEL_DESAGREGACION = c_c.NIVEL_DESAGREGACION
                       order by cm.FECHA_MOVIMIENTO desc)
                       where rownum<=3)
                  loop
                    conteo_movs(n):=c_c3.tipo_movimiento;
                    n:=n+1;
                  end loop;
                  if conteo_movs(2)='ING' and conteo_movs(3)='ING' then                  
                    bandera:=1;
                  end if;
                end if;
                  
                
                if bandera=1 then
                  begin            
                    select depreciacion, depreciacion_acumulada
                    into v_valor_alicuota_anterior, v_valor_alicuota_anterior_ac
                    from (select depreciacion, depreciacion_acumulada
                    from conbi_depreciacion_amortizacio  
                    where 
                    PREURBANIZACI_NRO_URBANIZACION  = c_c.PREURBANIZACI_NRO_URBANIZACION
                    and  PRED_NRO_PREDIO            = c_c.PRED_NRO_PREDIO
                    and  NIVEL_SECUNDARIO           = c_c.NIVEL_DESAGREGACION
                    and  control_tipo_proceso       = 'AMO'
                     order by control_ano_proceso desc, control_mes_proceso desc,con_id desc
                    ) where rownum = 1;
                    
                      costo_adquisicion         := nvl(c_c.VALOR_PREDIO,1);
                      cuenta_conbi_db           := v_CUENTA_CONBI_DB;
                      cuenta_conbi_cr           := v_CUENTA_CONBI_CR;
                      fecha                     := c_c.meses;
                      nueva_alicuota            := c_c.meses + 1;                                                         
                      tvidautilsindepreciar     := vida_util_total - nueva_alicuota;
                      amortizacion              := round((c_c.VALOR_PREDIO/600),1);
                      alicuota_anterior         := v_valor_alicuota_anterior;
                      alicuota_acumulada_ant    := v_valor_alicuota_anterior_ac;
                      alicuota_acumulada        := alicuota_acumulada_ant + amortizacion; 
                      valor_neto                := c_c.valor_predio - alicuota_acumulada;
                      NUEVA_ALICUOTA_ACUMULADA  := alicuota_acumulada + amortizacion;  --round((c_c.VALOR_PREDIO/600),1) * nueva_alicuota ;
                    exception
                      when no_data_found then
                        costo_adquisicion         := nvl(c_c.VALOR_PREDIO,1);
                        cuenta_conbi_db           := v_CUENTA_CONBI_DB;
                        cuenta_conbi_cr           := v_CUENTA_CONBI_CR;
                        fecha                     := c_c.meses;
                        nueva_alicuota            := c_c.meses + 1;
                        amortizacion              := round((c_c.VALOR_PREDIO/600),1) * nueva_alicuota ; 
                        tvidautilsindepreciar     := 600 - nueva_alicuota;
                        alicuota_acumulada_ant    := 0;                                                   
                        nueva_alicuota_acumulada  := round((c_c.VALOR_PREDIO/600),1) * nueva_alicuota ;
                        alicuota_anterior         := 0;    
                        valor_neto                := c_c.VALOR_PREDIO - amortizacion * nueva_alicuota;
                        alicuota_acumulada        := amortizacion * nueva_alicuota;
                   end;
                   
                   
                   
                   
                  INSERT INTO CONBI_DEP_AMO_PRUEBAS1(
                                                      preurbanizaci_nro_urbanizacion, 
                                                      pred_nro_predio,
                                                      NIVEL_SECUNDARIO,
                                                      DEPRECIACION,   
                                                      DEPRECIACION_ACUMULADA,
                                                      DEPRECIACION_AJUSTADA,        
                                                      axi_depreciacion,  
                                                      vida_util,                    
                                                      tvidautilsindepreciar,        
                                                      tiempo_dep_amo_calculada,     
                                                      control_ano_proceso,        
                                                      control_mes_proceso,
                                                      control_tipo_proceso,         
                                                      valor_neto,                   
                                                      TRANSACCION,
                                                      TIPO_MOVIMIENTO,
                                                      ID,
                                                      FECHA_INCORPORA,
                                                      COSTO_HISTORICO,
                                                      NUEVA_ALICUOTA_ACUMULADA,       
                                                      VALOR_ALICUOTA_ANT,             
                                                      ALICUOTA_ANTERIOR,
                                                      cuenta_conbi_db,
                                                      cuenta_conbi_cr,
                                                      CUENTA_MOVIMIENTO,
                                                      USO_PREDIO,
                                                      USO_NIVEL1,
                                                      USO_NIVEL2,
                                                      TIPO_ACTO_JURIDICO,
                                                      TIPO_PROCEDENCIA,
                                                      CLASE_CUENTA
                                                      )
                  VALUES    (
                              c_c.PREURBANIZACI_NRO_URBANIZACION,     
                              c_c.PRED_NRO_PREDIO,                    
                              c_c.nivel_desagregacion,                 
                              amortizacion,                           
                              alicuota_acumulada_ant,                 
                              0,                                      
                              0,                                      
                              vida_util_total,                        
                              tvidautilsindepreciar,                  
                              nueva_alicuota,                         
                              v_ano,                                  
                              v_mes,                                  
                              'AMO',                                  
                              valor_neto,                              
                              1,                                      
                              'CAL',                                  
                              SEQ_CONBI_AMO_PLANB.NEXTVAL,             
                              c_c.fecha_ingreso_contabilidad,                  /* FECHA COSTO ADQUISICION */
                              c_c.VALOR_PREDIO,                      
                              alicuota_acumulada,                     
                              alicuota_anterior,                      
                              nueva_alicuota,                         
                              cuenta_conbi_db,                        
                              cuenta_conbi_cr,
                              c_c.CUENTA_CONBI_DB,
                              c_c.USO_PREDIO,
                              c_c.USO_NIVEL1,
                              c_c.USO_NIVEL2,
                              c_c.TIPO_ACTO_JURIDICO,
                              c_c.TIPO_PROCEDENCIA,
                              c_c.CLASE_CUENTA 
                              );   
                    
        
                  end if;
          --end loop;
        end loop;
     end loop;
     commit;
     DELETE tmp_amortizacion1;
     commit;
end;