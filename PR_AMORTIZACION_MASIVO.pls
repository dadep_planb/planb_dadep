create or replace PROCEDURE      "PR_AMORTIZACION_MASIVO" (v_anoR VARCHAR2, v_mesR VARCHAR2)
AS 
type anos is varray(3) of varchar2(5);
conteo_anos anos;
v_ano varchar2(4) := '2007';
v_mes varchar2(2) := '01';
urbanizacion            number:= 0;
predio                  number:= 0;
costo_adquisicion       number:= 0;
cuenta_conbi_db         number:= 0;
cuenta_conbi_cr         number:= 0;
fecha                   number:= 0;
nueva_alicuota          number:= 0;
amortizacion            number:= 0;
tvidautilsindepreciar   number:= 0;
alicuota_acumulada_ant  number:= 0;
nueva_alicuota_acumulada  number:= 0;
alicuota_anterior       number:= 0;    
valor_neto              number:= 0;
alicuota_acumulada      number:= 0;
vida_util_total         number:= 600;
v_count                 number:= 600;
conteo_rupi             NUMBER:= 0;
n                       number:=1;
bandera                 number:=0;
predio_activo           number:=0;
v_valor_alicuota_anterior_ac          number:=0;
v_valor_alicuota_anterior number:=0;
v_suma_egresosC         number :=0;
v_suma_ingresosC        number :=0;
v_tipo_movimiento       number := 0;
v_tipo_transaccion      number := 0;
v_costo_historico       number  :=  0;
v_costo_historico_ant       number  :=  0;
v_fecha_baja            varchar2(30);
v_egr varchar2(1) := 0;
meses                   number:= 0;
fec_ing_cont            varchar2(20);
depreciacion            number:= 0;
v_cuenta_conbi_db number := 0;
  v_cuenta_conbi_cr number := 0;
  v_movimiento  varchar2(10) := '';
  v_ano1       varchar2(10) := 0;
  v_mes1       varchar2(10) := 0;
  v_tran  number  := 0;
  v_nroM number := 0;
  v_anterior number := 0;
  v_mesa number := 0;
  v_ing number := 0;  
  v_un_egreso number :=0;
begin

while v_ano<v_anoR or (v_ano=v_anoR and v_mes<=v_mesR)
  loop
    delete from conbi_dep_amo_pruebas1;
    commit;
    /*Predios amortizables en  la fecha de consulta*/             
  for c_c in (select  distinct
                  cm.PREURBANIZACI_NRO_URBANIZACION,
                  cm.PRED_NRO_PREDIO,
                  cm.NIVEL_DESAGREGACION, 
                  cm.CUENTA_CONBI_DB,
                  cm.CUENTA_CONBI_CR,
                  cm.VALOR_PREDIO,
                  cm.amortizable,
                  cm.TIPO_MOVIMIENTO,
                  cm.TIPO_ACTO_JURIDICO,
                  cm.USO_PREDIO,
                  cm.USO_NIVEL1,
                  cm.USO_NIVEL2,
                  cm.TIPO_PROCEDENCIA,
                  cm.TIPO_TRANSACCION,
                  cm.CLASE_CUENTA,
                  cm.FECHA_MOVIMIENTO
                  from      conbi_movimientos cm
                  where exists (select PRED_NRO_PREDIO from vm_predios_activos_dic pa where CM.PREURBANIZACI_NRO_URBANIZACION = pa.PREURBANIZACI_NRO_URBANIZACION
                                                      and   CM.PRED_NRO_PREDIO = pa.PRED_NRO_PREDIO
                                                      and   CM.NIVEL_DESAGREGACION = pa.NIVEL_DESAGREGACION)
                  
                  and     cm.FECHA_MOVIMIENTO = (select max(mm.fecha_movimiento)
                                                     from   CONBI_MOVIMIENTOS mm 
                                                    where   CM.PREURBANIZACI_NRO_URBANIZACION = MM.PREURBANIZACI_NRO_URBANIZACION
                                                      and   CM.PRED_NRO_PREDIO = MM.PRED_NRO_PREDIO
                                                      and   CM.NIVEL_DESAGREGACION = MM.NIVEL_DESAGREGACION
                                                      and   ( to_char(MM.FECHA_MOVIMIENTO, 'yyyy') < v_ano  or 
                                                             (to_char(MM.FECHA_MOVIMIENTO, 'yyyy') = v_ano 
                                                             and    to_char(MM.FECHA_MOVIMIENTO, 'mm')<= v_mes))
                                                      and   (MM.cuenta_conbi_db like '1710%' or MM.cuenta_conbi_db like '1720%' )
                                                      )
                  and     to_char((select min (mmm.fecha_movimiento)
                                    from conbi_movimientos mmm
                                    where mmm.CUENTA_CONBI_DB like '17%' and CM.PREURBANIZACI_NRO_URBANIZACION = mmm.PREURBANIZACI_NRO_URBANIZACION
                                    and   CM.PRED_NRO_PREDIO = mmm.PRED_NRO_PREDIO
                                    and   CM.NIVEL_DESAGREGACION = mmm.NIVEL_DESAGREGACION ), 'yyyymm') >= '199201'
                  and     cm.amortizable='S'
                  and    (cm.cuenta_conbi_db not like '83%' 
                  and     cm.cuenta_conbi_db not like '1710010225%'
                  and     cm.cuenta_conbi_db not like '16%')
                  order by cm.preurbanizaci_nro_urbanizacion asc, cm.pred_nro_predio asc
              )
   loop          
     
      bandera:=0;  
        v_valor_alicuota_anterior := 0;
        v_valor_alicuota_anterior_ac := 0;
      
        /*Sumar todos los movimientos y ver si el saldo es cero*/
        begin
        /*consultar si el predio tiene fecha de baja*/
        select fecha_baja into v_fecha_baja
             from rupi_predios where urbanizaci_nro_urbanizacion = c_c.preurbanizaci_nro_urbanizacion
                                                  and nro_predio = c_c.pred_nro_predio
                                                  and fecha_baja < ('01/jul/2013')
                                               ;
        exception
          when no_data_found then
              /*Consultar el costo historico hasta la fecha de consulta*/
            select nvl(sum(VALOR_PREDIO),0)
            into v_suma_ingresosC
            from    CONBI_MOVIMIENTOS CM 
            where   CM.PREURBANIZACI_NRO_URBANIZACION = c_c.PREURBANIZACI_NRO_URBANIZACION
            and   CM.PRED_NRO_PREDIO = c_c.PRED_NRO_PREDIO
            and   CM.NIVEL_DESAGREGACION = c_c.NIVEL_DESAGREGACION
            --and   cm.tipo_movimiento = 'ING'
            and   (cm.CUENTA_CONBI_DB like '1710%' or cm.CUENTA_CONBI_DB like '1720%') --or cm.CUENTA_CONBI_CR like '1710%'  or cm.CUENTA_CONBI_CR like '1720%')
            and   ( to_char(cm.FECHA_MOVIMIENTO, 'yyyy') < v_ano  or 
                  (to_char(cm.FECHA_MOVIMIENTO, 'yyyy') = v_ano 
                  and    to_char(cm.FECHA_MOVIMIENTO, 'mm')<= v_mes))
            ;

            select nvl(sum(VALOR_PREDIO) ,0)
            into v_suma_egresosC
            from    CONBI_MOVIMIENTOS CM 
            where   CM.PREURBANIZACI_NRO_URBANIZACION = c_c.PREURBANIZACI_NRO_URBANIZACION
            and   CM.PRED_NRO_PREDIO = c_c.PRED_NRO_PREDIO
            and   CM.NIVEL_DESAGREGACION = c_c.NIVEL_DESAGREGACION
            --and   cm.tipo_movimiento = 'EGR' --and cm.tipo_transaccion = 2
            and   (cm.CUENTA_CONBI_CR like '1710%' or cm.CUENTA_CONBI_CR like '1720%')
            and   ( to_char(cm.FECHA_MOVIMIENTO, 'yyyy') < v_ano  or 
                  (to_char(cm.FECHA_MOVIMIENTO, 'yyyy') = v_ano 
                  and    to_char(cm.FECHA_MOVIMIENTO, 'mm')<= v_mes))
            ;
            v_costo_historico := v_suma_ingresosC-v_suma_egresosC;
             
            if   v_costo_historico>0 then
              --Consultar las cuentas del movimiento
              select  CA.CUENTA_CONBI_DB, CA.CUENTA_CONBI_CR     
                into cuenta_conbi_db, cuenta_conbi_cr
                from CONBI_PR_PLAN_CONTABLE_ALTERNO CA 
                where 
                CA.USO_PREDIO=c_c.USO_PREDIO 
                and CA.USO_NIVEL1=c_c.USO_NIVEL1 
                and CA.USO_NIVEL2=c_c.USO_NIVEL2 
                and CA.TIPO_ACTO_JURIDICO=c_c.TIPO_ACTO_JURIDICO 
                and CA.TIPO_PROCEDENCIA=c_c.TIPO_PROCEDENCIA
                AND CA.CLASE_CUENTA= c_c.CLASE_CUENTA 
                and CA.TIPO_MOVIMIENTO='AMO'  
                ;
              /*Consultar la fecha de ingreso a contabilidad del predio*/
              select to_char(min (mmm.fecha_movimiento),'DD/MM/YYYY')
              into fec_ing_cont
              from conbi_movimientos mmm
              where mmm.PREURBANIZACI_NRO_URBANIZACION = c_c.PREURBANIZACI_NRO_URBANIZACION
              and   mmm.PRED_NRO_PREDIO = c_c.PRED_NRO_PREDIO
              and   mmm.NIVEL_DESAGREGACION = c_c.NIVEL_DESAGREGACION
              and   (mmm.CUENTA_CONBI_DB like '1710%' or mmm.CUENTA_CONBI_DB like '1720%')
                ;
                    
              begin    
                v_valor_alicuota_anterior := 0;
                v_valor_alicuota_anterior_ac := 0;
                v_costo_historico_ant := 0;
                meses := 0;
                /*Consultar los datos del mes anterior*/
                select depreciacion, depreciacion_acumulada,val_total,TIEMPO_DEP_AMO_CALCULADA,egr
                into v_valor_alicuota_anterior, v_valor_alicuota_anterior_ac,v_costo_historico_ant,meses,v_egr
                from (select depreciacion, depreciacion_acumulada,val_total,TIEMPO_DEP_AMO_CALCULADA,egr
                from conbi_depreciacion_amortizacio  
                where 
                PREURBANIZACI_NRO_URBANIZACION  = c_c.PREURBANIZACI_NRO_URBANIZACION
                and  PRED_NRO_PREDIO            = c_c.PRED_NRO_PREDIO
                and  NIVEL_SECUNDARIO           = c_c.NIVEL_DESAGREGACION
                and  control_tipo_proceso       = 'AMO'
                and  tipo_movimiento            = 'CAL'
                order by control_ano_proceso desc, control_mes_proceso desc, con_id desc
                ) where rownum = 1;
               
                /*Si la depreciaion acumulada es cero puede ser que haya egresado por desincorporación
                 *y debe volver a comenzar las alicuotas
                */
                 /* if v_egr = 1 then
                    meses :=0;
                  end if;*/
                  
                /*Validar si el costo historico ha cambiado para ver si se debe recalcular la alicuota*/
                if v_costo_historico_ant = v_costo_historico then
                  --Si el costo historico es igual al anterior no se recalcula la alícuota
                    costo_adquisicion         := v_costo_historico;
                    fecha                     := meses;
                    nueva_alicuota            := meses + 1;                                                         
                    tvidautilsindepreciar     := vida_util_total - nueva_alicuota;
                    depreciacion              := v_valor_alicuota_anterior;
                    alicuota_anterior         := v_valor_alicuota_anterior;
                    alicuota_acumulada_ant    := v_valor_alicuota_anterior_ac;
                    alicuota_acumulada        := alicuota_acumulada_ant + depreciacion; 
                    valor_neto                := costo_adquisicion - alicuota_acumulada;
                    NUEVA_ALICUOTA_ACUMULADA  := alicuota_acumulada + depreciacion;  
                    
                    if valor_neto > 0 then
                          depreciacion              := round(((v_costo_historico-v_valor_alicuota_anterior_ac)/(tvidautilsindepreciar+1)),1);
                          alicuota_acumulada_ant    := v_valor_alicuota_anterior_ac;
                          alicuota_acumulada := v_valor_alicuota_anterior_ac + depreciacion;
                          nueva_alicuota_acumulada  := alicuota_acumulada + depreciacion;
                          valor_neto                := costo_adquisicion - alicuota_acumulada;
                          /*Predios que tienen una reclasificación por un menor valor y el cálculo de la alicuota da negativo*/
                          if depreciacion < 0 then
                            depreciacion              := round(((v_costo_historico)/600),1);
                            alicuota_acumulada_ant := meses * depreciacion;
                            alicuota_acumulada        := alicuota_acumulada_ant + depreciacion; 
                            valor_neto                := costo_adquisicion - alicuota_acumulada;
                            nueva_alicuota_acumulada  := alicuota_acumulada + depreciacion;
                          end if;
                      else
                          
                          depreciacion              := round(((v_costo_historico)/600),1);
                          alicuota_acumulada_ant := meses * depreciacion;
                          alicuota_acumulada        := alicuota_acumulada_ant + depreciacion; 
                          valor_neto                := costo_adquisicion - alicuota_acumulada;
                          nueva_alicuota_acumulada  := alicuota_acumulada + depreciacion;
                      end if;
                else
                  if v_costo_historico_ant < v_costo_historico then
                    /*Cuando ya se está depreciando desde meses anteriores*/
                    costo_adquisicion         := v_costo_historico;
                    fecha                     := meses;
                    nueva_alicuota            := meses + 1;                                                         
                    tvidautilsindepreciar     := vida_util_total - nueva_alicuota;
                    depreciacion              := round(((v_costo_historico-v_valor_alicuota_anterior_ac)/(tvidautilsindepreciar+1)),1);
                    alicuota_anterior         := v_valor_alicuota_anterior;
                    alicuota_acumulada_ant    := v_valor_alicuota_anterior_ac;
                    alicuota_acumulada        := alicuota_acumulada_ant + depreciacion; 
                    valor_neto                := costo_adquisicion - alicuota_acumulada;
                    NUEVA_ALICUOTA_ACUMULADA  := alicuota_acumulada + depreciacion;  
                    
                   if valor_neto > 0 then
                          depreciacion              := round(((v_costo_historico-v_valor_alicuota_anterior_ac)/(tvidautilsindepreciar+1)),1);
                          alicuota_acumulada_ant    := v_valor_alicuota_anterior_ac;
                          alicuota_acumulada := v_valor_alicuota_anterior_ac + depreciacion;
                          nueva_alicuota_acumulada  := alicuota_acumulada + depreciacion;
                          valor_neto                := costo_adquisicion - alicuota_acumulada;
                          /*Predios que tienen una reclasificación por un menor valor y el cálculo de la alicuota da negativo*/
                          if depreciacion < 0 then
                            depreciacion              := round(((v_costo_historico)/600),1);
                            alicuota_acumulada_ant := meses * depreciacion;
                            alicuota_acumulada        := alicuota_acumulada_ant + depreciacion; 
                            valor_neto                := costo_adquisicion - alicuota_acumulada;
                            nueva_alicuota_acumulada  := alicuota_acumulada + depreciacion;
                          end if;
                      else
                          
                          depreciacion              := round(((v_costo_historico)/600),1);
                          alicuota_acumulada_ant := meses * depreciacion;
                          alicuota_acumulada        := alicuota_acumulada_ant + depreciacion; 
                          valor_neto                := costo_adquisicion - alicuota_acumulada;
                          nueva_alicuota_acumulada  := alicuota_acumulada + depreciacion;
                      end if;
                  else
                    --Si el costo historico es menor al anterior se debe recalcular la alicuota
                      costo_adquisicion         := v_costo_historico;
                      fecha                     := meses;
                      nueva_alicuota            := meses + 1;                                                         
                      tvidautilsindepreciar     := vida_util_total - nueva_alicuota;
                      depreciacion              := round(((v_costo_historico)/(tvidautilsindepreciar+1)),1);
                      alicuota_anterior         := v_valor_alicuota_anterior;
                      alicuota_acumulada_ant    := 0;
                      alicuota_acumulada        := alicuota_acumulada_ant + depreciacion; 
                      valor_neto                := costo_adquisicion - alicuota_acumulada;
                      NUEVA_ALICUOTA_ACUMULADA  := alicuota_acumulada + depreciacion;  
                      
                      if valor_neto > 0 then
                          depreciacion              := round(((v_costo_historico-v_valor_alicuota_anterior_ac)/(tvidautilsindepreciar+1)),1);
                          alicuota_acumulada_ant    := v_valor_alicuota_anterior_ac;
                          alicuota_acumulada := v_valor_alicuota_anterior_ac + depreciacion;
                          nueva_alicuota_acumulada  := alicuota_acumulada + depreciacion;
                          valor_neto                := costo_adquisicion - alicuota_acumulada;
                          /*Predios que tienen una reclasificación por un menor valor y el cálculo de la alicuota da negativo*/
                          if depreciacion < 0 then
                            depreciacion              := round(((v_costo_historico)/600),1);
                            alicuota_acumulada_ant := meses * depreciacion;
                            alicuota_acumulada        := alicuota_acumulada_ant + depreciacion; 
                            valor_neto                := costo_adquisicion - alicuota_acumulada;
                            nueva_alicuota_acumulada  := alicuota_acumulada + depreciacion;
                          end if;
                      else
                          
                          depreciacion              := round(((v_costo_historico)/600),1);
                          alicuota_acumulada_ant := meses * depreciacion;
                          alicuota_acumulada        := alicuota_acumulada_ant + depreciacion; 
                          valor_neto                := costo_adquisicion - alicuota_acumulada;
                          nueva_alicuota_acumulada  := alicuota_acumulada + depreciacion;
                      end if;
                  end if;
                end if;
              exception when no_data_found then
                /*El predio nunca se ha amotizado, es la primera vez*/
                  costo_adquisicion         := v_costo_historico;--nvl(c_c.VALOR_PREDIO,1);
                  fecha                     := meses;
                  nueva_alicuota            := meses + 1;
                  depreciacion              := round((costo_adquisicion/600),1) * nueva_alicuota ; 
                  tvidautilsindepreciar     := 600 - nueva_alicuota;
                  alicuota_acumulada_ant    := 0;                                                   
                  nueva_alicuota_acumulada  := round((costo_adquisicion/600),1) * nueva_alicuota ;
                  alicuota_anterior         := 0;    
                  valor_neto                := costo_adquisicion - depreciacion * nueva_alicuota;
                  alicuota_acumulada        := depreciacion * nueva_alicuota;
              end;
            
              INSERT INTO CONBI_DEP_AMO_PRUEBAS1(
                                            preurbanizaci_nro_urbanizacion, 
                                            pred_nro_predio,
                                            NIVEL_SECUNDARIO,
                                            DEPRECIACION,   
                                            DEPRECIACION_ACUMULADA,
                                            DEPRECIACION_AJUSTADA,        
                                            axi_depreciacion,  
                                            vida_util,                    
                                            tvidautilsindepreciar,        
                                            tiempo_dep_amo_calculada,     
                                            control_ano_proceso,        
                                            control_mes_proceso,
                                            control_tipo_proceso,         
                                            valor_neto,                   
                                            TRANSACCION,
                                            TIPO_MOVIMIENTO,
                                            ID,
                                            FECHA_INCORPORA,
                                            COSTO_HISTORICO,
                                            NUEVA_ALICUOTA_ACUMULADA,       
                                            VALOR_ALICUOTA_ANT,             
                                            ALICUOTA_ANTERIOR,
                                            cuenta_conbi_db,
                                            cuenta_conbi_cr,
                                            CUENTA_MOVIMIENTO,
                                            USO_PREDIO,
                                            USO_NIVEL1,
                                            USO_NIVEL2,
                                            TIPO_ACTO_JURIDICO,
                                            TIPO_PROCEDENCIA,
                                            CLASE_CUENTA
                                            )
                  VALUES    (
                              c_c.PREURBANIZACI_NRO_URBANIZACION,     
                              c_c.PRED_NRO_PREDIO,                    
                              c_c.nivel_desagregacion,                 
                              depreciacion,                           
                              alicuota_acumulada_ant,                 
                              0,                                      
                              0,                                      
                              vida_util_total,                        
                              tvidautilsindepreciar,                  
                              nueva_alicuota,                         
                              v_ano,                                  
                              v_mes,                                  
                              'AMO',                                  
                              valor_neto,                              
                              1,                                      
                              'CAL',                                  
                              SEQ_CONBI_AMO_PLANB.NEXTVAL,             
                              fec_ing_cont,                  /* FECHA COSTO ADQUISICION */
                              costo_adquisicion,                --Costo adquisicion a la fecha
                              alicuota_acumulada,                     
                              alicuota_anterior,                      
                              nueva_alicuota,                         
                              cuenta_conbi_db,                        
                              cuenta_conbi_cr,
                              c_c.CUENTA_CONBI_DB,
                              c_c.USO_PREDIO,
                              c_c.USO_NIVEL1,
                              c_c.USO_NIVEL2,
                              c_c.TIPO_ACTO_JURIDICO,
                              c_c.TIPO_PROCEDENCIA,
                              c_c.CLASE_CUENTA 
                              ); 
            end if;
        end;
     end loop;

    commit;
   
   /*Proceso de leer la tabla conbi_dep_amo_pruebas1 y guardar en conbi_depreciacion_amortizacio*/
  for c_cP in (select distinct
                              NIVEL_SECUNDARIO,
                              PRED_NRO_PREDIO,
                              CONTROL_ANO_PROCESO,
                              CONTROL_MES_PROCESO,
                              CONTROL_TIPO_PROCESO,
                              DEPRECIACION,
                              DEPRECIACION_ACUMULADA,
                              DEPRECIACION_AJUSTADA,
                              NUEVA_ALICUOTA_ACUMULADA,
                              AXI_DEPRECIACION,
                              CUENTA_CONBI_DB,
                              CUENTA_CONBI_CR,
                              PREURBANIZACI_NRO_URBANIZACION,
                              CON_ID,
                              TIEMPO_DEP_AMO_CALCULADA,
                              VALOR_NETO,
                              TIPO_MOVIMIENTO,
                              TRANSACCION,
                              FECHA_INCORPORA,
                              COSTO_HISTORICO
                         from conbi_dep_amo_pruebas1
                              /*where 
                              CONTROL_ANO_PROCESO = V_ANO and CONTROL_MES_PROCESO = V_MES 
                              and CONTROL_TIPO_PROCESO = 'AMO'
                         -- order by preurbanizaci_nro_urbanizacion asc, pred_nro_predio asc*/
                              )
                              
  loop
     v_tran := 1;
    v_ing  := 0;
    /*Nro de movimientos del periodo consultado*/
      select count(*)
    into v_nroM 
      from      conbi_movimientos cm
      where     cm.PREURBANIZACI_NRO_URBANIZACION = c_cP.PREURBANIZACI_NRO_URBANIZACION
      and cm.PRED_NRO_PREDIO = c_cP.PRED_NRO_PREDIO
      and cm.NIVEL_DESAGREGACION = c_cP.NIVEL_SECUNDARIO
      and to_char(cm.FECHA_MOVIMIENTO, 'yyyy') = v_ano   
      and to_char(cm.FECHA_MOVIMIENTO, 'mm') = v_mes
      and cm.FECHA_MOVIMIENTO>=c_cP.FECHA_INCORPORA;
        
    if v_nroM>1 then 
        /*Buscar todos los movimientos del predio para el periodo consultado*/
        for c_cM in (
                    select  
                                      cm.PREURBANIZACI_NRO_URBANIZACION,
                                      cm.PRED_NRO_PREDIO,
                                      cm.NIVEL_DESAGREGACION, 
                                      cm.CUENTA_CONBI_DB,
                                      cm.CUENTA_CONBI_CR,
                                      cm.VALOR_PREDIO,
                                      cm.depreciable,
                                      cm.TIPO_MOVIMIENTO,
                                      cm.TIPO_ACTO_JURIDICO,
                                      cm.USO_PREDIO,
                                      cm.USO_NIVEL1,
                                      cm.USO_NIVEL2,
                                      cm.TIPO_PROCEDENCIA,
                                      cm.TIPO_TRANSACCION,
                                      cm.CLASE_CUENTA,
                                      cm.FECHA_MOVIMIENTO
                      from      conbi_movimientos cm
                      where     cm.PREURBANIZACI_NRO_URBANIZACION = c_cP.PREURBANIZACI_NRO_URBANIZACION
                            and cm.PRED_NRO_PREDIO = c_cP.PRED_NRO_PREDIO
                            and cm.NIVEL_DESAGREGACION = c_cP.NIVEL_SECUNDARIO
                            and to_char(cm.FECHA_MOVIMIENTO, 'yyyy') = v_ano   
                            and to_char(cm.FECHA_MOVIMIENTO, 'mm') = v_mes
                            and cm.FECHA_MOVIMIENTO>=c_cP.FECHA_INCORPORA
                           /* and rownum < (select max(rownum) from conbi_movimientos cm1
                                          where     cm1.PREURBANIZACI_NRO_URBANIZACION = c_cP.PREURBANIZACI_NRO_URBANIZACION
                                                and cm1.PRED_NRO_PREDIO = c_cP.PRED_NRO_PREDIO
                                                and cm1.NIVEL_DESAGREGACION = c_cP.NIVEL_SECUNDARIO
                                                and to_char(cm1.FECHA_MOVIMIENTO, 'yyyy') = v_ano   
                                                and to_char(cm1.FECHA_MOVIMIENTO, 'mm') = v_mes
                                                and cm1.FECHA_MOVIMIENTO>=c_cP.FECHA_INCORPORA
                                          )*/
                      order by cm.FECHA_MOVIMIENTO asc
              )
          loop 
            begin  
              if c_cM.TIPO_MOVIMIENTO = 'ING' then
                v_movimiento := 'AMO';
              end if;
              if c_cM.TIPO_MOVIMIENTO = 'EGR' then
                v_movimiento := 'EAMO';
              end if;
                    
              select DISTINCT CA.CUENTA_CONBI_DB, CA.CUENTA_CONBI_CR     
              into v_cuenta_conbi_db, v_cuenta_conbi_cr
              from CONBI_PR_PLAN_CONTABLE_ALTERNO CA 
              where CA.USO_PREDIO=c_cM.USO_PREDIO 
                and CA.USO_NIVEL1=c_cM.USO_NIVEL1 
                and CA.USO_NIVEL2=c_cM.USO_NIVEL2 
                and CA.TIPO_ACTO_JURIDICO=c_cM.TIPO_ACTO_JURIDICO 
                and CA.TIPO_PROCEDENCIA=c_cM.TIPO_PROCEDENCIA
                and CA.CLASE_CUENTA= c_cM.CLASE_CUENTA 
                and CA.TIPO_MOVIMIENTO=v_movimiento
              ;
            
              select depreciacion, depreciacion_acumulada
              into v_valor_alicuota_anterior, v_valor_alicuota_anterior_ac
              from (
                select depreciacion, depreciacion_acumulada
                from conbi_depreciacion_amortizacio  
                where PREURBANIZACI_NRO_URBANIZACION  = c_cP.PREURBANIZACI_NRO_URBANIZACION
                  and  PRED_NRO_PREDIO            = c_cP.PRED_NRO_PREDIO
                  and  NIVEL_SECUNDARIO           = c_cP.NIVEL_SECUNDARIO
                  and  control_tipo_proceso       = 'AMO'
                  order by control_ano_proceso desc, control_mes_proceso desc, con_id desc
                ) where rownum = 1;
              
              if c_cM.TIPO_MOVIMIENTO = 'EGR' then
                /*Insertar en la tabla el valor en cero*/
                INSERT  into conbi_depreciacion_amortizacio
                        ( NIVEL_SECUNDARIO,
                          PRED_NRO_PREDIO,
                          CONTROL_ANO_PROCESO,
                          CONTROL_MES_PROCESO,
                          CONTROL_TIPO_PROCESO,
                          DEPRECIACION,
                          DEPRECIACION_AJUSTADA,
                          DEPRECIACION_ACUMULADA,
                          AXI_DEPRECIACION,
                          CUENTA_CONBI_DB,
                          CUENTA_CONBI_CR,
                          PREURBANIZACI_NRO_URBANIZACION,
                          CON_ID,
                          TIEMPO_DEP_AMO_CALCULADA,
                          VALOR_NETO,
                          TIPO_MOVIMIENTO,
                          TRANSACCION,
                          VERSION,
                          FECULTACT,
                          VAL_TOTAL--,
                          )
                          values
                          (
                            c_cM.NIVEL_DESAGREGACION,
                            c_cM.pred_nro_predio,
                            c_cP.control_ano_proceso,        
                            c_cP.control_mes_proceso,
                            c_cP.control_tipo_proceso,
                            v_valor_alicuota_anterior_ac, 
                            0,        
                            0,
                            c_cP.axi_depreciacion,  
                            v_cuenta_conbi_db,
                            v_cuenta_conbi_cr,
                            c_cP.preurbanizaci_nro_urbanizacion, 
                            seq_conbi_amo_planb.nextval, 
                            c_cP.tiempo_dep_amo_calculada,
                            c_cP.valor_neto,
                            c_cM.tipo_movimiento,
                            v_tran,
                            0,
                            sysdate,
                            0
                          );
                v_tran := v_tran +1;
              /*else--Si es un ingreso
                --Preguntar si el predio viene de traslado y guardar el movimiento          
                if v_ing = 0 then
                  INSERT  into conbi_depreciacion_amortizacio
                      ( NIVEL_SECUNDARIO,
                        PRED_NRO_PREDIO,
                        CONTROL_ANO_PROCESO,
                        CONTROL_MES_PROCESO,
                        CONTROL_TIPO_PROCESO,
                        DEPRECIACION,
                        DEPRECIACION_AJUSTADA,
                        DEPRECIACION_ACUMULADA,
                        AXI_DEPRECIACION,
                        CUENTA_CONBI_DB,
                        CUENTA_CONBI_CR,
                        PREURBANIZACI_NRO_URBANIZACION,
                        CON_ID,
                        TIEMPO_DEP_AMO_CALCULADA,
                        VALOR_NETO,
                        TIPO_MOVIMIENTO,
                        TRANSACCION,
                        VERSION,
                        FECULTACT,
                        VAL_TOTAL--,
                        )
                        values
                        (
                          c_cM.NIVEL_DESAGREGACION,
                          c_cP.pred_nro_predio,
                          c_cP.control_ano_proceso,        
                          c_cP.control_mes_proceso,
                          c_cP.control_tipo_proceso,
                          v_valor_alicuota_anterior, 
                          0,        
                          v_valor_alicuota_anterior,
                          c_cP.axi_depreciacion,  
                          v_cuenta_conbi_db,
                          v_cuenta_conbi_cr,
                          c_cP.preurbanizaci_nro_urbanizacion, 
                          seq_conbi_amo_planb.nextval,
                          c_cP.tiempo_dep_amo_calculada,
                          c_cP.valor_neto,
                          c_cM.TIPO_MOVIMIENTO,
                          v_tran,
                          0,
                          sysdate,
                          0
                        );
                  v_tran := v_tran +1;
                  v_ing := 1;
                end if;*/
              end if;
            exception
            when no_data_found then
                v_ing := 0;
            end; 
          end loop;
    /*else 
      if v_nroM=1 then
      
      for c_cM in (
                    select  
                                      cm.PREURBANIZACI_NRO_URBANIZACION,
                                      cm.PRED_NRO_PREDIO,
                                      cm.NIVEL_DESAGREGACION, 
                                      cm.CUENTA_CONBI_DB,
                                      cm.CUENTA_CONBI_CR,
                                      cm.VALOR_PREDIO,
                                      cm.depreciable,
                                      cm.TIPO_MOVIMIENTO,
                                      cm.TIPO_ACTO_JURIDICO,
                                      cm.USO_PREDIO,
                                      cm.USO_NIVEL1,
                                      cm.USO_NIVEL2,
                                      cm.TIPO_PROCEDENCIA,
                                      cm.TIPO_TRANSACCION,
                                      cm.CLASE_CUENTA,
                                      cm.FECHA_MOVIMIENTO
                      from      conbi_movimientos cm
                      where     cm.PREURBANIZACI_NRO_URBANIZACION = c_cP.PREURBANIZACI_NRO_URBANIZACION
                            and cm.PRED_NRO_PREDIO = c_cP.PRED_NRO_PREDIO
                            and cm.NIVEL_DESAGREGACION = c_cP.NIVEL_SECUNDARIO
                            and to_char(cm.FECHA_MOVIMIENTO, 'yyyy') = v_ano   
                            and to_char(cm.FECHA_MOVIMIENTO, 'mm') = v_mes
                            and (cm.cuenta_conbi_db like '1710%' or cm.cuenta_conbi_db like '1720%' )
                            and cm.cuenta_conbi_db not like '1710010225%'
                      order by cm.FECHA_MOVIMIENTO asc
              )
          loop
            begin
              if c_cM.TIPO_MOVIMIENTO = 'EGR' then
                select DISTINCT CA.CUENTA_CONBI_DB, CA.CUENTA_CONBI_CR     
                into v_cuenta_conbi_db, v_cuenta_conbi_cr
                from CONBI_PR_PLAN_CONTABLE_ALTERNO CA
                where 
                CA.USO_PREDIO=c_cM.USO_PREDIO 
                and CA.USO_NIVEL1=c_cM.USO_NIVEL1 
                and CA.USO_NIVEL2=c_cM.USO_NIVEL2 
                and CA.TIPO_ACTO_JURIDICO=c_cM.TIPO_ACTO_JURIDICO 
                and CA.TIPO_PROCEDENCIA=c_cM.TIPO_PROCEDENCIA
                AND CA.CLASE_CUENTA= c_cM.CLASE_CUENTA 
                and CA.TIPO_MOVIMIENTO='EAMO';
                          
                INSERT  into conbi_depreciacion_amortizacio
                ( NIVEL_SECUNDARIO,
                  PRED_NRO_PREDIO,
                  CONTROL_ANO_PROCESO,
                  CONTROL_MES_PROCESO,
                  CONTROL_TIPO_PROCESO,
                  DEPRECIACION,
                  DEPRECIACION_AJUSTADA,
                  DEPRECIACION_ACUMULADA,
                  AXI_DEPRECIACION,
                  CUENTA_CONBI_DB,
                  CUENTA_CONBI_CR,
                  PREURBANIZACI_NRO_URBANIZACION,
                  CON_ID,
                  TIEMPO_DEP_AMO_CALCULADA,
                  VALOR_NETO,
                  TIPO_MOVIMIENTO,
                  TRANSACCION,
                  VERSION,
                  FECULTACT,
                  VAL_TOTAL--,
                  )
                  values
                  (
                    c_cP.NIVEL_SECUNDARIO,
                    c_cP.pred_nro_predio,
                    c_cP.control_ano_proceso,        
                    c_cP.control_mes_proceso,
                    c_cP.control_tipo_proceso,
                    c_cM.VALOR_PREDIO, 
                    0,        
                    0,
                    c_cP.axi_depreciacion,  
                    c_cP.cuenta_conbi_db,
                    c_cP.cuenta_conbi_cr,
                    c_cP.preurbanizaci_nro_urbanizacion, 
                    seq_conbi_amo_planb.nextval,
                    c_cP.tiempo_dep_amo_calculada,
                    c_cP.valor_neto,
                    c_cP.TIPO_MOVIMIENTO,
                    v_tran,
                    0,
                    sysdate,
                    c_cP.COSTO_HISTORICO --Costo historico sobre el que se ha calculado la alicuota
                  );     
                v_tran := v_tran +1;
              end if;
            end;
          end loop;
        end if;*/
    end if;            
    
    /*Insertar la depreciacion*/
    INSERT  into conbi_depreciacion_amortizacio
                ( NIVEL_SECUNDARIO,
                  PRED_NRO_PREDIO,
                  CONTROL_ANO_PROCESO,
                  CONTROL_MES_PROCESO,
                  CONTROL_TIPO_PROCESO,
                  DEPRECIACION,
                  DEPRECIACION_AJUSTADA,
                  DEPRECIACION_ACUMULADA,
                  AXI_DEPRECIACION,
                  CUENTA_CONBI_DB,
                  CUENTA_CONBI_CR,
                  PREURBANIZACI_NRO_URBANIZACION,
                  CON_ID,
                  TIEMPO_DEP_AMO_CALCULADA,
                  VALOR_NETO,
                  TIPO_MOVIMIENTO,
                  TRANSACCION,
                  VERSION,
                  FECULTACT,
                  VAL_TOTAL--,
                  )
                  values
                  (
                  c_cP.NIVEL_SECUNDARIO,
                  c_cP.PRED_NRO_PREDIO,
                  c_cP.CONTROL_ANO_PROCESO,
                  c_cP.CONTROL_MES_PROCESO,
                  c_cP.CONTROL_TIPO_PROCESO,
                  c_cP.DEPRECIACION,
                  c_cP.DEPRECIACION_AJUSTADA,
                  c_cP.NUEVA_ALICUOTA_ACUMULADA,
                  c_cP.AXI_DEPRECIACION,
                  c_cP.CUENTA_CONBI_DB,
                  c_cP.CUENTA_CONBI_CR,
                  c_cP.PREURBANIZACI_NRO_URBANIZACION,
                  seq_conbi_amo_planb.nextval,
                  c_cP.TIEMPO_DEP_AMO_CALCULADA,
                  c_cP.VALOR_NETO,
                  c_cP.TIPO_MOVIMIENTO,
                  v_tran,
                  0,
                  sysdate,
                  c_cP.COSTO_HISTORICO
                  );
    v_tran := v_tran +1;
  end loop;
 
  /*Periodo inmediatamente anterior, para predios que tienen solo egresos en el mes*/
if  v_mes = '01' then
  v_mes1 := '12';
  v_ano1 := to_char(to_number(v_ano,9999)-1);
else
  v_mesa := to_number(v_mes,99)-1;
  if v_mesa < 10 then
    v_mes1 := '0' || to_char(v_mesa);
  else
    v_mes1 := to_char(v_mesa);
  end if;
  v_ano1 := v_ano;
end if;
    
    for c_cA in (
               select               
                      cp.nivel_secundario,
                      cp.PREURBANIZACI_NRO_URBANIZACION,
                      cp.PRED_NRO_PREDIO,
                      cp.CONTROL_ANO_PROCESO,
                      cp.CONTROL_MES_PROCESO,
                      cp.CONTROL_TIPO_PROCESO,
                      cp.DEPRECIACION,
                      cp.DEPRECIACION_AJUSTADA,
                      cp.DEPRECIACION_ACUMULADA,
                      cp.AXI_DEPRECIACION,
                      cp.CUENTA_CONBI_DB,
                      cp.CUENTA_CONBI_CR,
                      cp.CON_ID,
                      cp.TIEMPO_DEP_AMO_CALCULADA,
                      cp.VALOR_NETO,
                      --cp.TIPO_MOVIMIENTO,
                      cp.TRANSACCION,
                      --cm1.TIPO_MOVIMIENTO,
                      cm1.TIPO_ACTO_JURIDICO,
                      cm1.USO_PREDIO,
                      cm1.USO_NIVEL1,
                      cm1.USO_NIVEL2,
                      cm1.TIPO_PROCEDENCIA,
                      cm1.TIPO_TRANSACCION,
                      cm1.CLASE_CUENTA
                      --cm1.nivel_desagregacion
                from  conbi_movimientos cm1
                      inner  join conbi_depreciacion_amortizacio cp on cm1.preurbanizaci_nro_urbanizacion = cp.preurbanizaci_nro_urbanizacion 
                      and cp.PRED_NRO_PREDIO = cm1.PRED_NRO_PREDIO and   cm1.nivel_desagregacion = cp.nivel_secundario 
                where 
                not exists (select PRED_NRO_PREDIO from conbi_dep_amo_pruebas1 pa where cp.PREURBANIZACI_NRO_URBANIZACION = pa.PREURBANIZACI_NRO_URBANIZACION
                                                      and   cp.PRED_NRO_PREDIO = pa.PRED_NRO_PREDIO
                                                      and   cp.nivel_secundario = pa.nivel_secundario)
                  
                
                and   cp.CONTROL_ANO_PROCESO = v_ano1
                and   cp.CONTROL_MES_PROCESO = v_mes1
                and   cp.control_tipo_proceso='AMO'
                and   TO_CHAR(cm1.fecha_movimiento,'YYYYMM') = v_ano || v_mes
                and   cm1.tipo_movimiento = 'EGR' and cm1.tipo_transaccion = 2
                and   (cm1.cuenta_conbi_cr like '1710%' or cm1.cuenta_conbi_cr like '1720%')
                and cp.con_id=(select max(cp1.con_id) as dd from conbi_depreciacion_amortizacio cp1 
                                                  where cp1.preurbanizaci_nro_urbanizacion = cp.preurbanizaci_nro_urbanizacion
                                                  and cp1.PRED_NRO_PREDIO = cp.PRED_NRO_PREDIO and cp1.nivel_secundario =  cp.nivel_secundario)
                order by cp.preurbanizaci_nro_urbanizacion asc, cp.pred_nro_predio asc
    )
    loop 
        
        /*select count(*) /* cuenta si tiene mas de un egreso en el mismo mes */
       /* into  v_un_egreso
        from  conbi_movimientos cmm
        where   
              cmm.PREURBANIZACI_NRO_URBANIZACION = c_cA.PREURBANIZACI_NRO_URBANIZACION
        and   cmm.PRED_NRO_PREDIO = c_cA.PRED_NRO_PREDIO
        and   cmm.nivel_desagregacion = c_cA.nivel_desagregacion;    */     
        
        /*select count(*)
        into    v_anterior
        from    conbi_dep_amo_pruebas1 cp3
        where 
              cp3.PREURBANIZACI_NRO_URBANIZACION = c_cA.PREURBANIZACI_NRO_URBANIZACION
        and   cp3.PRED_NRO_PREDIO = c_cA.PRED_NRO_PREDIO
        and   cp3.nivel_secundario = c_cA.NIVEL_SECUNDARIO;

  -- if v_un_egreso = 1 then   /*si solo tiene un movimiento en el mes*/
       --if v_anterior = 0 then 
                
          select DISTINCT CA.CUENTA_CONBI_DB, CA.CUENTA_CONBI_CR     
          into v_CUENTA_CONBI_DB, v_CUENTA_CONBI_CR
          from CONBI_PR_PLAN_CONTABLE_ALTERNO CA
          where 
          CA.USO_PREDIO=c_cA.USO_PREDIO 
          and CA.USO_NIVEL1=c_cA.USO_NIVEL1 
          and CA.USO_NIVEL2=c_cA.USO_NIVEL2 
          and CA.TIPO_ACTO_JURIDICO=c_cA.TIPO_ACTO_JURIDICO 
          and CA.TIPO_PROCEDENCIA=c_cA.TIPO_PROCEDENCIA
          AND CA.CLASE_CUENTA= c_cA.CLASE_CUENTA 
          and CA.TIPO_MOVIMIENTO='EAMO';
                    
          INSERT  into conbi_depreciacion_amortizacio
                              ( NIVEL_SECUNDARIO,
                                PRED_NRO_PREDIO,
                                CONTROL_ANO_PROCESO,
                                CONTROL_MES_PROCESO,
                                CONTROL_TIPO_PROCESO,
                                DEPRECIACION,
                                DEPRECIACION_AJUSTADA,
                                DEPRECIACION_ACUMULADA,
                                AXI_DEPRECIACION,
                                CUENTA_CONBI_DB,
                                CUENTA_CONBI_CR,
                                PREURBANIZACI_NRO_URBANIZACION,
                                CON_ID,
                                TIEMPO_DEP_AMO_CALCULADA,
                                VALOR_NETO,
                                TIPO_MOVIMIENTO,
                                TRANSACCION,
                                VERSION,
                                FECULTACT,
                                VAL_TOTAL,
                                EGR
                                )
                                values
                                (
                                  c_cA.nivel_secundario,
                                  c_cA.pred_nro_predio,
                                  v_ano,
                                  v_mes,
                                  c_cA.control_tipo_proceso,
                                  c_cA.DEPRECIACION_ACUMULADA, 
                                  0,        
                                  0,
                                  c_cA.axi_depreciacion,  
                                  v_CUENTA_CONBI_DB,
                                  v_cuenta_conbi_cr,
                                  c_cA.preurbanizaci_nro_urbanizacion, 
                                  seq_conbi_amo_planb.nextval, 
                                  c_cA.tiempo_dep_amo_calculada,
                                  c_cA.valor_neto,
                                  'EGR',
                                  v_tran,
                                  0,
                                  sysdate,
                                  0,
                                  1
                                );     
          v_tran := v_tran +1;
        --end if;    
      --end if;     
    end loop;

INSERT INTO CONBI_CONTROL_PROCESOS values ('AMO',V_ANO, V_MES, 'F', sysdate, 0);
--execute immediate 'truncate table conbi_dep_amo_pruebas1';

commit;
    delete from conbi_dep_amo_pruebas1;
    commit;
        
    if v_mes = '12' then
          v_mes := '01';
          v_ano := to_char(to_number(v_ano,'9999')+1);
        else
            v_mes := to_char(to_number(v_mes,99)+1);
            if length(v_mes) = 1 then
                v_mes := '0' || v_mes;
            end if;
        end if;
    end loop;
end;