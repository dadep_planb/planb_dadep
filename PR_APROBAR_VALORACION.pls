create or replace PROCEDURE        "PR_APROBAR_VALORACION" (V_ANO VARCHAR2, V_MES VARCHAR2)AS 

BEGIN

  INSERT INTO conbi_depreciacion_amortizacio(
              CONTROL_ANO_PROCESO, 
              CONTROL_MES_PROCESO, 
              PREURBANIZACI_NRO_URBANIZACION, 
              PRED_NRO_PREDIO, 
              DEPRECIACION,
              DEPRECIACION_AJUSTADA, 
              DEPRECIACION_ACUMULADA,
              AXI_DEPRECIACION,
              CUENTA_CONBI_DB, 
              CUENTA_CONBI_CR, 
              CONTROL_TIPO_PROCESO,
              TIEMPO_DEP_AMO_CALCULADA,
              VALOR_NETO, 
              NIVEL_SECUNDARIO,
              TIPO_MOVIMIENTO,
              TRANSACCION
              )
        
              SELECT    CONTROL_ANO_VALORIZACION,        
                        CONTROL_MES_VALORIZACION,
                        preurbanizaci_nro_urbanizacion, 
                        pred_nro_predio,
                        valorizacion_mes,                         
                        0,
                        VAL_ACUMULADA,
                        0,                                    
                        CUENTA_CONBI_DB,                      
                        CUENTA_CONBI_CR,
                        'VAL',
                        0,
                        0,
                        NIVEL_SECUNDARIO,
                        'CAL',
                        0
               from     conbi_dep_amo_pruebas2
              where 
                        CONTROL_ANO_VALORIZACION = V_ANO and CONTROL_MES_VALORIZACION = V_MES;
                              

INSERT INTO CONBI_CONTROL_PROCESOS values ('VAL',V_ANO, V_MES, 'F', sysdate, 0);
--delete from conbi_dep_amo_pruebas;
commit;
--commit;
END;