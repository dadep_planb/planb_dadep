create or replace PROCEDURE pr_consulta_valorizaplanb (V_ANO  VARCHAR2, 
V_MES  VARCHAR2)  AS 
avaluo_mes              number:= 0;
v_valorizacion_mes      number:= 0;
v_acumulada_mes         number:= 0;
cuenta_conbi_db         number:= 0;
cuenta_conbi_cr         number:= 0;
fecha                   number:= 0;
v_suma_ingresosC        number:= 0;
v_suma_egresosC         number:= 0;
v_cuenta_conbi_db       number:= 0;
v_cuenta_conbi_cr       number:= 0;
v_depreciacion          number:= 0;
v_costo_historico       number:= 0;
v_costo_h               number:= 0;
fec_ing_cont            varchar2(20);
entra                   varchar2(20);

BEGIN
DELETE FROM VALORIZA_CONSULTA;
COMMIT;

for c_cv in (select distinct cm.PREURBANIZACI_NRO_URBANIZACION,
                        cm.PRED_NRO_PREDIO,  
                        cm.tipo_procedencia,
                        ae.fecha_avaluo,
                        ae.valor_avaluo,
                        cm.valor_predio,
                        cm.nivel_desagregacion, 
                        cm.fecha_movimiento,
                        cm.amortizable,
                        cm.depreciable
                        from conbi_movimientos cm
                        inner join  RUPI_AVALUO_EDIFICACIONES ae on cm.PREURBANIZACI_NRO_URBANIZACION = ae.PREURBANIZACI_NRO_URBANIZACION and ae.PRED_NRO_PREDIO = cm.PRED_NRO_PREDIO 
                        inner join  conbi_datos_contables DC on DC.PREURBANIZACI_NRO_URBANIZACION = ae.PREURBANIZACI_NRO_URBANIZACION and ae.PRED_NRO_PREDIO = DC.PRED_NRO_PREDIO and  ae.fecha_avaluo >= DC.FECHA_INGRESO_CONTABILIDAD
                        where 
                        cm.clase_cuenta = 'B' and cm.uso_predio = 1
                        and cm.depreciable = 'S'
                        and cm.nivel_desagregacion <> 1
                        and cm.FECHA_MOVIMIENTO = (select max(mm.fecha_movimiento)
                                                     from   CONBI_MOVIMIENTOS mm 
                                                    where   CM.PREURBANIZACI_NRO_URBANIZACION = MM.PREURBANIZACI_NRO_URBANIZACION
                                                      and   CM.PRED_NRO_PREDIO = MM.PRED_NRO_PREDIO
                                                      and   CM.NIVEL_DESAGREGACION = MM.NIVEL_DESAGREGACION
                                                     )
                        and to_char(DC.FECHA_INGRESO_CONTABILIDAD, 'yyyy') >= '1992'
              union 
                  select distinct cm.PREURBANIZACI_NRO_URBANIZACION,
                        cm.PRED_NRO_PREDIO,  
                        cm.tipo_procedencia,
                        ar.fecha_avaluo,
                        ar.valor_avaluo,
                        cm.valor_predio,
                        cm.nivel_desagregacion,
                        cm.fecha_movimiento,
                        cm.amortizable,
                        cm.depreciable
                        from conbi_movimientos cm
                        inner join  rupi_avaluo_terrenos ar on cm.PREURBANIZACI_NRO_URBANIZACION = ar.PREURBANIZACI_NRO_URBANIZACION and ar.PRED_NRO_PREDIO = cm.PRED_NRO_PREDIO 
                        inner join  conbi_datos_contables CD on CD.PREURBANIZACI_NRO_URBANIZACION = ar.PREURBANIZACI_NRO_URBANIZACION and ar.PRED_NRO_PREDIO = CD.PRED_NRO_PREDIO and ar.fecha_avaluo >= CD.FECHA_INGRESO_CONTABILIDAD
                        where 
                        cm.clase_cuenta = 'B' and cm.uso_predio = 1
                        and cm.nivel_desagregacion = 1
                        and cm.FECHA_MOVIMIENTO = (select max(mm.fecha_movimiento)
                                                     from   CONBI_MOVIMIENTOS mm 
                                                    where   CM.PREURBANIZACI_NRO_URBANIZACION = MM.PREURBANIZACI_NRO_URBANIZACION
                                                      and   CM.PRED_NRO_PREDIO = MM.PRED_NRO_PREDIO
                                                      and   CM.NIVEL_DESAGREGACION = MM.NIVEL_DESAGREGACION
                                                     )
                        and to_char(CD.FECHA_INGRESO_CONTABILIDAD, 'yyyy') >= '1992'
                        order by preurbanizaci_nro_urbanizacion, pred_nro_predio, fecha_avaluo)      
     loop 
     v_depreciacion := 0;
     v_costo_h := 0;
           /*Consultar el fecha incorporacion */
           if   c_cv.amortizable='S' then --el predio es amortizable, ingresa a contabilidad con ctas 17
                select min (mmm.fecha_movimiento)--to_char(min (mmm.fecha_movimiento),'DD/MM/YYYY')
                into fec_ing_cont
                from conbi_movimientos mmm
                where mmm.PREURBANIZACI_NRO_URBANIZACION = c_cv.PREURBANIZACI_NRO_URBANIZACION
                and   mmm.PRED_NRO_PREDIO = c_cv.PRED_NRO_PREDIO
                and   mmm.NIVEL_DESAGREGACION = c_cv.NIVEL_DESAGREGACION
                and   (mmm.CUENTA_CONBI_DB like '1710%' or mmm.CUENTA_CONBI_DB like '1720%');
                  
                select nvl(sum(valor_predio),0)
                into v_suma_ingresosC
                from    conbi_movimientos cm 
                where   cm.preurbanizaci_nro_urbanizacion = c_cv.preurbanizaci_nro_urbanizacion
                and   cm.pred_nro_predio = c_cv.pred_nro_predio
                and   cm.nivel_desagregacion = c_cv.nivel_desagregacion
                and   (cm.CUENTA_CONBI_DB like '1710%' or cm.CUENTA_CONBI_DB like '1720%')
                and   cm.fecha_movimiento <= c_cv.fecha_movimiento;
    
                select nvl(sum(valor_predio) ,0)
                into v_suma_egresosC
                from    conbi_movimientos cm 
                where   cm.preurbanizaci_nro_urbanizacion = c_cv.preurbanizaci_nro_urbanizacion
                and   cm.pred_nro_predio = c_cv.pred_nro_predio
                and   cm.nivel_desagregacion = c_cv.nivel_desagregacion
                and   (cm.CUENTA_CONBI_cr like '1710%' or cm.CUENTA_CONBI_cr like '1720%')
                and   cm.fecha_movimiento <= c_cv.fecha_movimiento;  
          else if c_cv.depreciable='S' then --El predio es depreciable, ingresa a contabilidad con ctas 16
                select min (mmm.fecha_movimiento)--to_char(min (mmm.fecha_movimiento),'DD/MM/YYYY')
                into fec_ing_cont
                from conbi_movimientos mmm
                where mmm.preurbanizaci_nro_urbanizacion = c_cv.preurbanizaci_nro_urbanizacion
                and   mmm.pred_nro_predio = c_cv.pred_nro_predio
                and   mmm.nivel_desagregacion = c_cv.nivel_desagregacion
                and   (mmm.cuenta_conbi_db like '1640%');    
                  
                  /*Consultar el costo historico hasta la fecha de consulta*/
                select nvl(sum(valor_predio),0)
                into   v_suma_ingresosC
                from   conbi_movimientos cm 
                where  cm.preurbanizaci_nro_urbanizacion = c_cv.preurbanizaci_nro_urbanizacion
                and    cm.pred_nro_predio = c_cv.pred_nro_predio
                and    cm.nivel_desagregacion = c_cv.nivel_desagregacion
                and    (cm.cuenta_conbi_db like '1640%' or cm.cuenta_conbi_db like '1920%')
                and    cm.fecha_movimiento <= c_cv.fecha_movimiento;
    
                select nvl(sum(valor_predio) ,0)
                into v_suma_egresosC
                from    conbi_movimientos cm 
                where   cm.preurbanizaci_nro_urbanizacion = c_cv.preurbanizaci_nro_urbanizacion
                and   cm.pred_nro_predio = c_cv.pred_nro_predio
                and   cm.nivel_desagregacion = c_cv.nivel_desagregacion
                and   (cm.cuenta_conbi_cr like '1640%' or cm.cuenta_conbi_cr like '1920%')
                and   cm.cuenta_conbi_cr not like '19200633%'
                and   cm.fecha_movimiento <= c_cv.fecha_movimiento;
          else --El predio no es depreciable ni amortizable, ingresa a contabilidad con ctas 1605
              select min (mmm.fecha_movimiento)--to_char(min (mmm.fecha_movimiento),'DD/MM/YYYY')
                into fec_ing_cont
                from conbi_movimientos mmm
                where mmm.preurbanizaci_nro_urbanizacion = c_cv.preurbanizaci_nro_urbanizacion
                and   mmm.pred_nro_predio = c_cv.pred_nro_predio
                and   mmm.nivel_desagregacion = c_cv.nivel_desagregacion
                and   (mmm.cuenta_conbi_db like '16050133%' or mmm.cuenta_conbi_db like '16050234%' or mmm.cuenta_conbi_db like '19200633%');  
                
                  /*Consultar el costo historico hasta la fecha de consulta*/
                select nvl(sum(valor_predio),0)
                into   v_suma_ingresosC
                from   conbi_movimientos cm 
                where  cm.preurbanizaci_nro_urbanizacion = c_cv.preurbanizaci_nro_urbanizacion
                and    cm.pred_nro_predio = c_cv.pred_nro_predio
                and    cm.nivel_desagregacion = c_cv.nivel_desagregacion
                and    (cm.cuenta_conbi_db like '16050133%' or cm.cuenta_conbi_db like '16050234%' or cm.cuenta_conbi_db like '19200633%')
                and    cm.fecha_movimiento <= c_cv.fecha_movimiento;
    
                select nvl(sum(valor_predio) ,0)
                into v_suma_egresosC
                from    conbi_movimientos cm 
                where   cm.preurbanizaci_nro_urbanizacion = c_cv.preurbanizaci_nro_urbanizacion
                and   cm.pred_nro_predio = c_cv.pred_nro_predio
                and   cm.nivel_desagregacion = c_cv.nivel_desagregacion
                and    (cm.cuenta_conbi_cr like '16050133%' or cm.cuenta_conbi_cr like '16050234%' or cm.cuenta_conbi_cr like '19200633%')
                and   cm.fecha_movimiento <= c_cv.fecha_movimiento;
          end if;
          begin 
            --Depreciacion acumulada y costo historico hasta el periodo del avaluo
              select dep, v_costo_historico
              into v_depreciacion, v_costo_h
              from (select depreciacion_acumulada as dep, cda.VAL_TOTAL as v_costo_historico
              from conbi_depreciacion_amortizacio  cda
              where 
              preurbanizaci_nro_urbanizacion  = c_cv.preurbanizaci_nro_urbanizacion
              and  pred_nro_predio            = c_cv.pred_nro_predio
              and  nivel_secundario           = c_cv.nivel_desagregacion
              and  control_tipo_proceso       = 'DEP'
              and  control_ano_proceso        = to_char(c_cv.fecha_avaluo, 'yyyy')
              and  control_mes_proceso        = to_char(c_cv.fecha_avaluo, 'mm')
              ) where rownum = 1;

            v_valorizacion_mes := c_cv.valor_avaluo - (v_costo_historico - v_depreciacion) - v_acumulada_mes; 
          exception when no_data_found then
            v_valorizacion_mes := c_cv.valor_avaluo - v_costo_historico - v_acumulada_mes;
          end; 
             
          v_costo_historico := v_suma_ingresosC - v_suma_egresosC;
        if fec_ing_cont <= c_cv.fecha_avaluo then
             
             if   v_costo_historico > 0 then
             
                INSERT INTO VALORIZA_CONSULTA(
                                                  preurbanizaci_nro_urbanizacion, 
                                                  pred_nro_predio,
                                                  NIVEL_SECUNDARIO,
                                                  CONTROL_ANO_VALORIZACION,        
                                                  CONTROL_MES_VALORIZACION,
                                                  control_tipo_proceso,               --  AMO
                                                  DEPRECIACION, 
                                                  CUENTA_CONBI_DB,                    --  VALOR_PREDIO  
                                                  CUENTA_CONBI_CR,
                                                  VALOR_AVALUO,
                                                  COSTO_HISTORICO,
                                                  valorizacion_mes,                   /* Al�. Acumulada*/
                                                  acumulada_mes,
                                                  id,
                                                  FECHA_AVALUO,
                                                  fecha_incorpora                                       
                                                  )
                        VALUES    (
                                  c_cv.PREURBANIZACI_NRO_URBANIZACION,     
                                  c_cv.PRED_NRO_PREDIO,                    
                                  c_cv.nivel_desagregacion,                
                                  v_ano,           
                                  v_mes,
                                  'VAL',
                                  v_depreciacion,
                                  v_CUENTA_CONBI_DB,                       
                                  v_CUENTA_CONBI_CR,
                                  c_cv.valor_avaluo,
                                  v_costo_h,-- v_costo_historico,--
                                  v_valorizacion_mes,
                                  v_depreciacion,
                                  0,
                                  C_CV.fecha_avaluo,
                                  fec_ing_cont
                                  );
       end if;      
  end if;
  end if;           
end loop;

commit; 

END;