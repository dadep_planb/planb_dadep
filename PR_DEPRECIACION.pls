create or replace PROCEDURE PR_DEPRECIACION(v_ano VARCHAR2, v_mes VARCHAR2) IS
  -- ACTUALIZADO A MAYO 12 DE 2009
  -- Se define un cursor para cargar los predios que se deben hacer las depreciaciones
  -- Se deprecia todos los predios de uso fiscal ingresados a contabilidad quitando los predios desincorporados
  -- Nuevo select para Sumar las depreciaciones
  CURSOR C_PREDIOS_DEPRECIAR(FECHA VARCHAR2) is
  SELECT PREURBANIZACI_NRO_URBANIZACION,
      PRED_NRO_PREDIO,
      NIVEL_DESAGREGACION
    FROM CONBI_MOVIMIENTOS
    WHERE DEPRECIABLE                       = 'S'
    AND (TIPO_TRANSACCION                   = 1
    OR TIPO_TRANSACCION                     = 3)
    AND TO_CHAR(FECHA_MOVIMIENTO,'YYYYMM') <= '201212';
  -- UTILIZADO PARA PRUEBAS CON UN SOLO PREDIO --   AND PRED_NRO_PREDIO = 1550 --NRO_PRE
 
  -- UTILIZADO PARA PRUEBAS CON UN SOLO PREDIO --AND PRED_NRO_PREDIO = 1550; --NRO_PRE
  --  Cursor para traer los datos de los predios ingresados a contabilidad
  CURSOR C_PREDIO(NRO_URB NUMBER, NRO_PRE NUMBER, DESG NUMBER, V_FECHA VARCHAR2)
  IS
    SELECT SUM(VALOR_PREDIO) COSTO_PREDIO,
      TIPO_ACTO_JURIDICO,
      USO_PREDIO,
      USO_NIVEL1,
      USO_NIVEL2,
      CLASE_CUENTA,
      TIPO_PROCEDENCIA
    FROM CONBI_MOVIMIENTOS
    WHERE PREURBANIZACI_NRO_URBANIZACION    = NRO_URB
    AND PRED_NRO_PREDIO                     = NRO_PRE
    AND NIVEL_DESAGREGACION                 = DESG
    AND (TIPO_MOVIMIENTO                    = 'ING'
    OR (TIPO_MOVIMIENTO                     = 'IAMO'
    OR TIPO_MOVIMIENTO                      = 'IDEP'))
    AND TO_CHAR(FECHA_MOVIMIENTO,'YYYYMM') <= V_FECHA
    AND USO_NIVEL1                         <> 11
    GROUP BY TIPO_ACTO_JURIDICO,
      USO_PREDIO,
      USO_NIVEL1,
      USO_NIVEL2,
      CLASE_CUENTA,
      TIPO_PROCEDENCIA;
  
   
CURSOR C_PREDIO_DEP(NRO_URB NUMBER, NRO_PRE NUMBER, DESG NUMBER, V_FECHA DATE)
  IS
    SELECT DEPRECIACION_ACUMULADA,
      TIEMPO_DEP_AMO_CALCULADA,
      AXI_DEPRECIACION
    FROM CONBI_DEPRECIACION_AMORTIZACIO
    WHERE PREURBANIZACI_NRO_URBANIZACION = NRO_URB
    AND PRED_NRO_PREDIO                  = NRO_PRE
    AND NIVEL_SECUNDARIO                 = DESG
    AND CONTROL_TIPO_PROCESO             = 'DEP'
    AND CONTROL_ANO_PROCESO              = TO_CHAR(V_FECHA,'YYYY')
    AND CONTROL_MES_PROCESO              = TO_CHAR(V_FECHA,'MM');
  vr_predio_dep C_PREDIO_DEP%ROWTYPE;
  -- Variables
  v_costo_predio      NUMBER:=0;
  v_depreciacion_mes  NUMBER:=0;
  v_depreciacion_acum NUMBER:=0;
  --n_val_ingresado        NUMBER:=0;
  v_tiempo_depreciacion NUMBER:=0;
  v_cuenta_conbi_db     VARCHAR2(20);
  v_cuenta_conbi_cr     VARCHAR2(20);
  v_respuesta           VARCHAR2(2);
  v_vida_util           NUMBER:=0;
  v_valor_neto          NUMBER:=0;
  v_fecha               DATE;
  v_total_predio        NUMBER := 0;
  v_cuenta              NUMBER := 0;
BEGIN
  v_fecha        := TO_DATE(v_ano || v_mes,'YYYYMM');
  v_fecha        := ADD_MONTHS(v_fecha,-1);
  v_total_predio := FN_CONTAR_PREDIOS('S','N',2012 || 12);
END;
-- Abrir el cursor para recorrer los predios a depreciar

FOR VC_PREDIOS_DEPRECIAR IN C_PREDIOS_DEPRECIAR(2012 || 12) 
LOOP
  v_cuenta := v_cuenta+ 1;
  set_Item_Property('B_PROCESOS.BARRA_PROGRESO',PROMPT_TEXT,TO_CHAR(ROUND(((v_cuenta/v_total_predio)*100))) || ' % Completado');
  SYNCHRONIZE;
  set_Item_Property('B_PROCESOS.BARRA_PROGRESO', WIDTH,((v_cuenta/v_total_predio)*10));
  OPEN C_PREDIO(VC_PREDIOS_DEPRECIAR.PREURBANIZACI_NRO_URBANIZACION, VC_PREDIOS_DEPRECIAR.PRED_NRO_PREDIO,VC_PREDIOS_DEPRECIAR.NIVEL_DESAGREGACION, v_ano || v_mes);
  FETCH C_PREDIO INTO vr_predio;
  IF C_PREDIO%NOTFOUND THEN
    vr_predio := NULL;
  END IF;
  OPEN C_PREDIO_DEP(VC_PREDIOS_DEPRECIAR.PREURBANIZACI_NRO_URBANIZACION, VC_PREDIOS_DEPRECIAR.PRED_NRO_PREDIO,VC_PREDIOS_DEPRECIAR.NIVEL_DESAGREGACION,v_fecha);
  FETCH C_PREDIO_DEP INTO vr_predio_dep;
  IF C_PREDIO_DEP%NOTFOUND THEN
    vr_predio_dep := NULL;
  END IF;
  /*
  -- Nuevo select para Sumar las depreciaciones
  SELECT NVL(SUM(VALOR_PREDIO),0)
  INTO n_val_ingresado
  FROM CONBI_MOVIMIENTOS
  WHERE PREURBANIZACI_NRO_URBANIZACION = VC_PREDIOS_DEPRECIAR.PREURBANIZACI_NRO_URBANIZACION
  AND PRED_NRO_PREDIO = VC_PREDIOS_DEPRECIAR.PRED_NRO_PREDIO
  AND NIVEL_DESAGREGACION = vc_predios_depreciar.nivel_desagregacion
  AND TO_CHAR(FECHA_MOVIMIENTO,'YYYYMM') <= V_fecha
  AND (TIPO_MOVIMIENTO = 'ING'
  OR (TIPO_MOVIMIENTO = 'IAMO'
  OR TIPO_MOVIMIENTO = 'IDEP'))
  AND NVL(TIPO_CONSTRUCCION,'0') <> '1'
  AND CUENTA_CONBI_CR LIKE '1685010%'
  AND TIPO_MOVIMIENTO = 'ING';
  */
  v_depreciacion_mes       := 0;
  v_depreciacion_acum      := NVL(vr_predio_dep.depreciacion_acumulada,0);
  v_tiempo_depreciacion    := NVL(vr_predio_dep.tiempo_dep_amo_calculada,0);
  IF vr_predio.clase_cuenta = 'B' THEN
    v_vida_util            := pk_conbi.fn_leer_tiempo_vida_util(vc_predios_depreciar.preurbanizaci_nro_urbanizacion, vc_predios_depreciar.pred_nro_predio, vc_predios_depreciar.nivel_desagregacion);
  ELSE
    v_vida_util := 600;
  END IF;
  v_costo_predio        := pk_conbi.fn_leer_costo_historico(vc_predios_depreciar.preurbanizaci_nro_urbanizacion, vc_predios_depreciar.pred_nro_predio, vc_predios_depreciar.nivel_desagregacion,v_ano || v_mes);
  IF v_depreciacion_acum < v_costo_predio AND v_tiempo_depreciacion < v_vida_util AND vr_predio.uso_predio IS NOT NULL THEN
    -- inicia de preciar un mes despues de la fecha de escritura
    v_depreciacion_mes    := v_costo_predio      /v_vida_util;
    v_depreciacion_acum   := v_depreciacion_acum + v_depreciacion_mes+ global.n_val_ingresado;
    IF v_depreciacion_acum > v_costo_predio THEN
      v_depreciacion_mes  := v_costo_predio                              - NVL(vr_predio_dep.depreciacion_acumulada,0);
      v_depreciacion_acum := NVL(vr_predio_dep.depreciacion_acumulada,0) + v_depreciacion_mes;
    END IF;
    v_valor_neto          := v_costo_predio        - v_depreciacion_acum;
    v_tiempo_depreciacion := v_tiempo_depreciacion +1;
    -- Leer la cuentas para el movimiento
    pk_conbi.pr_leer_cuenta(vr_predio.tipo_acto_juridico, 'DEP', vr_predio.uso_predio, vr_predio.uso_nivel1, vr_predio.uso_nivel2, vr_predio.clase_cuenta, vr_predio.tipo_procedencia, v_cuenta_conbi_db, v_cuenta_conbi_cr, v_respuesta);
    IF v_respuesta = 'S' THEN -- Si encontró La cuenta
      INSERT
      INTO CONBI_DEP_AMO_PRUEBAS
        (
          CONTROL_ANO_PROCESO,
          CONTROL_MES_PROCESO,
          PREURBANIZACI_NRO_URBANIZACION,
          PRED_NRO_PREDIO,
          DEPRECIACION,
          DEPRECIACION_AJUSTADA,
          DEPRECIACION_ACUMULADA,
          AXI_DEPRECIACION,
          CUENTA_CONBI_DB,
          CUENTA_CONBI_CR,
          CONTROL_TIPO_PROCESO,
          TIEMPO_DEP_AMO_CALCULADA,
          VALOR_NETO,
          NIVEL_SECUNDARIO
        )
        VALUES
        (
          v_ano,
          v_mes,
          vc_predios_depreciar.preurbanizaci_nro_urbanizacion,
          vc_predios_depreciar.pred_nro_predio,
          v_depreciacion_mes,
          0,
          v_depreciacion_acum,
          0,
          v_cuenta_conbi_db,
          v_cuenta_conbi_cr,
          'DEP',
          v_tiempo_depreciacion,
          v_valor_neto,
          vc_predios_depreciar.nivel_desagregacion
        );
    ELSE
      ROLLBACK;
      PR_DESPLIEGA_MENSAJE('AL_STOP_1','EL PROCESO FALLÓ. Faltan cuentas contables para uso, uso nivel1 y uso nivel2. Favor crearlas por Pantalla "Conbi Plan Contable ".' || VC_PREDIOS_DEPRECIAR.PREURBANIZACI_NRO_URBANIZACION || '-' || VC_PREDIOS_DEPRECIAR.PRED_NRO_PREDIO || '-' || VC_PREDIOS_DEPRECIAR.NIVEL_DESAGREGACION);
      RAISE form_trigger_failure;
    END IF;
  END IF;
  -- Cierra el cursor a predios
  CLOSE C_PREDIO;
  CLOSE C_PREDIO_DEP;
END LOOP;
CLEAR; 
COMMIT;
SYNCHRONIZE;
--set_Item_Property('B_PROCESOS.BARRA_PROGRESO',PROMPT_TEXT, '100 % Completado');
--set_Item_Property('B_PROCESOS.BARRA_PROGRESO', WIDTH,10);
--PR_DESPLIEGA_MENSAJE('AL_STOP_1','EL PROCESO DE DEPRECIACIÓN FINALIZÓ SATISFACTORIAMENTE');
END;				 
				