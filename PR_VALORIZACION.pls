create or replace PROCEDURE PR_VALORIZACION(v_ano VARCHAR2, v_mes VARCHAR2) IS
-- Se define un cursor para cargar los predios que se deben hacer las valorizaciones
-- Se deprecia todos los predios de uso fiscal ingresados a contabilidad quitando los predios desincorporados 
 
CURSOR C_PREDIOS_VALORIZAR(FECHA VARCHAR2) IS	
  SELECT PREURBANIZACI_NRO_URBANIZACION, PRED_NRO_PREDIO, NIVEL_DESAGREGACION 
	  FROM CONBI_MOVIMIENTOS 
	 WHERE (DEPRECIABLE = 'S'  OR USO_NIVEL1 = 11)  AND TIPO_TRANSACCION = 1 AND TO_CHAR(FECHA_MOVIMIENTO,'YYYYMM') <= FECHA
	 --AND  PREURBANIZACI_NRO_URBANIZACION =2 AND PRED_NRO_PREDIO=388
   MINUS 
  SELECT PREURBANIZACI_NRO_URBANIZACION, PRED_NRO_PREDIO, NIVEL_DESAGREGACION 
    FROM CONBI_MOVIMIENTOS 
   WHERE (DEPRECIABLE = 'S'  OR USO_NIVEL1 = 11) AND TIPO_TRANSACCION = 2  AND TO_CHAR(FECHA_MOVIMIENTO,'YYYYMM') <= FECHA;
	
--  Cursor para traer los datos de los predios ingresados a contabilidad
CURSOR C_PREDIO(NRO_URB NUMBER, NRO_PRE NUMBER, DESG NUMBER, V_FECHA VARCHAR2) IS
		SELECT SUM(VALOR_PREDIO) COSTO_PREDIO,TIPO_ACTO_JURIDICO,
	         USO_PREDIO,USO_NIVEL1, USO_NIVEL2, CLASE_CUENTA, TIPO_PROCEDENCIA
	  FROM CONBI_MOVIMIENTOS
	 	WHERE PREURBANIZACI_NRO_URBANIZACION =  NRO_URB
	  	AND PRED_NRO_PREDIO = NRO_PRE
	  	AND NIVEL_DESAGREGACION = DESG
		  AND (TIPO_MOVIMIENTO = 'ING' OR (TIPO_MOVIMIENTO = 'IAMO' OR TIPO_MOVIMIENTO = 'IDEP'))
		  AND TO_CHAR(FECHA_MOVIMIENTO,'YYYYMM') <= V_FECHA
	 	GROUP BY TIPO_ACTO_JURIDICO, USO_PREDIO,USO_NIVEL1, USO_NIVEL2, 
	 	 				 CLASE_CUENTA, TIPO_PROCEDENCIA
	  MINUS
		SELECT SUM(VALOR_PREDIO) COSTO_PREDIO,TIPO_ACTO_JURIDICO,
			 		 USO_PREDIO,USO_NIVEL1, USO_NIVEL2, CLASE_CUENTA, TIPO_PROCEDENCIA
		FROM CONBI_MOVIMIENTOS
	  WHERE PREURBANIZACI_NRO_URBANIZACION = NRO_URB
	    AND PRED_NRO_PREDIO = NRO_PRE
	  	AND NIVEL_DESAGREGACION = DESG
	    AND (TIPO_MOVIMIENTO = 'EGR'OR (TIPO_MOVIMIENTO = 'EAMO' OR TIPO_MOVIMIENTO = 'EDEP'))
	    AND TO_CHAR(FECHA_MOVIMIENTO,'YYYYMM') <= V_FECHA
		GROUP BY TIPO_ACTO_JURIDICO, USO_PREDIO,USO_NIVEL1, USO_NIVEL2, 
				    CLASE_CUENTA, TIPO_PROCEDENCIA;

vr_predio C_PREDIO%ROWTYPE;  

CURSOR C_PREDIO_VAL(NRO_URB NUMBER, NRO_PRE NUMBER, DESG NUMBER, V_FECHA DATE) IS
	   SELECT VALOR_NETO 
	 	 FROM CONBI_DEPRECIACION_AMORTIZACIO
	 	 WHERE PREURBANIZACI_NRO_URBANIZACION = NRO_URB
	 	       AND PRED_NRO_PREDIO = NRO_PRE
	 	       AND NIVEL_SECUNDARIO = DESG
	 	       AND CONTROL_TIPO_PROCESO <> 'VAL'
	 	       AND CONTROL_ANO_PROCESO = TO_CHAR(V_FECHA,'YYYY')
	 	       AND CONTROL_MES_PROCESO = TO_CHAR(V_FECHA,'MM');
	 	       
vr_predio_val C_PREDIO_VAL%ROWTYPE;  	 	       

-- Variables 
v_costo_predio								NUMBER:=0;

v_cuenta_conbi_db							VARCHAR2(20);
v_cuenta_conbi_cr							VARCHAR2(20);
v_respuesta										VARCHAR2(2);

v_fecha												DATE;
v_valor_neto									NUMBER:=0;
v_valorizacion_mes					  NUMBER:=0;
v_avaluo							    		NUMBER:=0;

v_total_predio								NUMBER := 0;
v_cuenta											NUMBER := 0;

v_valorizacion_acum           NUMBER := 0;

BEGIN

 v_total_predio := FN_CONTAR_PREDIOS_VAL(v_ano || v_mes);

 v_fecha := TO_DATE(v_ano || v_mes,'YYYYMM');


	-- Abrir el cursor para recorrer los predios a depreciar
	FOR VC_PREDIOS_VALORIZAR IN C_PREDIOS_VALORIZAR(v_ano || v_mes) LOOP

     		v_cuenta := v_cuenta + 1;
     
/*     		set_Item_Property('B_PROCESOS.BARRA_PROGRESO',PROMPT_TEXT,TO_CHAR(ROUND(((v_cuenta/v_total_predio)*100))) || ' % Completado'); 
     		SYNCHRONIZE;
     		set_Item_Property('B_PROCESOS.BARRA_PROGRESO', WIDTH,((v_cuenta/v_total_predio)*10));
*/
	      OPEN C_PREDIO(VC_PREDIOS_VALORIZAR.PREURBANIZACI_NRO_URBANIZACION, VC_PREDIOS_VALORIZAR.PRED_NRO_PREDIO,VC_PREDIOS_VALORIZAR.NIVEL_DESAGREGACION, v_ano || v_mes);
	      FETCH C_PREDIO INTO vr_predio;
	    
	      IF C_PREDIO%NOTFOUND THEN
	    	  vr_predio := NULL;
	      END IF;
	    
	      OPEN C_PREDIO_VAL(VC_PREDIOS_VALORIZAR.PREURBANIZACI_NRO_URBANIZACION, VC_PREDIOS_VALORIZAR.PRED_NRO_PREDIO,VC_PREDIOS_VALORIZAR.NIVEL_DESAGREGACION,v_fecha);
	      FETCH C_PREDIO_VAL INTO vr_predio_val;

	      IF C_PREDIO_VAL%NOTFOUND THEN
	    	  vr_predio_val := NULL;
	      END IF;
   	     
				v_valor_neto  := NVL(vr_predio_val.valor_neto,0);
				
				IF VC_PREDIOS_VALORIZAR.NIVEL_DESAGREGACION = 1 THEN
					
          v_avaluo := pk_conbi.fn_leer_avaluo_terreno(vc_predios_valorizar.preurbanizaci_nro_urbanizacion,vc_predios_valorizar.pred_nro_predio,v_fecha);
          
				ELSIF VC_PREDIOS_VALORIZAR.NIVEL_DESAGREGACION = 2 THEN
					
          v_avaluo := pk_conbi.fn_leer_avaluo_edificacion(vc_predios_valorizar.preurbanizaci_nro_urbanizacion,vc_predios_valorizar.pred_nro_predio,v_fecha);
					
        END IF;

        IF v_avaluo IS NOT NULL AND  v_avaluo > 0 THEN

           v_costo_predio := pk_conbi.fn_leer_costo_historico(vc_predios_valorizar.preurbanizaci_nro_urbanizacion,
           																									vc_predios_valorizar.pred_nro_predio,
           																									vc_predios_valorizar.nivel_desagregacion,
           																									v_ano || v_mes);
        
          -- funci�n que trae el valor acumulado de la valorizacion mes anterior 	
           v_valorizacion_acum :=  pk_conbi.fn_leer_val_acum_mes_ant(vc_predios_valorizar.preurbanizaci_nro_urbanizacion,vc_predios_valorizar.pred_nro_predio,'VAL',VC_PREDIOS_VALORIZAR.NIVEL_DESAGREGACION, ADD_MONTHS(TO_DATE(v_ano || v_mes,'YYYYMM'),-1));  
        	

 	  	     IF vr_predio.uso_nivel1 <> 11 THEN  -- SI NO SON TERRENOS
 	  	     	
 	  	     	   v_valorizacion_mes :=  v_avaluo - v_valor_neto;
 	  	     	   
 	  	     	   IF v_valorizacion_acum  >  0 THEN 
 	  	            v_valorizacion_mes :=  v_valorizacion_mes - v_valorizacion_acum; 
 	  	     	  -- ELSIF v_valorizacion_acum  <  0  THEN
 	  	     	  -- 	  v_valorizacion_mes :=  v_valorizacion_mes - v_valorizacion_acum;
 	  	     	   END IF;
 	  	     	   
 	  	     	ELSE
 	  	     	   --SI SON TERRENOS	
 	  	     	   v_valorizacion_mes := v_avaluo - v_costo_predio;
 	  	     	   
 	  	     	   IF (v_valorizacion_acum  <>  v_valorizacion_mes) OR v_valorizacion_acum > 0 THEN 
 	  	            v_valorizacion_mes :=  v_valorizacion_mes - v_valorizacion_acum;
 	  	     	  -- ELSIF  (v_valorizacion_acum  <>  v_valorizacion_mes) OR v_valorizacion_acum < 0 THEN
 	  	     	  -- 	  v_valorizacion_mes :=  v_valorizacion_mes - v_valorizacion_acum;
 	  	     	   END IF;
 	  	     	   
 	  	     END IF;
 	  	     
           ---TEMPORAL--------------
           IF vr_predio.clase_cuenta = 'B' AND v_valorizacion_mes >= 0 THEN  ---AND v_valorizacion_mes => 0 ******OJO PARA DESVALORIZACIONES
           
		 			 -- Leer la cuentas para el movimiento
					 pk_conbi.pr_leer_cuenta(vr_predio.tipo_acto_juridico, 'VAL',
				  										  	vr_predio.uso_predio, vr_predio.uso_nivel1, 
				  										  	vr_predio.uso_nivel2, vr_predio.clase_cuenta, vr_predio.tipo_procedencia,
				  										  	v_cuenta_conbi_db, v_cuenta_conbi_cr, v_respuesta);
				  										  	

		   		 IF v_respuesta = 'S' THEN -- Si encontr� La cuenta
		           
		     			 -- inserta el saldo calculado de ajuste hasta dic -2000
			 			  INSERT INTO CONBI_DEPRECIACION_AMORTIZACIO(
		 									    CONTROL_ANO_PROCESO, 
													CONTROL_MES_PROCESO, 
													PREURBANIZACI_NRO_URBANIZACION, 
													PRED_NRO_PREDIO, 
													DEPRECIACION,
													DEPRECIACION_AJUSTADA, 
													DEPRECIACION_ACUMULADA,
													AXI_DEPRECIACION,
													CUENTA_CONBI_DB, 
													CUENTA_CONBI_CR, 
													CONTROL_TIPO_PROCESO,
													TIEMPO_DEP_AMO_CALCULADA,
													VALOR_NETO, 
													NIVEL_SECUNDARIO 
										   )
										   VALUES(
										      v_ano,
										      v_mes,
											  	vc_predios_valorizar.preurbanizaci_nro_urbanizacion,
											 	  vc_predios_valorizar.pred_nro_predio,
											 	  v_valorizacion_mes,
											 	  0,
											 	  (v_valorizacion_mes+v_valorizacion_acum),
											 	  0,
											 	  v_cuenta_conbi_db,
											 	  v_cuenta_conbi_cr,
											 	  'VAL',
											 	  NULL,
											 	  NULL,
											 	  vc_predios_valorizar.nivel_desagregacion
										   );	
									--	    COMMIT;
										--    CLEAR_MESSAGE;
		      ELSE
						        		ROLLBACK;
						          	PR_DESPLIEGA_MENSAJE('AL_STOP_1','EL PROCESO FALL�. Faltan cuentas contables para uso, uso nivel1 y uso nivel2. Favor crearlas por Pantalla "Conbi Plan Contable ".' || VC_PREDIOS_VALORIZAR.PREURBANIZACI_NRO_URBANIZACION || '-' || VC_PREDIOS_VALORIZAR.PRED_NRO_PREDIO || '-' || VC_PREDIOS_VALORIZAR .NIVEL_DESAGREGACION);
						      			RAISE form_trigger_failure;
		      END IF; 	
		      
		    END IF; ---FIN TEMPORAL  

       END IF;
	  
	  -- Cierra el cursor a predios
		CLOSE C_PREDIO;
		CLOSE C_PREDIO_VAL;

	END LOOP;
	 COMMIT;
	CLEAR_MESSAGE;

	
/*	SYNCHRONIZE;
	set_Item_Property('B_PROCESOS.BARRA_PROGRESO',PROMPT_TEXT, '100 % Completado'); 
	set_Item_Property('B_PROCESOS.BARRA_PROGRESO', WIDTH,10);
*/
  PR_DESPLIEGA_MENSAJE('AL_STOP_1','EL PROCESO DE VALORIZACI�N FINALIZ� SATISFACTORIAMENTE');

END;