create or replace procedure pr_valorizaplanb (V_ANO  VARCHAR2, V_MES  VARCHAR2 ) 
AS

avaluo_mes              number:= 0;
v_valorizacion_mes      number:= 0;
v_acumulada_mes         number:= 0;
cuenta_conbi_db         number:= 0;
cuenta_conbi_cr         number:= 0;
fecha                   number:= 0;
v_suma_ingresos         number:= 0;
v_suma_egresos          number:= 0;
v_cuenta_conbi_db       number:= 0;
v_cuenta_conbi_cr       number:= 0;
v_depreciacion          number:= 0;
v_costo_historico       number:= 0;
fec_ing_cont            varchar2(20);
begin
delete from conbi_dep_amo_pruebas2; 
commit;

    -------------------------------------------------------------------------------
    /*   CONSULTA PARA LOS PREDIOS INCORPORADOS HASTA LA FECHA DEL ULTIMO CORTE  */
    /*   ALMACENA EN UNA TABLA TEMORAL POR SESION                                */
    -------------------------------------------------------------------------------
    for c_cv in (
                  select distinct cm.PREURBANIZACI_NRO_URBANIZACION,
                        cm.PRED_NRO_PREDIO,  
                        cm.tipo_procedencia,
                        cm.tipo_acto_juridico, 
                        cm.uso_nivel1, 
                        cm.uso_nivel2, 
                        cm.uso_predio,
                        ae.fecha_avaluo,
                        ae.valor_avaluo,
                        cm.valor_predio,
                        cm.nivel_desagregacion,
                        cm.amortizable,
                        cm.depreciable,
                        cm.cuenta_conbi_db
                        from conbi_movimientos cm
                        inner join  RUPI_AVALUO_EDIFICACIONES ae on cm.PREURBANIZACI_NRO_URBANIZACION = ae.PREURBANIZACI_NRO_URBANIZACION and ae.PRED_NRO_PREDIO = cm.PRED_NRO_PREDIO 
                        where
                        to_char(ae.fecha_avaluo,'yyyy')= v_ano
                        and   to_char(ae.fecha_avaluo,'mm') = v_mes
                        and cm.clase_cuenta = 'B' and cm.uso_predio = 1
                        and cm.nivel_desagregacion = 2
                        and cm.FECHA_MOVIMIENTO = (select max(mm.fecha_movimiento)
                                                     from   CONBI_MOVIMIENTOS mm 
                                                    where   CM.PREURBANIZACI_NRO_URBANIZACION = MM.PREURBANIZACI_NRO_URBANIZACION
                                                      and   CM.PRED_NRO_PREDIO = MM.PRED_NRO_PREDIO
                                                      and   CM.NIVEL_DESAGREGACION = MM.NIVEL_DESAGREGACION
                                                      and   ( to_char(MM.FECHA_MOVIMIENTO, 'yyyy') < v_ano  or 
                                                             (to_char(MM.FECHA_MOVIMIENTO, 'yyyy') = v_ano 
                                                             and    to_char(MM.FECHA_MOVIMIENTO, 'mm')<= v_mes))
                                                      --and   (MM.cuenta_conbi_db like '1710%' or MM.cuenta_conbi_db like '1720%' )
                                                      )
                      and     to_char((select min (mmm.fecha_movimiento)
                                        from conbi_movimientos mmm
                                        where (mmm.cuenta_conbi_db like '1640%'
                                                or mmm.cuenta_conbi_db like '19200601%' 
                                                or mmm.cuenta_conbi_db like '19200602%' 
                                                or mmm.cuenta_conbi_db like '19200609%')
                      and     cm.preurbanizaci_nro_urbanizacion = mmm.preurbanizaci_nro_urbanizacion
                      and     cm.pred_nro_predio = mmm.pred_nro_predio
                      and     cm.nivel_desagregacion = mmm.nivel_desagregacion ), 'yyyymm') >= '199201'
                  union 
                  select distinct cm.PREURBANIZACI_NRO_URBANIZACION,
                        cm.PRED_NRO_PREDIO,  
                        cm.tipo_procedencia,
                        cm.tipo_acto_juridico, 
                        cm.uso_nivel1, 
                        cm.uso_nivel2, 
                        cm.uso_predio,
                        ar.fecha_avaluo,
                        ar.valor_avaluo,
                        cm.valor_predio,
                        cm.nivel_desagregacion,
                        cm.amortizable,
                        cm.depreciable,
                        cm.cuenta_conbi_db
                        from conbi_movimientos cm
                        inner join  rupi_avaluo_terrenos ar on cm.PREURBANIZACI_NRO_URBANIZACION = ar.PREURBANIZACI_NRO_URBANIZACION and ar.PRED_NRO_PREDIO = cm.PRED_NRO_PREDIO 
                        where to_char(ar.fecha_avaluo,'yyyy')= v_ano
                        and   to_char(ar.fecha_avaluo,'mm') = v_mes
                        and cm.clase_cuenta = 'B' and cm.uso_predio = 1
                        and cm.nivel_desagregacion = 1
                        and cm.FECHA_MOVIMIENTO = (select max(mm.fecha_movimiento)
                                                     from   CONBI_MOVIMIENTOS mm 
                                                    where   CM.PREURBANIZACI_NRO_URBANIZACION = MM.PREURBANIZACI_NRO_URBANIZACION
                                                      and   CM.PRED_NRO_PREDIO = MM.PRED_NRO_PREDIO
                                                      and   CM.NIVEL_DESAGREGACION = MM.NIVEL_DESAGREGACION
                                                      and   ( to_char(MM.FECHA_MOVIMIENTO, 'yyyy') < v_ano  or 
                                                             (to_char(MM.FECHA_MOVIMIENTO, 'yyyy') = v_ano 
                                                             and    to_char(MM.FECHA_MOVIMIENTO, 'mm')<= v_mes))
                                                      --and   (MM.cuenta_conbi_db like '1710%' or MM.cuenta_conbi_db like '1720%' )
                                                      )
                      and     to_char((select min (mmm.fecha_movimiento)
                                    from conbi_movimientos mmm
                                    where mmm.CUENTA_CONBI_DB like '17%' and CM.PREURBANIZACI_NRO_URBANIZACION = mmm.PREURBANIZACI_NRO_URBANIZACION
                                    and   CM.PRED_NRO_PREDIO = mmm.PRED_NRO_PREDIO
                                    and   CM.NIVEL_DESAGREGACION = mmm.NIVEL_DESAGREGACION ), 'yyyymm') >= '199201'
                  order by preurbanizaci_nro_urbanizacion, pred_nro_predio
                        ) 
    
    
    loop 
    v_depreciacion := 0;
        begin /*si es terreno*/
          if   c_cv.amortizable='S' then --el predio es amortizable, ingresa a contabilidad con ctas 17
          select nvl(sum(VALOR_PREDIO),0)
              into v_suma_ingresos
              from    CONBI_MOVIMIENTOS CM 
              where   CM.PREURBANIZACI_NRO_URBANIZACION = c_cv.PREURBANIZACI_NRO_URBANIZACION
              and   CM.PRED_NRO_PREDIO = c_cv.PRED_NRO_PREDIO
              and   CM.NIVEL_DESAGREGACION = c_cv.NIVEL_DESAGREGACION
              --and   cm.tipo_movimiento = 'ING'
              and   (cm.CUENTA_CONBI_DB like '1710%' or cm.CUENTA_CONBI_DB like '1720%')
              and   ( to_char(cm.FECHA_MOVIMIENTO, 'yyyy') < v_ano  or 
                    (to_char(cm.FECHA_MOVIMIENTO, 'yyyy') = v_ano 
                    and    to_char(cm.FECHA_MOVIMIENTO, 'mm')<= v_mes))
              ;
  
              select nvl(sum(VALOR_PREDIO) ,0)
              into v_suma_egresos
              from    CONBI_MOVIMIENTOS CM 
              where   CM.PREURBANIZACI_NRO_URBANIZACION = c_cv.PREURBANIZACI_NRO_URBANIZACION
              and   CM.PRED_NRO_PREDIO = c_cv.PRED_NRO_PREDIO
              and   CM.NIVEL_DESAGREGACION = c_cv.NIVEL_DESAGREGACION
              --and   cm.tipo_movimiento = 'EGR' --and cm.tipo_transaccion = 2
              and   (cm.CUENTA_CONBI_cr like '1710%' or cm.CUENTA_CONBI_cr like '1720%')
              and   ( to_char(cm.FECHA_MOVIMIENTO, 'yyyy') < v_ano  or 
                    (to_char(cm.FECHA_MOVIMIENTO, 'yyyy') = v_ano 
                    and    to_char(cm.FECHA_MOVIMIENTO, 'mm')<= v_mes))
              ;
              
              /*Consultar la fecha de ingreso a contabilidad del predio*/
              select to_char(min (mmm.fecha_movimiento),'DD/MM/YYYY')
              into fec_ing_cont
              from conbi_movimientos mmm
              where mmm.PREURBANIZACI_NRO_URBANIZACION = c_cv.PREURBANIZACI_NRO_URBANIZACION
              and   mmm.PRED_NRO_PREDIO = c_cv.PRED_NRO_PREDIO
              and   mmm.NIVEL_DESAGREGACION = c_cv.NIVEL_DESAGREGACION
             and   (mmm.CUENTA_CONBI_DB like '1710%' or mmm.CUENTA_CONBI_DB like '1720%');

        else if c_cv.depreciable='S' then --El predio es depreciable, ingresa a contabilidad con ctas 16
          select nvl(sum(valor_predio),0)
              into v_suma_ingresos
              from    conbi_movimientos cm 
              where   cm.preurbanizaci_nro_urbanizacion = c_cv.preurbanizaci_nro_urbanizacion
              and   cm.pred_nro_predio = c_cv.pred_nro_predio
              and   cm.nivel_desagregacion = c_cv.nivel_desagregacion
              and   cm.tipo_movimiento = 'ING'
              and    (cm.cuenta_conbi_db like '1640%' or cm.cuenta_conbi_db like '1920%')
              and   cm.cuenta_conbi_db not like '19200633%'
              and   ( to_char(cm.fecha_movimiento, 'yyyy') < v_ano  or 
                    (to_char(cm.fecha_movimiento, 'yyyy') = v_ano 
                    and    to_char(cm.fecha_movimiento, 'mm')<= v_mes))
              ;
          select nvl(sum(valor_predio) ,0)
              into v_suma_egresos
              from    conbi_movimientos cm 
              where   cm.preurbanizaci_nro_urbanizacion = c_cv.preurbanizaci_nro_urbanizacion
              and   cm.pred_nro_predio = c_cv.pred_nro_predio
              and   cm.nivel_desagregacion = c_cv.nivel_desagregacion
              --and   cm.tipo_movimiento = 'EGR'
              and   (cm.cuenta_conbi_cr like '1640%' or cm.cuenta_conbi_cr like '1920%')
              and   cm.cuenta_conbi_cr not like '19200633%'
              and   ( to_char(cm.fecha_movimiento, 'yyyy') < v_ano  or 
                    (to_char(cm.fecha_movimiento, 'yyyy') = v_ano 
                    and    to_char(cm.fecha_movimiento, 'mm')<= v_mes))
              ;
          /*Consultar la fecha de ingreso a contabilidad del predio*/
            select to_char(min (mmm.fecha_movimiento),'DD/mm/YYYY')
            into fec_ing_cont
            from conbi_movimientos mmm
            where mmm.preurbanizaci_nro_urbanizacion = c_cv.preurbanizaci_nro_urbanizacion
            and   mmm.pred_nro_predio = c_cv.pred_nro_predio
            and   mmm.nivel_desagregacion = c_cv.nivel_desagregacion
            and   (mmm.cuenta_conbi_db like '1640%')
              ;    
        else
          select nvl(sum(valor_predio),0)
              into v_suma_ingresos
              from    conbi_movimientos cm 
              where   cm.preurbanizaci_nro_urbanizacion = c_cv.preurbanizaci_nro_urbanizacion
              and   cm.pred_nro_predio = c_cv.pred_nro_predio
              and   cm.nivel_desagregacion = c_cv.nivel_desagregacion
              and   cm.tipo_movimiento = 'ING'
              and    (cm.cuenta_conbi_db like '16050133%' or cm.cuenta_conbi_db like '16050234%' or cm.cuenta_conbi_db like '19200633%')
              and   ( to_char(cm.fecha_movimiento, 'yyyy') < v_ano  or 
                    (to_char(cm.fecha_movimiento, 'yyyy') = v_ano 
                    and    to_char(cm.fecha_movimiento, 'mm')<= v_mes))
              ;
          select nvl(sum(valor_predio) ,0)
              into v_suma_egresos
              from    conbi_movimientos cm 
              where   cm.preurbanizaci_nro_urbanizacion = c_cv.preurbanizaci_nro_urbanizacion
              and   cm.pred_nro_predio = c_cv.pred_nro_predio
              and   cm.nivel_desagregacion = c_cv.nivel_desagregacion
              and    (cm.cuenta_conbi_cr like '16050133%' or cm.cuenta_conbi_cr like '16050234%' or cm.cuenta_conbi_cr like '19200633%')
              and   ( to_char(cm.fecha_movimiento, 'yyyy') < v_ano  or 
                    (to_char(cm.fecha_movimiento, 'yyyy') = v_ano 
                    and    to_char(cm.fecha_movimiento, 'mm')<= v_mes))
              ;
          /*Consultar la fecha de ingreso a contabilidad del predio*/
            select to_char(min (mmm.fecha_movimiento),'DD/mm/YYYY')
            into fec_ing_cont
            from conbi_movimientos mmm
            where mmm.preurbanizaci_nro_urbanizacion = c_cv.preurbanizaci_nro_urbanizacion
            and   mmm.pred_nro_predio = c_cv.pred_nro_predio
            and   mmm.nivel_desagregacion = c_cv.nivel_desagregacion
            and   (mmm.cuenta_conbi_db like '16050133%' or mmm.cuenta_conbi_db like '16050234%' or mmm.cuenta_conbi_db like '19200633%');  
              
        end if;
            
            if c_cv.fecha_avaluo >= fec_ing_cont then -- que la fecha de avaluo sea mayor que la de incorporaci�n
                    
                    if  (v_suma_ingresos-v_suma_egresos) > 0 then --Si est� incorporado el predio
                    
                        /*Consultar el costo historico*/
                        select cd.costo_historico 
                          into v_costo_historico
                         from conbi_datos_contables cd 
                        where 
                              cd.PREURBANIZACI_NRO_URBANIZACION = c_cv.PREURBANIZACI_NRO_URBANIZACION 
                          and cd.PRED_NRO_PREDIO = c_cv.PRED_NRO_PREDIO 
                          and cd.nivel_desagregacion = c_cv.nivel_desagregacion;
                          
                          
                        v_valorizacion_mes := 0;
                        v_acumulada_mes    := 0;
                            
                        begin 
                          select depreciacion_acumulada
                          into v_acumulada_mes
                          from (select depreciacion_acumulada
                          from conbi_depreciacion_amortizacio  cda
                          where 
                          preurbanizaci_nro_urbanizacion  = c_cv.preurbanizaci_nro_urbanizacion
                          and  pred_nro_predio            = c_cv.pred_nro_predio
                          and  nivel_secundario           = c_cv.nivel_desagregacion
                          and  control_tipo_proceso       = 'VAL'
                          order by control_ano_proceso desc, control_mes_proceso desc, con_id desc
                          ) where rownum = 1;
                            
                              begin
                                --Consultar la depreciacion acumulada
                                select dep
                                  into v_depreciacion
                                  from (select depreciacion_acumulada as dep
                                  from conbi_depreciacion_amortizacio  cda
                                  where 
                                  preurbanizaci_nro_urbanizacion  = c_cv.preurbanizaci_nro_urbanizacion
                                  and  pred_nro_predio            = c_cv.pred_nro_predio
                                  and  nivel_secundario           = c_cv.nivel_desagregacion
                                  and  control_tipo_proceso       = 'DEP'
                                  and  control_ano_proceso        = v_ano
                                  and  control_mes_proceso        = v_mes
                                  ) where rownum = 1;

                                v_valorizacion_mes := c_cv.valor_avaluo - (v_costo_historico - v_depreciacion) - v_acumulada_mes;   
                              exception when no_data_found then
                                v_valorizacion_mes := c_cv.valor_avaluo - v_costo_historico - v_acumulada_mes;
                              end;
                              
                          exception
                             when   no_data_found then
                              begin
                                --Consultar la depreciacion acumulada
                                select dep
                                  into v_depreciacion
                                  from (select depreciacion_acumulada as dep
                                  from conbi_depreciacion_amortizacio  cda
                                  where 
                                  preurbanizaci_nro_urbanizacion  = c_cv.preurbanizaci_nro_urbanizacion
                                  and  pred_nro_predio            = c_cv.pred_nro_predio
                                  and  nivel_secundario           = c_cv.nivel_desagregacion
                                  and  control_tipo_proceso       = 'DEP'
                                  and  control_ano_proceso        = v_ano
                                  and  control_mes_proceso        = v_mes
                                  ) where rownum = 1;

                                v_valorizacion_mes := c_cv.valor_avaluo - (v_costo_historico - v_depreciacion);   
                              exception when no_data_found then
                                v_valorizacion_mes := c_cv.valor_avaluo - v_costo_historico;
                              end;
                        end;  
    
                        if  v_valorizacion_mes < 0 THEN --Desvalorizacion
                        
                              select  CA.CUENTA_CONBI_DB, CA.CUENTA_CONBI_CR     
                              into    v_CUENTA_CONBI_DB, v_CUENTA_CONBI_CR
                              from    CONBI_PR_PLAN_CONTABLE_ALTERNO CA 
                              where 
                                  CA.USO_PREDIO=c_cv.USO_PREDIO 
                              and CA.USO_NIVEL1=c_cv.USO_NIVEL1 
                              and CA.USO_NIVEL2=c_cv.USO_NIVEL2 
                              and CA.TIPO_ACTO_JURIDICO=c_cv.TIPO_ACTO_JURIDICO 
                              and CA.TIPO_PROCEDENCIA=c_cv.TIPO_PROCEDENCIA
                              AND CA.CLASE_CUENTA= 'B' 
                              and CA.TIPO_MOVIMIENTO='EDESV';
                       else    --Valorizaci�n     
                              select  CA.CUENTA_CONBI_DB, CA.CUENTA_CONBI_CR     
                              into    v_CUENTA_CONBI_DB, v_CUENTA_CONBI_CR
                              from    CONBI_PR_PLAN_CONTABLE_ALTERNO CA 
                              where 
                                  CA.USO_PREDIO=c_cv.USO_PREDIO 
                              and CA.USO_NIVEL1=c_cv.USO_NIVEL1 
                              and CA.USO_NIVEL2=c_cv.USO_NIVEL2 
                              and CA.TIPO_ACTO_JURIDICO=c_cv.TIPO_ACTO_JURIDICO 
                              and CA.TIPO_PROCEDENCIA=c_cv.TIPO_PROCEDENCIA
                              AND CA.CLASE_CUENTA= 'B' 
                              and CA.TIPO_MOVIMIENTO='VAL';
                       end if;
                  
                        INSERT INTO CONBI_DEP_AMO_PRUEBAS2(
                                                  preurbanizaci_nro_urbanizacion, 
                                                  pred_nro_predio,
                                                  NIVEL_SECUNDARIO,
                                                  CONTROL_ANO_VALORIZACION,        
                                                  CONTROL_MES_VALORIZACION,
                                                  control_tipo_proceso,               --  AMO
                                                  DEPRECIACION, 
                                                  CUENTA_CONBI_DB,                    --  VALOR_PREDIO  
                                                  CUENTA_CONBI_CR,
                                                  VALOR_AVALUO,
                                                  COSTO_HISTORICO,
                                                  valorizacion_mes,                   /* Al�. Acumulada*/
                                                  acumulada_mes,
                                                  ID,
                                                  FECHA_INCORPORA,
                                                  FECHA_AVALUO,
                                                  CUENTA_MOVIMIENTO,
                                                  VAL_ACUMULADA
                                                  )
                        VALUES    (
                                  c_cv.PREURBANIZACI_NRO_URBANIZACION,     
                                  c_cv.PRED_NRO_PREDIO,                    
                                  c_cv.nivel_desagregacion,                
                                  v_ano,           
                                  v_mes,
                                  'VAL',
                                  v_depreciacion,
                                  v_CUENTA_CONBI_DB,                       
                                  v_CUENTA_CONBI_CR,
                                  c_cv.valor_avaluo,
                                  c_cv.valor_predio,
                                  v_valorizacion_mes,
                                  v_acumulada_mes,
                                  SEQ_CONBI_AMO_PLANB.NEXTVAL,
                                  fec_ing_cont,
                                  C_CV.fecha_avaluo,
                                  c_cv.cuenta_conbi_db,
                                  (v_valorizacion_mes+v_acumulada_mes)
                                  );
                  end if;
            end if;
        end if;
        end;
    end loop;
    commit;
end;