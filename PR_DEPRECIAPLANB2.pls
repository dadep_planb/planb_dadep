create or replace procedure      pr_depreciaplanb2 (V_ANO  VARCHAR2, V_MES  VARCHAR2) 
AS

urbanizacion            number:= 0;
predio                  number:= 0;
costo_adquisicion       number:= 0;
cuenta_conbi_db         number:= 0;
cuenta_conbi_cr         number:= 0;
fecha                   number:= 0;
nueva_alicuota          number:= 0;
depreciacion            number:= 0;
tvidautilsindepreciar   number:= 0;
alicuota_acumulada_ant  number:= 0;
nueva_alicuota_acumulada  number:= 0;
alicuota_anterior       number:= 0;    
valor_neto              number:= 0;
alicuota_acumulada      number:= 0;
v_CUENTA_CONBI_DB       varchar2(30):= 0;
v_CUENTA_CONBI_CR       varchar2(30):= 0;
vida_util_total         number:= 600;
v_count                 number:= 600;
conteo_rupi             NUMBER:= 0;
n                       number:=1;
bandera                 number:=0;
predio_activo           number:=0;
v_valor_alicuota_anterior_ac          number:=0;
v_valor_alicuota_anterior number:=0;
v_tipo_movimiento       number := 0;
v_tipo_transaccion      number := 0;
v_alicuota_traslado     number := 0;
v_valor_predio_traslado number := 0;
v_movimiento            varchar2(30):= 0;
v_suma_egresos          number :=0;
v_suma_ingresos         number :=0;
v_suma_egresosC         number :=0;
v_suma_ingresosC        number :=0;
v_ult_mov               varchar2(30):= 0;
v_ult_tran              varchar2(30):= 0;
v_costo_historico       number  :=  0;
type movs is varray(3) of varchar2(5);
conteo_movs movs;
begin
delete from conbi_dep_amo_pruebas;

commit;

   
                      
      /*Predios depreciables en  la fecha de consulta*/
     insert into  tmp_depreciacion1(PREURBANIZACI_NRO_URBANIZACION,
                                  PRED_NRO_PREDIO,
                                  NIVEL_DESAGREGACION,
                                  fecha_ingreso_contabilidad,
                                  CUENTA_CONBI_DB,
                                  CUENTA_CONBI_CR,
                                  VALOR_PREDIO,
                                  depreciable,
                                  TIPO_MOVIMIENTO,
                                  TIPO_ACTO_JURIDICO,
                                  USO_PREDIO,
                                  USO_NIVEL1,
                                  USO_NIVEL2,
                                  TIPO_PROCEDENCIA,
                                  TIPO_TRANSACCION,
                                  CLASE_CUENTA,
                                  FECHA_MOVIMIENTO) 
                          select distinct 
                                      cm.PREURBANIZACI_NRO_URBANIZACION,
                                      cm.PRED_NRO_PREDIO,
                                      cm.NIVEL_DESAGREGACION, 
                                      (select min (mmm.fecha_movimiento)
                                        from conbi_movimientos mmm
                                        where mmm.CUENTA_CONBI_DB like '1640%' and CM.PREURBANIZACI_NRO_URBANIZACION = mmm.PREURBANIZACI_NRO_URBANIZACION
                                        and   CM.PRED_NRO_PREDIO = mmm.PRED_NRO_PREDIO
                                        and   CM.NIVEL_DESAGREGACION = mmm.NIVEL_DESAGREGACION ) as fecha_ingreso_contabilidad , -- cdc.fecha_ingreso_contabilidad,
                                      cm.CUENTA_CONBI_DB,
                                      cm.CUENTA_CONBI_CR,
                                      cm.VALOR_PREDIO,
                                      cm.depreciable,
                                      cm.TIPO_MOVIMIENTO,
                                      cm.TIPO_ACTO_JURIDICO,
                                      cm.USO_PREDIO,
                                      cm.USO_NIVEL1,
                                      cm.USO_NIVEL2,
                                      cm.TIPO_PROCEDENCIA,
                                      cm.TIPO_TRANSACCION,
                                      cm.CLASE_CUENTA,
                                      cm.FECHA_MOVIMIENTO
                      from      conbi_movimientos cm
                      left join conbi_datos_contables cdc on cm.PREURBANIZACI_NRO_URBANIZACION = cdc.preurbanizaci_nro_urbanizacion and
                                cm.pred_nro_predio = cdc.pred_nro_predio and cdc.nivel_desagregacion = cm.nivel_desagregacion           
                      where     cm.FECHA_MOVIMIENTO = (select max(mm.fecha_movimiento)
                                                         from   CONBI_MOVIMIENTOS mm 
                                                        where   CM.PREURBANIZACI_NRO_URBANIZACION = MM.PREURBANIZACI_NRO_URBANIZACION
                                                          and   CM.PRED_NRO_PREDIO = MM.PRED_NRO_PREDIO
                                                          and   CM.NIVEL_DESAGREGACION = MM.NIVEL_DESAGREGACION
                                                          and   ( to_char(MM.FECHA_MOVIMIENTO, 'yyyy') < v_ano  or 
                                                                 (to_char(MM.FECHA_MOVIMIENTO, 'yyyy') = v_ano 
                                                                 and    to_char(MM.FECHA_MOVIMIENTO, 'mm')<= v_mes))
                                                          --and   MM.cuenta_conbi_db not like '31050303%'
                                                          and   (MM.cuenta_conbi_db like '1640%' or MM.cuenta_conbi_cr like '1640%')
                                                          )
                                                          --and   MM.FECHA_MOVIMIENTO <('01/jul/2013'))
                      --and     cm.tipo_movimiento<>'EGR'
                      --and     to_char(fecha_ingreso_contabilidad, 'yyyymm') >= '199201' 
                      and     to_char((select min (mmm.fecha_movimiento)
                                        from conbi_movimientos mmm
                                        where mmm.CUENTA_CONBI_DB like '1640%' and CM.PREURBANIZACI_NRO_URBANIZACION = mmm.PREURBANIZACI_NRO_URBANIZACION
                                        and   CM.PRED_NRO_PREDIO = mmm.PRED_NRO_PREDIO
                                        and   CM.NIVEL_DESAGREGACION = mmm.NIVEL_DESAGREGACION ), 'yyyymm') >= '199201'
                      and     cm.depreciable='S'
                      and     cm.NIVEL_DESAGREGACION            <>     1  
                      and    (cm.cuenta_conbi_db not like '83%' and cm.cuenta_conbi_db not like '1605%'  
                      and     cm.cuenta_conbi_db not like '16409024%' and cm.cuenta_conbi_db not like '19200633%' 
                      and     cm.cuenta_conbi_db not like '17%')
                      ;
                      
   commit;                     
      
      for c_c in (SELECT distinct ta.PREURBANIZACI_NRO_URBANIZACION, ta.PRED_NRO_PREDIO, ta.NIVEL_DESAGREGACION, 
                        cd.valor_costo_adquisicion,
                        ta.tipo_movimiento,
                        ta.fecha_ingreso_contabilidad,
                        ta.USO_PREDIO,
                        ta.USO_NIVEL1,
                        ta.USO_NIVEL2,
                        ta.TIPO_ACTO_JURIDICO,
                        ta.TIPO_PROCEDENCIA,
                        ta.CLASE_CUENTA,  
                        ta.tipo_transaccion,
                        ta.fecha_movimiento,
                        ta.cuenta_conbi_db,
                        abs(round(MONTHS_BETWEEN( TO_DATE(TO_CHAR(ta.fecha_ingreso_contabilidad,'YYYYMM'),'YYYYMM'),
                        TO_DATE(TO_CHAR(v_ano||v_mes),'YYYYMM')),0)) as meses,
                        abs(round(MONTHS_BETWEEN( TO_DATE(TO_CHAR(ta.fecha_movimiento,'YYYYMM'),'YYYYMM'),
                        TO_DATE(TO_CHAR(v_ano||v_mes),'YYYYMM')),0)) as mesesuno,
                        ta.valor_predio
                 from   tmp_depreciacion1 ta, conbi_datos_contables  cd
                 where    ta.PREURBANIZACI_NRO_URBANIZACION =     cd.PREURBANIZACI_NRO_URBANIZACION
                 and      ta.PRED_NRO_PREDIO                =     cd.PRED_NRO_PREDIO
                 and      ta.NIVEL_DESAGREGACION            =     cd.NIVEL_DESAGREGACION
                 and      ta.NIVEL_DESAGREGACION            <>     1                 
                 and      ta.DEPRECIABLE='S' 
                 and      (to_char(ta.fecha_ingreso_contabilidad,'yyyy')  < v_ano  or 
                          (to_char(ta.fecha_ingreso_contabilidad,'yyyy')  = v_ano 
                 and      to_char(ta.fecha_ingreso_contabilidad, 'mm')   <= v_mes)) 
                 order by ta.PREURBANIZACI_NRO_URBANIZACION, ta.PRED_NRO_PREDIO
                 )      
     loop          
     
     /*Consultar el ultimo movimiento del predio en contabilidad*/
      select cm.TIPO_MOVIMIENTO,cm.TIPO_TRANSACCION
      into v_ult_mov, v_ult_tran
      from      conbi_movimientos cm
      where     cm.FECHA_MOVIMIENTO = (select max(mm.fecha_movimiento)
                                         from   CONBI_MOVIMIENTOS mm 
                                        where   CM.PREURBANIZACI_NRO_URBANIZACION = MM.PREURBANIZACI_NRO_URBANIZACION
                                          and   CM.PRED_NRO_PREDIO = MM.PRED_NRO_PREDIO
                                          and   CM.NIVEL_DESAGREGACION = MM.NIVEL_DESAGREGACION
                                          and   MM.FECHA_MOVIMIENTO <('01/jul/2013')
                                          and   (MM.CUENTA_CONBI_DB like '1640%' or MM.CUENTA_CONBI_CR like '1640%')
                                          )
    
      and     cm.PREURBANIZACI_NRO_URBANIZACION = c_c.PREURBANIZACI_NRO_URBANIZACION
      and     cm.PRED_NRO_PREDIO = c_c.PRED_NRO_PREDIO
      and     cm.NIVEL_DESAGREGACION = c_c.NIVEL_DESAGREGACION;
                  
     if v_ult_mov = 'EGR' and v_ult_tran=2 then
        bandera := 0;
     else
     
             
            bandera:=0;
                       
            v_valor_alicuota_anterior := 0;
            v_valor_alicuota_anterior_ac := 0;
         
                --if c_cU.TIPO_MOVIMIENTO = 'ING' then  
                  /*Sumar todos los movimientos y ver si el saldo es cero*/
                  begin
                    select nvl(sum(VALOR_PREDIO),0)
                    into v_suma_ingresos
                    from    CONBI_MOVIMIENTOS CM 
                    where   CM.PREURBANIZACI_NRO_URBANIZACION = c_c.PREURBANIZACI_NRO_URBANIZACION
                    and   CM.PRED_NRO_PREDIO = c_c.PRED_NRO_PREDIO
                    and   CM.NIVEL_DESAGREGACION = c_c.NIVEL_DESAGREGACION
                    and   cm.tipo_movimiento = 'ING'
                    and   (cm.CUENTA_CONBI_DB like '1640%' or cm.CUENTA_CONBI_CR like '1640%')
                    and   CM.FECHA_MOVIMIENTO <('01/jul/2013')
                    ;
                    
                    select nvl(sum(VALOR_PREDIO) ,0)
                    into v_suma_egresos
                    from    CONBI_MOVIMIENTOS CM 
                    where   CM.PREURBANIZACI_NRO_URBANIZACION = c_c.PREURBANIZACI_NRO_URBANIZACION
                    and   CM.PRED_NRO_PREDIO = c_c.PRED_NRO_PREDIO
                    and   CM.NIVEL_DESAGREGACION = c_c.NIVEL_DESAGREGACION
                    and   cm.tipo_movimiento = 'EGR' --and cm.tipo_transaccion = 2
                    and   (cm.CUENTA_CONBI_DB like '1640%' or cm.CUENTA_CONBI_CR like '1640%')
                    and   CM.FECHA_MOVIMIENTO <('01/jul/2013')
                    ;
                    
                    if (v_suma_ingresos-v_suma_egresos) > 0 then
                    
                      /*Consultar el costo historico hasta la fecha de consulta*/
                      select nvl(sum(VALOR_PREDIO),0)
                      into v_suma_ingresosC
                      from    CONBI_MOVIMIENTOS CM 
                      where   CM.PREURBANIZACI_NRO_URBANIZACION = c_c.PREURBANIZACI_NRO_URBANIZACION
                      and   CM.PRED_NRO_PREDIO = c_c.PRED_NRO_PREDIO
                      and   CM.NIVEL_DESAGREGACION = c_c.NIVEL_DESAGREGACION
                      and   cm.tipo_movimiento = 'ING'
                      and   (cm.CUENTA_CONBI_DB like '1640%' or cm.CUENTA_CONBI_CR like '1640%')
                      and   ( to_char(cm.FECHA_MOVIMIENTO, 'yyyy') < v_ano  or 
                            (to_char(cm.FECHA_MOVIMIENTO, 'yyyy') = v_ano 
                            and    to_char(cm.FECHA_MOVIMIENTO, 'mm')<= v_mes))
                      ;
                      select nvl(sum(VALOR_PREDIO) ,0)
                      into v_suma_egresosC
                      from    CONBI_MOVIMIENTOS CM 
                      where   CM.PREURBANIZACI_NRO_URBANIZACION = c_c.PREURBANIZACI_NRO_URBANIZACION
                      and   CM.PRED_NRO_PREDIO = c_c.PRED_NRO_PREDIO
                      and   CM.NIVEL_DESAGREGACION = c_c.NIVEL_DESAGREGACION
                      and   cm.tipo_movimiento = 'EGR' --and cm.tipo_transaccion = 2
                      and   (cm.CUENTA_CONBI_DB like '1640%' or cm.CUENTA_CONBI_CR like '1640%')
                      and   ( to_char(cm.FECHA_MOVIMIENTO, 'yyyy') < v_ano  or 
                            (to_char(cm.FECHA_MOVIMIENTO, 'yyyy') = v_ano 
                            and    to_char(cm.FECHA_MOVIMIENTO, 'mm')<= v_mes))
                      ;
                       v_costo_historico := v_suma_ingresosC-v_suma_egresosC;
                       
                      if   v_costo_historico=0 then
                        bandera := 0;
                      else
                       bandera:=1;
                        select CA.CUENTA_CONBI_DB, CA.CUENTA_CONBI_CR     
                          into v_CUENTA_CONBI_DB, v_CUENTA_CONBI_CR
                          from CONBI_PR_PLAN_CONTABLE_ALTERNO CA 
                          where 
                          CA.USO_PREDIO=c_c.USO_PREDIO 
                          and CA.USO_NIVEL1=c_c.USO_NIVEL1 
                          and CA.USO_NIVEL2=c_c.USO_NIVEL2 
                          and CA.TIPO_ACTO_JURIDICO=c_c.TIPO_ACTO_JURIDICO 
                          and CA.TIPO_PROCEDENCIA=c_c.TIPO_PROCEDENCIA
                          AND CA.CLASE_CUENTA= c_c.CLASE_CUENTA 
                          and CA.TIPO_MOVIMIENTO='DEP'
                          ;
                      end if;
                    end if;
                  exception
                    when others then
                      bandera := 0;
                  end;
                --end if;
                                
                if bandera=1 then
                  begin            
                    select depreciacion, depreciacion_acumulada
                    into v_valor_alicuota_anterior, v_valor_alicuota_anterior_ac
                    from (select depreciacion, depreciacion_acumulada
                    from conbi_depreciacion_amortizacio  
                    where 
                    PREURBANIZACI_NRO_URBANIZACION  = c_c.PREURBANIZACI_NRO_URBANIZACION
                    and  PRED_NRO_PREDIO            = c_c.PRED_NRO_PREDIO
                    and  NIVEL_SECUNDARIO           = c_c.NIVEL_DESAGREGACION
                    and  control_tipo_proceso       = 'DEP'
                    order by control_ano_proceso desc, control_mes_proceso desc, con_id desc
                    ) where rownum = 1;
                    
                      costo_adquisicion         := v_costo_historico;--nvl(c_c.VALOR_PREDIO,1);
                      cuenta_conbi_db           := v_CUENTA_CONBI_DB;
                      cuenta_conbi_cr           := v_CUENTA_CONBI_CR;
                      fecha                     := c_c.meses;
                      nueva_alicuota            := c_c.meses + 1;                                                         
                      tvidautilsindepreciar     := vida_util_total - nueva_alicuota;
                      depreciacion              := round((costo_adquisicion/600),1);
                      alicuota_anterior         := v_valor_alicuota_anterior;
                      alicuota_acumulada_ant    := v_valor_alicuota_anterior_ac;
                      alicuota_acumulada        := alicuota_acumulada_ant + depreciacion; 
                      valor_neto                := costo_adquisicion - alicuota_acumulada;
                      NUEVA_ALICUOTA_ACUMULADA  := alicuota_acumulada + depreciacion;  --round((c_c.VALOR_PREDIO/600),1) * nueva_alicuota ;
                      
                      /*Predios que tienen una reclasificación por un menor valor y el cálculo da negativo*/
                      if valor_neto < 0 then
                        alicuota_acumulada_ant := c_c.meses * depreciacion;
                        alicuota_acumulada := alicuota_acumulada_ant + depreciacion;
                        NUEVA_ALICUOTA_ACUMULADA  := alicuota_acumulada + depreciacion;
                        valor_neto                := costo_adquisicion - alicuota_acumulada;
                      end if;
                    exception
                      when no_data_found then
                          begin
                            select cm.alicuota_traslado, cm.valor_predio, MIN(cm.fecha_movimiento)
                            into v_alicuota_traslado, v_valor_predio_traslado, v_movimiento 
                            from conbi_movimientos cm 
                            where cuenta_conbi_db like '31050303%'
                            and cm.tipo_movimiento= 'ING' 
                            and cm.preurbanizaci_nro_urbanizacion = c_c.preurbanizaci_nro_urbanizacion
                            and cm.pred_nro_predio = c_c.pred_nro_predio
                            and to_char(cm.fecha_movimiento,'yyyymm') = to_char(c_c.fecha_movimiento,'yyyymm') 
                            group by cm.preurbanizaci_nro_urbanizacion, cm.pred_nro_predio, cm.alicuota_traslado, cm.valor_predio, cm.fecha_movimiento;
                            
                            costo_adquisicion         := v_costo_historico;--nvl(c_c.VALOR_PREDIO,1);
                            cuenta_conbi_db           := v_CUENTA_CONBI_DB;
                            cuenta_conbi_cr           := v_CUENTA_CONBI_CR;
                            c_c.meses                 := v_alicuota_traslado;
                            fecha                     := c_c.meses;
                            nueva_alicuota            := c_c.meses + 1;
                            depreciacion              := round((costo_adquisicion/600),1); 
                            tvidautilsindepreciar     := 600 - nueva_alicuota;
                            alicuota_acumulada_ant    := v_valor_predio_traslado;                                                   
                            nueva_alicuota_acumulada  := v_valor_predio_traslado + depreciacion ;
                            alicuota_anterior         := 0;    
                            alicuota_acumulada        := depreciacion + v_valor_predio_traslado;
                            valor_neto                := costo_adquisicion - alicuota_acumulada;
                          exception when no_data_found then
                            costo_adquisicion         := v_costo_historico;--nvl(c_c.VALOR_PREDIO,1);
                            cuenta_conbi_db           := v_CUENTA_CONBI_DB;
                            cuenta_conbi_cr           := v_CUENTA_CONBI_CR;
                            fecha                     := c_c.meses;
                            nueva_alicuota            := c_c.meses + 1;
                            depreciacion              := round((costo_adquisicion/600),1) * nueva_alicuota ; 
                            tvidautilsindepreciar     := 600 - nueva_alicuota;
                            alicuota_acumulada_ant    := 0;                                                   
                            nueva_alicuota_acumulada  := round((costo_adquisicion/600),1) * nueva_alicuota ;
                            alicuota_anterior         := 0;    
                            valor_neto                := costo_adquisicion - depreciacion * nueva_alicuota;
                            alicuota_acumulada        := depreciacion * nueva_alicuota;
                          end;
                   end;
                   
                   
                   
                   
                  INSERT INTO CONBI_DEP_AMO_PRUEBAS(
                                                      preurbanizaci_nro_urbanizacion, 
                                                      pred_nro_predio,
                                                      NIVEL_SECUNDARIO,
                                                      DEPRECIACION,   
                                                      DEPRECIACION_ACUMULADA,
                                                      DEPRECIACION_AJUSTADA,        
                                                      axi_depreciacion,  
                                                      vida_util,                    
                                                      tvidautilsindepreciar,        
                                                      tiempo_dep_amo_calculada,     
                                                      control_ano_proceso,        
                                                      control_mes_proceso,
                                                      control_tipo_proceso,         
                                                      valor_neto,                   
                                                      TRANSACCION,
                                                      TIPO_MOVIMIENTO,
                                                      ID,
                                                      FECHA_INCORPORA,
                                                      COSTO_HISTORICO,
                                                      NUEVA_ALICUOTA_ACUMULADA,       
                                                      VALOR_ALICUOTA_ANT,             
                                                      ALICUOTA_ANTERIOR,
                                                      cuenta_conbi_db,
                                                      cuenta_conbi_cr,
                                                      CUENTA_MOVIMIENTO,
                                                      USO_PREDIO,
                                                      USO_NIVEL1,
                                                      USO_NIVEL2,
                                                      TIPO_ACTO_JURIDICO,
                                                      TIPO_PROCEDENCIA,
                                                      CLASE_CUENTA
                                                      )
                  VALUES    (
                              c_c.PREURBANIZACI_NRO_URBANIZACION,     
                              c_c.PRED_NRO_PREDIO,                    
                              c_c.nivel_desagregacion,                 
                              depreciacion,                           
                              alicuota_acumulada_ant,                 
                              0,                                      
                              0,                                      
                              vida_util_total,                        
                              tvidautilsindepreciar,                  
                              nueva_alicuota,                         
                              v_ano,                                  
                              v_mes,                                  
                              'DEP',                                  
                              valor_neto,                              
                              1,                                      
                              'CAL',                                  
                              SEQ_CONBI_AMO_PLANB.NEXTVAL,             
                              c_c.fecha_ingreso_contabilidad,                  /* FECHA COSTO ADQUISICION */
                              costo_adquisicion,--c_c.VALOR_PREDIO,                      
                              alicuota_acumulada,                     
                              alicuota_anterior,                      
                              nueva_alicuota,                         
                              cuenta_conbi_db,                        
                              cuenta_conbi_cr,
                              c_c.CUENTA_CONBI_DB,
                              c_c.USO_PREDIO,
                              c_c.USO_NIVEL1,
                              c_c.USO_NIVEL2,
                              c_c.TIPO_ACTO_JURIDICO,
                              c_c.TIPO_PROCEDENCIA,
                              c_c.CLASE_CUENTA 
                              );   
                    
        
                  end if;
            end if;
          --end loop;
        --end loop;
     end loop;
     commit;
     DELETE tmp_depreciacion1;
     commit;
end;