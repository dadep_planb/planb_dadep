create or replace procedure      pr_depreciaplanb (V_ANO  VARCHAR2, V_MES  VARCHAR2) 
AS

meses                   number:= 0;
costo_adquisicion       number:= 0;
cuenta_conbi_db         number:= 0;
cuenta_conbi_cr         number:= 0;
fecha                   number:= 0;
nueva_alicuota          number:= 0;
depreciacion            number:= 0;
tvidautilsindepreciar   number:= 0;
alicuota_acumulada_ant  number:= 0;
alicuota_anterior       number:= 0;    
valor_neto              number:= 0;
alicuota_acumulada      number:= 0;
bandera                 number:= 0;
v_tipo_movimiento       number:= 0;
v_tipo_transaccion      number:= 0;
v_alicuota_traslado     number:= 0;
v_valor_predio_traslado number:= 0;
v_suma_egresos          number:= 0;
v_suma_ingresos         number:= 0;
v_suma_egresosC         number:= 0;
v_suma_ingresosC        number:= 0;
v_costo_historico       number:= 0;
vida_util_total         number:= 600;
nueva_alicuota_acumulada      number:= 0;
v_costo_historico_ant         number:= 0;
v_valor_alicuota_anterior_ac  number:= 0;
v_valor_alicuota_anterior     number:= 0;
fec_ing_cont            varchar2(20):= 0;
v_movimiento            varchar2(30):= 0;


begin
delete from conbi_dep_amo_pruebas;

commit;
          
  /*Predios depreciables en  la fecha de consulta*/
    for c_c in (SELECT distinct 
                                cm.preurbanizaci_nro_urbanizacion,
                                cm.pred_nro_predio,
                                cm.nivel_desagregacion, 
                                cm.cuenta_conbi_db,
                                cm.cuenta_conbi_cr,
                                cm.valor_predio,
                                cm.depreciable,
                                cm.tipo_movimiento,
                                cm.tipo_acto_juridico,
                                cm.uso_predio,
                                cm.uso_nivel1,
                                cm.uso_nivel2,
                                cm.tipo_procedencia,
                                cm.tipo_transaccion,
                                cm.clase_cuenta,
                                cm.fecha_movimiento
                      from      conbi_movimientos cm
                      where     cm.fecha_movimiento = (select   max(mm.fecha_movimiento)
                                                         from   conbi_movimientos mm 
                                                        where   cm.preurbanizaci_nro_urbanizacion = mm.preurbanizaci_nro_urbanizacion
                                                          and   cm.pred_nro_predio = mm.pred_nro_predio
                                                          and   cm.nivel_desagregacion = mm.nivel_desagregacion
                                                          and   (to_char(mm.fecha_movimiento, 'yyyy') < v_ano  or 
                                                                (to_char(mm.fecha_movimiento, 'yyyy') = v_ano 
                                                                 and to_char(mm.fecha_movimiento, 'mm')<= v_mes))
                                                          and   (mm.cuenta_conbi_db like '1640%' 
                                                                 or mm.cuenta_conbi_db like '19200601%' 
                                                                 or mm.cuenta_conbi_db like '19200602%' 
                                                                 or mm.cuenta_conbi_db like '19200609%')
                                                          )
                      and     to_char((select min (mmm.fecha_movimiento)
                                        from conbi_movimientos mmm
                                        where (mmm.cuenta_conbi_db like '1640%'
                                                or mmm.cuenta_conbi_db like '19200601%' 
                                                or mmm.cuenta_conbi_db like '19200602%' 
                                                or mmm.cuenta_conbi_db like '19200609%')
                      and     cm.preurbanizaci_nro_urbanizacion = mmm.preurbanizaci_nro_urbanizacion
                      and     cm.pred_nro_predio = mmm.pred_nro_predio
                      and     cm.nivel_desagregacion = mmm.nivel_desagregacion ), 'yyyymm') >= '199201'
                      and     cm.depreciable='S'
                      and     cm.nivel_desagregacion            <>     1  
                      and     (cm.cuenta_conbi_db not like '83%' and cm.cuenta_conbi_db not like '1605%'  and    
                               cm.cuenta_conbi_db not like '16409024%' and cm.cuenta_conbi_db not like '19200633%' and 
                               cm.cuenta_conbi_db not like '17%')
                      /*and     cm.preurbanizaci_nro_urbanizacion = 2
                      and     cm.pred_nro_predio = 439*/
                      order by cm.preurbanizaci_nro_urbanizacion asc, cm.pred_nro_predio asc, cm.nivel_desagregacion asc
                 )      
     loop          
     
        bandera:=0;  
        v_valor_alicuota_anterior    := 0;
        v_valor_alicuota_anterior_ac := 0;
      
        /*Sumar todos los movimientos y ver si el saldo es cero*/
        begin
         select nvl(sum(valor_predio),0)
          into v_suma_ingresos
          from    conbi_movimientos cm 
          where   cm.preurbanizaci_nro_urbanizacion = c_c.preurbanizaci_nro_urbanizacion
          and   cm.pred_nro_predio = c_c.pred_nro_predio
          and   cm.nivel_desagregacion = c_c.nivel_desagregacion
          and   cm.tipo_movimiento = 'ING'
          and   (cm.cuenta_conbi_db like '1640%' or cm.cuenta_conbi_db like '1920%')
          and   cm.cuenta_conbi_db not like '19200633%'
          and   cm.fecha_movimiento <('01/jul/2013')
          ;
                    
          select nvl(sum(valor_predio) ,0)
          into v_suma_egresos
          from    conbi_movimientos cm 
          where   cm.preurbanizaci_nro_urbanizacion = c_c.preurbanizaci_nro_urbanizacion
          and   cm.pred_nro_predio = c_c.pred_nro_predio
          and   cm.nivel_desagregacion = c_c.nivel_desagregacion
          --and   cm.tipo_movimiento = 'EGR' 
          and   (cm.cuenta_conbi_cr like '1640%' or cm.cuenta_conbi_cr like '1920%')
          and   cm.cuenta_conbi_cr not like '19200633%'
          and   cm.fecha_movimiento <('01/jul/2013')
          ;
                    
          if (v_suma_ingresos-v_suma_egresos) > 0 then
            /*Consultar el costo historico hasta la fecha de consulta*/
            select nvl(sum(valor_predio),0)
              into v_suma_ingresosC
              from conbi_movimientos cm 
             where cm.preurbanizaci_nro_urbanizacion = c_c.preurbanizaci_nro_urbanizacion
               and cm.pred_nro_predio = c_c.pred_nro_predio
               and cm.nivel_desagregacion = c_c.nivel_desagregacion
               --and cm.tipo_movimiento = 'ING'
               and (cm.cuenta_conbi_db like '1640%' or cm.cuenta_conbi_db like '1920%')
               and  cm.cuenta_conbi_db not like '19200633%'
               and (to_char(cm.fecha_movimiento, 'yyyy') < v_ano  or 
                   (to_char(cm.fecha_movimiento, 'yyyy') = v_ano 
                    and to_char(cm.fecha_movimiento, 'mm')<= v_mes));
                   
            select nvl(sum(valor_predio) ,0)
              into v_suma_egresosC
              from conbi_movimientos cm 
             where cm.preurbanizaci_nro_urbanizacion = c_c.preurbanizaci_nro_urbanizacion
               and cm.pred_nro_predio = c_c.pred_nro_predio
               and cm.nivel_desagregacion = c_c.nivel_desagregacion
            --and   cm.tipo_movimiento = 'EGR'
               and (cm.cuenta_conbi_cr like '1640%' or cm.cuenta_conbi_cr like '1920%')
               and  cm.cuenta_conbi_cr not like '19200633%'
               and (to_char(cm.fecha_movimiento, 'yyyy') < v_ano  or 
                    (to_char(cm.fecha_movimiento, 'yyyy') = v_ano 
                     and to_char(cm.fecha_movimiento, 'mm')<= v_mes));
                       
             v_costo_historico := v_suma_ingresosC-v_suma_egresosC;
             
            if   v_costo_historico>0 then
              --Consultar las cuentas del movimiento
              select ca.cuenta_conbi_db, ca.cuenta_conbi_cr     
                into cuenta_conbi_db, cuenta_conbi_cr
                from conbi_pr_plan_contable_alterno ca 
               where 
                    ca.uso_predio = c_c.uso_predio 
                and ca.uso_nivel1 = c_c.uso_nivel1 
                and ca.uso_nivel2 = c_c.uso_nivel2 
                and ca.tipo_acto_juridico = c_c.tipo_acto_juridico 
                and ca.tipo_procedencia = c_c.tipo_procedencia
                and ca.clase_cuenta = c_c.clase_cuenta  
                and ca.tipo_movimiento = 'DEP';
                
              --Consultar la fecha de ingreso a contabilidad del predio
             select to_char(min (mmm.fecha_movimiento),'DD/MM/YYYY')
               into fec_ing_cont
               from conbi_movimientos mmm
              where mmm.preurbanizaci_nro_urbanizacion = c_c.preurbanizaci_nro_urbanizacion
                and mmm.pred_nro_predio = c_c.pred_nro_predio
                and mmm.nivel_desagregacion = c_c.nivel_desagregacion
                and (mmm.cuenta_conbi_db like '1640%' or mmm.cuenta_conbi_db like '1920%')
               and  mmm.cuenta_conbi_cr not like '19200633%';
                    
              /*Consultar los meses de depreciaci�n*/
              begin    
                v_valor_alicuota_anterior := 0;
                v_valor_alicuota_anterior_ac := 0;
                v_costo_historico_ant := 0;
                meses := 0;
                /*Consultar los datos del mes anterior*/
                select depreciacion, depreciacion_acumulada,val_total,tiempo_dep_amo_calculada
                into v_valor_alicuota_anterior, v_valor_alicuota_anterior_ac,v_costo_historico_ant,meses
                from (select depreciacion, depreciacion_acumulada,val_total,tiempo_dep_amo_calculada
                from conbi_depreciacion_amortizacio  
                where 
                preurbanizaci_nro_urbanizacion  = c_c.preurbanizaci_nro_urbanizacion
                and  pred_nro_predio            = c_c.pred_nro_predio
                and  nivel_secundario           = c_c.nivel_desagregacion
                and  control_tipo_proceso       = 'DEP'
                and  tipo_movimiento            = 'CAL'
                order by control_ano_proceso desc, control_mes_proceso desc, con_id desc
                ) where rownum = 1;
               

                /*Validar si el costo historico ha cambiado para ver si se debe recalcular la alicuota*/
                if v_costo_historico_ant = v_costo_historico then
                  --Si el costo historico es igual al anterior no se recalcula la al�cuota
                  begin
                    /*Preguntar si en el periodo de consulta fue trasladado*/
                    select cm.alicuota_traslado, cm.valor_predio, MIN(cm.fecha_movimiento)
                    into v_alicuota_traslado, v_valor_predio_traslado, v_movimiento 
                    from conbi_movimientos cm 
                    where cuenta_conbi_db like '31050303%'
                    and cm.tipo_movimiento= 'ING' 
                    and cm.preurbanizaci_nro_urbanizacion = c_c.preurbanizaci_nro_urbanizacion
                    and cm.pred_nro_predio = c_c.pred_nro_predio
                    and cm.nivel_desagregacion = c_c.nivel_desagregacion
                    and to_char(cm.fecha_movimiento,'yyyymm') = (v_ano||v_mes)
                    group by cm.preurbanizaci_nro_urbanizacion, cm.pred_nro_predio, cm.alicuota_traslado, cm.valor_predio, cm.fecha_movimiento;
                   
                    /*calcular valores de depreciaci�n*/
                    costo_adquisicion         := v_costo_historico;
                    fecha                     := meses + v_alicuota_traslado;
                    nueva_alicuota            := meses + v_alicuota_traslado + 1;
                    tvidautilsindepreciar     := 600 - nueva_alicuota;
                    depreciacion              := round(((v_costo_historico-v_valor_alicuota_anterior_ac-v_valor_predio_traslado)/tvidautilsindepreciar+1),1); 
                    alicuota_acumulada_ant    := v_valor_predio_traslado+v_valor_alicuota_anterior_ac;                                                   
                    nueva_alicuota_acumulada  := alicuota_acumulada_ant + depreciacion ;
                    /*Consultar alicuota anterior si tiene*/
                    begin
                      select depreciacion
                      into alicuota_anterior
                      from (select depreciacion
                      from conbi_depreciacion_amortizacio  
                      where 
                      preurbanizaci_nro_urbanizacion  = c_c.preurbanizaci_nro_urbanizacion
                      and  pred_nro_predio            = c_c.pred_nro_predio
                      and  nivel_secundario           = c_c.nivel_desagregacion
                      and  control_tipo_proceso       = 'DEP'
                      order by control_ano_proceso desc, control_mes_proceso desc, con_id desc
                      ) where rownum = 2;
                      
                    exception when no_data_found then
                      alicuota_anterior         := 0; 
                    end;
                    alicuota_acumulada        := depreciacion + v_valor_predio_traslado;
                    valor_neto                := costo_adquisicion - alicuota_acumulada;
                  exception when no_data_found then
                    /*Cuando el predio no viene de traslado y ya se est� depreciando desde meses anteriores*/
                    costo_adquisicion         := v_costo_historico;
                    fecha                     := meses;
                    nueva_alicuota            := meses + 1;                                                         
                    tvidautilsindepreciar     := vida_util_total - nueva_alicuota;
                    depreciacion              := v_valor_alicuota_anterior;
                    alicuota_anterior         := v_valor_alicuota_anterior;
                    if valor_neto > 0 then
                          depreciacion              := round(((v_costo_historico-v_valor_alicuota_anterior_ac)/(tvidautilsindepreciar+1)),1);
                          alicuota_acumulada_ant    := v_valor_alicuota_anterior_ac;
                          alicuota_acumulada := v_valor_alicuota_anterior_ac + depreciacion;
                          nueva_alicuota_acumulada  := alicuota_acumulada + depreciacion;
                          valor_neto                := costo_adquisicion - alicuota_acumulada;
                          /*Predios que tienen una reclasificaci�n por un menor valor y el c�lculo de la alicuota da negativo*/
                          if depreciacion < 0 then
                            depreciacion              := round(((v_costo_historico)/600),1);
                            alicuota_acumulada_ant := meses * depreciacion;
                            alicuota_acumulada        := alicuota_acumulada_ant + depreciacion; 
                            valor_neto                := costo_adquisicion - alicuota_acumulada;
                            nueva_alicuota_acumulada  := alicuota_acumulada + depreciacion;
                          end if;
                      else
                          
                          depreciacion              := round(((v_costo_historico)/600),1);
                          alicuota_acumulada_ant := meses * depreciacion;
                          alicuota_acumulada        := alicuota_acumulada_ant + depreciacion; 
                          valor_neto                := costo_adquisicion - alicuota_acumulada;
                          nueva_alicuota_acumulada  := alicuota_acumulada + depreciacion;
                      end if;
                  end;
                else
                  if v_costo_historico_ant < v_costo_historico then
                  --Si el costo historico es mayor al anterior se debe recalcular la alicuota
                     begin
                    /*Preguntar si en el periodo de consulta el predio fue trasladado*/
                    select cm.alicuota_traslado, cm.valor_predio, MIN(cm.fecha_movimiento)
                    into v_alicuota_traslado, v_valor_predio_traslado, v_movimiento 
                    from conbi_movimientos cm 
                    where cuenta_conbi_db like '31050303%'
                    and cm.tipo_movimiento= 'ING' 
                    and cm.preurbanizaci_nro_urbanizacion = c_c.preurbanizaci_nro_urbanizacion
                    and cm.pred_nro_predio = c_c.pred_nro_predio
                    and cm.nivel_desagregacion = c_c.nivel_desagregacion
                    and to_char(cm.fecha_movimiento,'yyyymm') = (v_ano||v_mes)--to_char(c_c.fecha_movimiento,'yyyymm') 
                    group by cm.preurbanizaci_nro_urbanizacion, cm.pred_nro_predio, cm.alicuota_traslado, cm.valor_predio, cm.fecha_movimiento;
                   
                    /*calcular valores de depreciaci�n*/
                    costo_adquisicion         := v_costo_historico;
                    fecha                     := meses + v_alicuota_traslado;
                    nueva_alicuota            := meses + v_alicuota_traslado + 1;
                    tvidautilsindepreciar     := 600 - nueva_alicuota;
                    if v_valor_alicuota_anterior_ac = 0 then
                      v_valor_alicuota_anterior_ac := v_valor_alicuota_anterior;
                    end if;
                    depreciacion              := round(((v_costo_historico-v_valor_alicuota_anterior_ac-v_valor_predio_traslado)/(tvidautilsindepreciar+1)),1); 
                    alicuota_acumulada_ant    := v_valor_predio_traslado+v_valor_alicuota_anterior_ac;                                                   
                    nueva_alicuota_acumulada  := alicuota_acumulada_ant + depreciacion ;
                    /*Consultar alicuota anterior si tiene*/
                    begin
                      select depreciacion
                      into alicuota_anterior
                      from (select depreciacion
                      from conbi_depreciacion_amortizacio  
                      where 
                      preurbanizaci_nro_urbanizacion  = c_c.preurbanizaci_nro_urbanizacion
                      and  pred_nro_predio            = c_c.pred_nro_predio
                      and  nivel_secundario           = c_c.nivel_desagregacion
                      and  control_tipo_proceso       = 'DEP'
                      and  rownum <2
                      order by control_ano_proceso desc, control_mes_proceso desc, con_id desc
                      );
                      
                    exception when no_data_found then
                      alicuota_anterior         := 0; 
                    end;   
                    alicuota_acumulada        := depreciacion + alicuota_acumulada_ant;
                    valor_neto                := costo_adquisicion - alicuota_acumulada;
                  exception when no_data_found then
                    /*Cuando el predio no viene de traslado y ya se est� depreciando desde meses anteriores*/
                    /*costo_adquisicion         := v_costo_historico;
                    fecha                     := meses;
                    nueva_alicuota            := meses + 1;                                                         
                    tvidautilsindepreciar     := vida_util_total - nueva_alicuota;
                    depreciacion              := round(((v_costo_historico)/600),1);
                    alicuota_anterior         := v_valor_alicuota_anterior;
                    /*Predios que tienen una reclasificaci�n por un menor valor y el c�lculo da negativo*/
                   /* alicuota_acumulada_ant := meses * depreciacion;
                    alicuota_acumulada := alicuota_acumulada_ant + depreciacion;
                    nueva_alicuota_acumulada  := alicuota_acumulada + depreciacion;
                    valor_neto                := costo_adquisicion - alicuota_acumulada;*/
                    /*Cuando ya se est� depreciando desde meses anteriores*/
                    costo_adquisicion         := v_costo_historico;
                    fecha                     := meses;
                    nueva_alicuota            := meses + 1;                                                         
                    tvidautilsindepreciar     := vida_util_total - nueva_alicuota;
                    
                    if valor_neto > 0 then
                          depreciacion              := round(((v_costo_historico-v_valor_alicuota_anterior_ac)/(tvidautilsindepreciar+1)),1);
                          alicuota_acumulada_ant    := v_valor_alicuota_anterior_ac;
                          alicuota_acumulada := v_valor_alicuota_anterior_ac + depreciacion;
                          nueva_alicuota_acumulada  := alicuota_acumulada + depreciacion;
                          valor_neto                := costo_adquisicion - alicuota_acumulada;
                          /*Predios que tienen una reclasificaci�n por un menor valor y el c�lculo de la alicuota da negativo*/
                          if depreciacion < 0 then
                            depreciacion              := round(((v_costo_historico)/600),1);
                            alicuota_acumulada_ant := meses * depreciacion;
                            alicuota_acumulada        := alicuota_acumulada_ant + depreciacion; 
                            valor_neto                := costo_adquisicion - alicuota_acumulada;
                            nueva_alicuota_acumulada  := alicuota_acumulada + depreciacion;
                          end if;
                      else
                          
                          depreciacion              := round(((v_costo_historico)/600),1);
                          alicuota_acumulada_ant := meses * depreciacion;
                          alicuota_acumulada        := alicuota_acumulada_ant + depreciacion; 
                          valor_neto                := costo_adquisicion - alicuota_acumulada;
                          nueva_alicuota_acumulada  := alicuota_acumulada + depreciacion;
                      end if;
                  end;
                  else
                  --Si el costo historico es menor al anterior se debe recalcular la alicuota
                     begin
                      /*Preguntar si en el periodo de consulta el predio fue trasladado*/
                      select cm.alicuota_traslado, cm.valor_predio, MIN(cm.fecha_movimiento)
                      into v_alicuota_traslado, v_valor_predio_traslado, v_movimiento 
                      from conbi_movimientos cm 
                      where cuenta_conbi_db like '31050303%'
                      and cm.tipo_movimiento= 'ING' 
                      and cm.preurbanizaci_nro_urbanizacion = c_c.preurbanizaci_nro_urbanizacion
                      and cm.pred_nro_predio = c_c.pred_nro_predio
                      and cm.nivel_desagregacion = c_c.nivel_desagregacion
                      and to_char(cm.fecha_movimiento,'yyyymm') = (v_ano||v_mes)--to_char(c_c.fecha_movimiento,'yyyymm') 
                      group by cm.preurbanizaci_nro_urbanizacion, cm.pred_nro_predio, cm.alicuota_traslado, cm.valor_predio, cm.fecha_movimiento;
                     
                      /*calcular valores de depreciaci�n*/
                      costo_adquisicion         := v_costo_historico;
                      fecha                     := meses + v_alicuota_traslado;
                      nueva_alicuota            := meses + v_alicuota_traslado + 1;
                      tvidautilsindepreciar     := 600 - nueva_alicuota;
                      depreciacion              := round(((v_costo_historico-v_valor_predio_traslado)/(tvidautilsindepreciar+1)),1); 
                      alicuota_acumulada_ant    := v_valor_predio_traslado;                                                   
                      nueva_alicuota_acumulada  := alicuota_acumulada_ant + depreciacion ;
                      /*Consultar alicuota anterior si tiene*/
                      begin
                        select depreciacion
                        into alicuota_anterior
                        from (select depreciacion
                        from conbi_depreciacion_amortizacio  
                        where 
                        preurbanizaci_nro_urbanizacion  = c_c.preurbanizaci_nro_urbanizacion
                        and  pred_nro_predio            = c_c.pred_nro_predio
                        and  nivel_secundario           = c_c.nivel_desagregacion
                        and  control_tipo_proceso       = 'DEP'
                        order by control_ano_proceso desc, control_mes_proceso desc, con_id desc
                        ) where rownum = 2;
                        
                      exception when no_data_found then
                        alicuota_anterior         := 0; 
                      end;   
                      alicuota_acumulada        := depreciacion + alicuota_acumulada_ant;
                      valor_neto                := costo_adquisicion - alicuota_acumulada;
                    exception when no_data_found then
                      /*Costo menor que el anterior, el predio no viene de traslado y ya se est� depreciando desde meses anteriores*/
                      costo_adquisicion         := v_costo_historico;
                      fecha                     := meses;
                      nueva_alicuota            := meses + 1;                                                         
                      tvidautilsindepreciar     := vida_util_total - nueva_alicuota;
                      alicuota_anterior         := v_valor_alicuota_anterior;
                      
                     if valor_neto > 0 then
                          depreciacion              := round(((v_costo_historico-v_valor_alicuota_anterior_ac)/(tvidautilsindepreciar+1)),1);
                          alicuota_acumulada_ant    := v_valor_alicuota_anterior_ac;
                          alicuota_acumulada := v_valor_alicuota_anterior_ac + depreciacion;
                          nueva_alicuota_acumulada  := alicuota_acumulada + depreciacion;
                          valor_neto                := costo_adquisicion - alicuota_acumulada;
                          /*Predios que tienen una reclasificaci�n por un menor valor y el c�lculo de la alicuota da negativo*/
                          if depreciacion < 0 then
                            depreciacion              := round(((v_costo_historico)/600),1);
                            alicuota_acumulada_ant := meses * depreciacion;
                            alicuota_acumulada        := alicuota_acumulada_ant + depreciacion; 
                            valor_neto                := costo_adquisicion - alicuota_acumulada;
                            nueva_alicuota_acumulada  := alicuota_acumulada + depreciacion;
                          end if;
                      else
                          
                          depreciacion              := round(((v_costo_historico)/600),1);
                          alicuota_acumulada_ant := meses * depreciacion;
                          alicuota_acumulada        := alicuota_acumulada_ant + depreciacion; 
                          valor_neto                := costo_adquisicion - alicuota_acumulada;
                          nueva_alicuota_acumulada  := alicuota_acumulada + depreciacion;
                      end if;
                    end;
                  end if;
                end if;
              exception when no_data_found then
                /*El predio nunca se ha depreciado, es la primera vez*/
                begin
                  /*Predios Trasladados*/
                  select sum(cm.alicuota_traslado), sum(cm.valor_predio), MIN(cm.fecha_movimiento)
                  into v_alicuota_traslado, v_valor_predio_traslado, v_movimiento 
                  from conbi_movimientos cm 
                  where cuenta_conbi_db like '31050303%'
                  and cm.tipo_movimiento= 'ING' 
                  and cm.preurbanizaci_nro_urbanizacion = c_c.preurbanizaci_nro_urbanizacion
                  and cm.pred_nro_predio = c_c.pred_nro_predio
                  and cm.nivel_desagregacion = c_c.nivel_desagregacion
                  and to_char(cm.fecha_movimiento,'yyyymm') = (v_ano||v_mes)--to_char(c_c.fecha_movimiento,'yyyymm') 
                  --group by cm.preurbanizaci_nro_urbanizacion, cm.pred_nro_predio, cm.alicuota_traslado, cm.valor_predio, cm.fecha_movimiento;
                  group by 'empty set of columns';
                  
                  /*calcular valores de depreciaci�n*/
                  costo_adquisicion         := v_costo_historico;--nvl(c_c.valor_predio,1);
                  meses                     := v_alicuota_traslado;
                  fecha                     := meses;
                  nueva_alicuota            := meses + 1;
                  tvidautilsindepreciar     := 600 - nueva_alicuota;
                  depreciacion              := round(((v_costo_historico-v_valor_predio_traslado)/(tvidautilsindepreciar+1)),1); 
                  alicuota_acumulada_ant    := v_valor_predio_traslado;                                                   
                  nueva_alicuota_acumulada  := v_valor_predio_traslado + depreciacion ;
                  /*Consultar alicuota anterior si tiene*/
                    begin
                      select depreciacion
                      into alicuota_anterior
                      from (select depreciacion
                      from conbi_depreciacion_amortizacio  
                      where 
                      preurbanizaci_nro_urbanizacion  = c_c.preurbanizaci_nro_urbanizacion
                      and  pred_nro_predio            = c_c.pred_nro_predio
                      and  nivel_secundario           = c_c.nivel_desagregacion
                      and  control_tipo_proceso       = 'DEP'
                      order by control_ano_proceso desc, control_mes_proceso desc, con_id desc
                      ) where rownum = 2;
                      
                    exception when no_data_found then
                      alicuota_anterior         := 0; 
                    end;    
                  alicuota_acumulada        := depreciacion + v_valor_predio_traslado;
                  valor_neto                := costo_adquisicion - alicuota_acumulada;
                exception when no_data_found then
                  costo_adquisicion         := v_costo_historico;--nvl(c_c.valor_predio,1);
                  fecha                     := meses;
                  nueva_alicuota            := meses + 1;
                  depreciacion              := round((costo_adquisicion/600),1) * nueva_alicuota ; 
                  tvidautilsindepreciar     := 600 - nueva_alicuota;
                  alicuota_acumulada_ant    := 0;                                                   
                  nueva_alicuota_acumulada  := round((costo_adquisicion/600),1) * nueva_alicuota ;
                  alicuota_anterior         := 0;    
                  valor_neto                := costo_adquisicion - depreciacion * nueva_alicuota;
                  alicuota_acumulada        := depreciacion * nueva_alicuota;
                end;
              end;
              
              INSERT INTO conbi_dep_amo_pruebas(
                                            preurbanizaci_nro_urbanizacion, 
                                            pred_nro_predio,
                                            nivel_secundario,
                                            depreciacion,   
                                            depreciacion_acumulada,
                                            depreciacion_ajustada,        
                                            axi_depreciacion,  
                                            vida_util,                    
                                            tvidautilsindepreciar,        
                                            tiempo_dep_amo_calculada,     
                                            control_ano_proceso,        
                                            control_mes_proceso,
                                            control_tipo_proceso,         
                                            valor_neto,                   
                                            transaccion,
                                            tipo_movimiento,
                                            ID,
                                            fecha_incorpora,
                                            costo_historico,
                                            nueva_alicuota_acumulada,       
                                            valor_alicuota_ant,             
                                            alicuota_anterior,
                                            cuenta_conbi_db,
                                            cuenta_conbi_cr,
                                            cuenta_movimiento,
                                            uso_predio,
                                            uso_nivel1,
                                            uso_nivel2,
                                            tipo_acto_juridico,
                                            tipo_procedencia,
                                            clase_cuenta
                                            )
                  VALUES    (
                              c_c.preurbanizaci_nro_urbanizacion,     
                              c_c.pred_nro_predio,                    
                              c_c.nivel_desagregacion,                 
                              depreciacion,                           
                              alicuota_acumulada_ant,                 
                              0,                                      
                              0,                                      
                              vida_util_total,                        
                              tvidautilsindepreciar,                  
                              nueva_alicuota,                         
                              v_ano,                                  
                              v_mes,                                  
                              'DEP',                                  
                              valor_neto,                              
                              1,                                      
                              'CAL',  
                              SEQ_CONBI_AMO_PLANB.NEXTVAL,             
                              fec_ing_cont,                  /* FECHA COSTO ADQUISICION */
                              costo_adquisicion,             --Costo adquisicion a la fecha
                              alicuota_acumulada,                     
                              alicuota_anterior,                      
                              nueva_alicuota,                         
                              cuenta_conbi_db,                        
                              cuenta_conbi_cr,
                              c_c.cuenta_conbi_db,
                              c_c.uso_predio,
                              c_c.uso_nivel1,
                              c_c.uso_nivel2,
                              c_c.tipo_acto_juridico,
                              c_c.tipo_procedencia,
                              c_c.clase_cuenta 
                              ); 
            end if;
         end if;
        exception
          when no_data_found then
            bandera := 0;
        end;
     end loop;
   commit;
end;